classdef MDSplus_java < MDSplus_interface
%MATLAB-MDSPlus java interface wrapper
%   This class implements a wrapper around
%   the MATLAB-MDSplus java interface
%   Details on how this interface works
%   can be found here:
%   https://mdsplus.org/index.php?title=Documentation:Reference:Matlab&open=1625619303906689417247&page=Documentation%2FReference%2FMatlab
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    methods

        function testenv(obj)
            ok=mdstest(0);
            if ok
                disp('MDSplus_java all good');
            end
        end

        function str = getmdsvaluecmdstr(obj)
            str = 'mdsvalue';
        end
        
        function fcn = getmdsvaluecmd(obj)
            fcn = str2func(obj.getmdsvaluecmdstr);
        end
                
        function ok = connect(obj, server)
            res = mdsconnect(server);
            if ~(res==1)
                warning('MDSplus_python:mdsconnectfail','mdsconnect failed (server ''%s'')',server);
                ok=false;
            else
                ok=true;
            end
        end

        function ok = open(obj, tree, shot)
            [res,ok] = mdsopen(tree, shot);
            if ~ok 
                warning('MDSplus_java:mdsopenfail','%s',res);
            end
        end

        function ok = close(obj)
            ok = mdsclose;
            if ~ok
                warning('MDSplus_java:mdsclosefail','mdsclose fail');
            end
        end

        function [val, ok] = value(obj, expr)
            [val, ok] = mdsvalue(expr);
            if ~ok
                warning('MDSplus_java:mdsvaluefail','mdsvalue fail, %s',val);
            end
        end
        
        function ok = put(obj, node, expression, varargin)
            [s]=mdsput(node, expression, varargin{:});
            if isnumeric(s)
                if ~rem(s,2)
                    mdserr = mdsvalue(['getmsg(' num2str(s) ')']);
                    warning('MDSplus_java:mdsputfail','mdsput failed, %s',mdserr);
                    ok=false;
                else
                    ok=true;
                end
            elseif ischar(s)
                    warning('MDSplus_java:mdsputfail','mdsput failed, %s',s);
                    ok=false;
            else
               warning('MDSplus_java:mdsputfail','mdsput failed, returned value untreated');
               ok=false;
            end 
        end
        
        function ok = disconnect(obj)
            ok=mdsdisconnect;
            if ~ok
               warning('MDSplus_java:mdsdisconnectfail','mdsdisconnect failed');
            end
        end
    
    end
end