classdef (Abstract) MDSplus_interface
    % Abstract class for MDSplus interfaces
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    methods(Abstract)
        str = getmdsvaluecmdstr(obj); % String for mdsvalue getting command
        fcn = getmdsvaluecmd(obj);    % Function handle version of getmdsivaluecmdstr
        ok = connect(obj, server);    % MDSplus connect wrapper
        ok = open(obj, shot);         % MDSplus open shot wrapper
        ok = close(obj);              % MDSplus close shot wrapper
        [val, ok] = value(obj, expr); % MDSplus value request wrapper
        ok = put(obj, node, expression, varargin);  % MDSplus put expression wrapper
        ok = disconnect(obj);         % MDSplus disconnect wrapper   
        testenv(obj);                 % Testing methodfor MDS environment                            
    end
end