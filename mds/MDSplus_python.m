classdef MDSplus_python < MDSplus_interface
%MATLAB-MDSPlus python interface wrapper
%   This class implements a wrapper around
%   the MATLAB-MDSplus python interface
%   Details on how this interface works
%   can be found here:
%   https://mdsplus.org/index.php?title=Documentation:Reference:Matlab&open=1625619303906689417247&page=Documentation%2FReference%2FMatlab
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    methods
        function testenv(obj)
            mdsUsePython(true);
            ok=mdstest(1);
            if ok
                disp('MDSplus_python all good');
            end
        end
       
        % Other methods not yet implemented
        % waiting for a good environment
        % to test MATLAB-Python MDSinterface 
    end
end