/*
  Creates dictionary of mem or stat signals for the RTC tree

call:
  _dict = rtc_make_dict(_type, [_inode,[_ithread]])

arguments:
  _type: "mem" or "stat"
  _inode: [optional] restrict dictionary to signals from this rt-node
  _ithread: [optional] restrict dictionary to signals from this thread
            (must be used together with _inode)

returns:
  _dict: signal with node addresses as data and mem/stat names as dimension.
         _dict[_name] then yields the node address corresponding to the mem named _name
*/
FUN PUBLIC rtc_make_dict(_type, optional _inode, optional _ithread)
{

  _nodestr= ($SHOT>65700 || $SHOT==-1) ? "node" : "tcvrt";

  _thread = present(_ithread) ? ".thread"//iii(_ithread,2) : "";
  _node   = present(_inode  ) ? "."//_nodestr//iii(_inode,2) : "";

  _base   = "\\rtc::top"//_node//_thread//"***:"//_type//"_%%%";
 
  _nids   = TreeFindNodeWild(_base);
  _path   = getnci(_nids,"PATH");
  if (size(_path)>0)
  {
    _len    = getnci(adjustl(adjustr(_path)//":raw"),"LENGTH");
    _path   = pack(_path,_len>0);
  }
  
  if (size(_path)>0)
  {
    _name   = getnci(adjustl(adjustr(_path)//":name"),"record");
    _order  = sort(_name);
  
    _dict   = MAKE_SIGNAL(_path[_order],*,_name[_order]);
  }
  else
  {
    _dict   = MAKE_SIGNAL([],*,[]);
  }
  return(_dict);

}
