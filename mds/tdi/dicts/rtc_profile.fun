/*
  Builds a jScope plottable profile 
  out of a MDS+ node stored
  with MARTe2 (i.e. with only the temporal
  dimension available). This works only for
  2D signals.

call:
  _sig = rtc_profile(_s)

arguments:
  _s input MDS+ channel containing
     a time dependent profile in this form:
     - data shape is [profile points, times]
     - the first dimension is time
  _d1 dimension array to be used in the build_signal expr.

returns:
  a jScope plottable build_signal
  expression to plot the profile 
  with live update capability (ctrl-Y in jScope)
*/
FUN PUBLIC rtc_profile(_s, optional in _d1)
{
  _d1 = present(_d1) ? _d1 : *;
  if (ndims(_s) != 2) ABORT();
  Return(make_param(make_signal(transpose(DATA(_s)), *, DIM_OF(_s), _d1),help_of(_s),1));
}
