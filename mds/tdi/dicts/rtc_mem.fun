/*
  Retrieves signal corresponding to named mem signal in RTC tree

call:
  _sig = rtc_mem(_name, [_inode,[_ithread]])

arguments:
  _name: name of the mem signal to retrieve
  _inode: [optional] retrieve dictionary from this rt-node only
  _ithread: [optional] retrieve dictionary from this thread only
            (must be used together with _inode)

returns:
  _sig: Signal (node reference) corresponding to mem named _name

See also: rtc_get
*/
FUN PUBLIC rtc_mem(_name, optional _inode, optional _ithread, optional _dict)
{
  Return(rtc_get("mem", "mem."//_name, _inode, _ithread, _dict));
}
