/*
  Computes delta time from a three-fold MARTe2 TimingsDatasource signal

call:
  _sig = rtc_deltatime(_signal)

arguments:
  _signal: name of the signal to compute on (usually a stats signal)
         it has to contain three signals, the Read, Exec and Write
	 timestamps in MARTe2 terminology
  _convfact: an optional conversion factor the result is multiplied with

returns:
  _sig: Signal containing Write - Read time (relative overall execution time)
        If no _convfact is given, the result should be in seconds,
	otherwise it is the deltatime recorded by MARTe2 (in its units) 
	multiplied by _convfact.
	Up to end 2022 MARTe2 records times in [ns]

*/
FUN PUBLIC rtc_deltatime(_signal, optional _convfact)
{
  _convf = present(_convfact  ) ? _convfact : 1e-9;
  _data = data(_signal);
  /* this works for present version of MDSplus segmented signals
     it could be necessary to change to dim_of(_signal,1) 
     in future releases */
  _time = dim_of(_signal);
  
  if(ndims(_data)!=2 || shape(_data,0)!=3)
  {
  	abort();
  }
  _delta = (_data[2,*] - _data[0,*]) * _convf;
  _retsig = make_signal(_delta,*,_time);
  Return(_retsig);
}
