/*
  Retrieves signal corresponding to named mem/stat signal in RTC tree

call:
  _sig = rtc_get(_type, _name, [_inode,[_ithread]])

arguments:
  _type: "mem" or "stat"
  _name: name of the signal to retrieve
  _inode: [optional] retrieve dictionary from this rt-node only
  _ithread: [optional] retrieve dictionary from this thread only
            (must be used together with _inode)

returns:
  _sig: Signal (node reference) corresponding to _name with type _type

  This function requires an rtc tree to be open.
  If _inode and _ithread are absent then the global dictionary is queried.
  For debugging puposes a dictionary can be given as a 5th argument and it
  will be used in place of the one stored in the tree.
*/
FUN PUBLIC rtc_get(_type, _name, optional _inode, optional _ithread, optional _dict)
{
  _has_inode   = present(_inode  ) && KIND(_inode  )>0;
  _has_ithread = present(_ithread) && KIND(_ithread)>0;
  _has_dict    = present(_dict   ) && KIND(_dict   )>0;  

  if (!_has_dict)
  {

    _nodestr= ($SHOT>65700 || $SHOT==-1) ? "node" : "tcvrt";

    _thread = _has_ithread ? ".thread"//iii(_ithread,2)//"." : "";
    _node   = _has_inode   ? _nodestr //iii(_inode  ,2)//"." : "";

    _base   = "\\rtc::top."//_node//_thread//"dicts:"//_type//"s";

    _dict   = build_path(_base);
  }
 
  
  _mem = size(_dict)>0 ? _dict[_name] : abort(); 
  
  if (dim_of(_mem) != _name) { abort();} 
 
  return(BUILD_PATH(data(_mem)));
}
