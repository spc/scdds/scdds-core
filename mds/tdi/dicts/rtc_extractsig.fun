/*
  Extracts a signal from a multiple signals MARTe2 acquired MDSplus node

call:
  _sig = rtc_extractsig(_signal,_signo)

arguments:
  _signal: name of the signal to work on
           it has to contain multiple signals,
	   this is the normal case when a multidimensional signal is
	   passed to the MDSWriter Datasource component in MARTe2
  _signo:  signal number to extract, one based

returns:
  _sig: Extracted signal

*/
FUN PUBLIC rtc_extractsig(_signal, _signo)
{    
  _data = data(_signal);
  /* this works for present version of MDSplus segmented signals
     it could be necessary to change to dim_of(_signal,1) 
     in future releases */  
  _time = dim_of(_signal);
  _maxsig = shape(_data,0);
  if(ndims(_data)!=2 || _signo>_maxsig || _signo<1)
  {
  	abort();
  } 
  _retdata = _data[_signo-1,*];
  _retsig = make_signal(_retdata,*,_time);
  
  Return(_retsig);
}
