classdef MDSplus_mdsipmex < MDSplus_interface
%MATLAB-MDSPlus mdsipmex interface wrapper
%   This class implements a wrapper around
%   the MATLAB-MDSplus mdsipmex interface
%   This is the MATLAB-MDSplus interfaced
%   developed and used by EFPL-SPC
%   Details on how this interface works
%   can be found here (EPFL access needed)
%   https://spcwiki.epfl.ch/wiki/MdsPlusCRPP#mdsipmex
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    methods
        
        function testenv(obj)
           assert(~isempty(which('mdsipmex')), ...
                'MDSplus_mdsipmex:noipmex','Could not find mdsipmex, are the mds matlab tools installed?');
           assert(~~exist('mdsconnect','file'),...
                'MDSplus_mdsipmex:nomdsconnect','mdsconnect not found, are the mds matlab tools installed?');
           assert(~~exist('mdsopen','file'),...
                'MDSplus_mdsipmex:nomdsopen','mdsopen not found, are the mds matlab tools installed?');
           assert(~~exist('mdsvalue','file'),...
                'MDSplus_mdsipmex:nomdsvalue','mdsvalue not found, are the mds matlab tools installed?');
           assert(~~exist('mdsvalueraw','file'),...
                'MDSplus_mdsipmex:nomdsvalueraw','mdsvalueraw not found, are the mds matlab tools installed?');
           assert(~~exist('mdsput','file'),...
                'MDSplus_mdsipmex:nomdsput','mdsput not found, are the mds matlab tools installed?');
           assert(~~exist('mdsclose','file'),...
                'MDSplus_mdsipmex:nomdsclose','mdsclose not found, are the mds matlab tools installed?');
           assert(~~exist('mdsdisconnect','file'),...
                'MDSplus_mdsipmex:nomdsdisconnect','mdsdisconnect not found, are the mds matlab tools installed?');
           disp('MDSplus_mdsipmex all good');
        end
        
        function str = getmdsvaluecmdstr(obj)
            str = 'mdsvalueraw';
        end
        
        function fcn = getmdsvaluecmd(obj)
            fcn = str2func(obj.getmdsvaluecmdstr);
        end
        
        function ok = connect(obj, server)
            ok = mdsconnect(server);
            if ~ok
                warning('MDSplus_mdsipmex:mdsconnectfail','mdsconnect failed (server ''%s'')',server);
            end
        end
        
        function ok = open(obj, tree, shot)
            [o,s] = mdsopen(tree, shot);
            if ~rem(s,2) || (shot ~= 0 && o ~= shot)
                mdserr = mdsvalue(['getmsg(' num2str(s) ')']);
                warning('MDSplus_mdsipmex:mdsopenfail','mds open failed, %s',mdserr);
                ok=false;
            else
                ok=true;
            end
        end

        function ok = close(obj)
            s = mdsclose;
            if ~rem(s,2)
                mdserr = mdsvalue(['getmsg(' num2str(s) ')']);
                warning('MDSplus_mdsipmex:mdsopenfail','mds open failed, %s',mdserr);
                ok=false;
            else
                ok=true;
            end
        end
        
        function [val, ok] = value(obj, expr)
            [o,s] = mdsvalueraw(expr);
            if ~rem(s,2)
                mdserr = mdsvalue(['getmsg(' num2str(s) ')']);
                warning('MDSplus_mdsipmex:mdsvaluefail','mdsvalue failed, %s',mdserr);
                ok=false;
                val=[];
            else
                ok=true;
                val=o;
            end
        end
        
        function ok = put(obj, node, expression, varargin)
            [s]=mdsput(node,expression,'x',varargin{:});
            if isnumeric(s)
                if ~rem(s,2)
                    mdserr = mdsvalue(['getmsg(' num2str(s) ')']);
                    warning('MDSplus_mdsipmex:mdsputfail','mdsput failed, %s',mdserr);
                    ok=false;
                else
                    ok=true;
                end
            elseif ischar(s)
                    warning('MDSplus_mdsipmex:mdsputfail','mdsput failed, %s',s);
                    ok=false;
            else
               warning('MDSplus_mdsipmex:mdsputfail','mdsput failed, returned value untreated');
               ok=false;
            end 
        end    
            
        function ok = disconnect(obj)
            [s]=mdsdisconnect;
            if ~s
               warning('MDSplus_mdsipmex:mdsdisconnectfail','mdsdisconnect failed');
               ok=false; 
            else
               ok=true;
            end
        end
    
    end
end

