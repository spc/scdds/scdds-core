/* Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only. */
/* [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022. */
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <libgen.h>

#define ALGOINFOMAXLEN 1024

//#pragma pack(1)

struct algoinfo {
 char text[ALGOINFOMAXLEN];
 unsigned int len;
} __attribute__((packed));

//#pragma pack()

int main(int argn, char *argv[])
{
	char sofilename[1024];
	char *filename;
	//char localfilename[1024];
	char *localfilename;
	//char algoinfofcnname[1024];
	char *algoinfofcnname;
	int size;
	
	void *libhnd;
	void (*algoinfofunc)(struct algoinfo *);
    	struct algoinfo ai;

	if(argn!=2)
	{
		printf("usage %s <scd .so filename>\n", argv[0]);
		exit(-1);
	} 	
		    
	libhnd=dlopen(argv[1], RTLD_LAZY | RTLD_GLOBAL);
	if(libhnd==NULL)
	{
		fprintf(stderr,"Error loading %s, try ldd .so to check libs\n", argv[1]);
		exit(0); 
	}
 
 	filename=basename(argv[1]);
	size=snprintf(localfilename, 0,"%s", filename);
	localfilename = malloc(size * sizeof(char));	
	snprintf(localfilename, strlen(filename)-2,"%s", filename);
 	
	size=snprintf(algoinfofcnname, 0,"%s_GetAlgoInfo", localfilename);
	algoinfofcnname = malloc(size * sizeof(char));
	snprintf(algoinfofcnname, strlen(localfilename)+13,"%s_GetAlgoInfo", localfilename);
 
	algoinfofunc=dlsym(libhnd, algoinfofcnname);
	if(algoinfofunc==NULL)
	{
		fprintf(stderr,"Error finding %s in .so\n", algoinfofcnname);
		perror(NULL);
		exit(0);
	}

    (*algoinfofunc)(&ai);
    
    printf("%s, (%d characters)\n", ai.text, ai.len);
    
    free(localfilename);
    free(algoinfofcnname);

}
