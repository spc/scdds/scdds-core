function export_sldd_to_base(dict_name)
% function to export sldd contents to base workspace
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

hDict = Simulink.data.dictionary.open([dict_name,'.sldd']);
hDesignData = hDict.getSection('Global');
childNamesList = {hDesignData.find.Name};
for n = 1:numel(childNamesList)
  hEntry = hDesignData.getEntry(childNamesList{n});
  assignin('base', hEntry.Name, hEntry.getValue);
end

