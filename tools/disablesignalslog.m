% function disablesignalslog(topmodelname,varargin)
%
% This function parses automatically
% all signals of the given modelname
% (comprising all submodels)
% and asks whether to disable log for
% signals (which are marked for logging)
%
% the function asks before removing any logging
% unless the additional parameter couple 'force', 1
% is given in varargin
%
% changed models are not saved to disk if the
% optional parameter 'dryrun' is set to 1,
% also they are all kept open in this case
%
% it seems that the Simulink Data Inspector
% is resilient to this changes
% try this:
% 1) Simulink.sdi.clear
% 2) restart Matlab
% 3) Delete signals manually there
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.


function disablesignalslog(topmodelname,varargin)
   P=inputParser;
   P.addParameter('force',0);
   P.addParameter('dryrun',0);
   P.parse(varargin{:});
   
   force=P.Results.force;
   dryrun=P.Results.dryrun;

   disp('Getting mdlrefs info ...');
   [models,blocks]=find_mdlrefs(topmodelname); 
   for mod=1:numel(models)
      fprintf(['Analyzing model ' char(models{mod}) '\n']);
      disablesignalslogsinglemodel(models{mod},force,dryrun);
   end
end

function disablesignalslogsinglemodel(modelname,force,dryrun)
load_system(modelname);
mdlsignals = find_system(modelname,'FindAll','on','LookUnderMasks','all','FollowLinks','on','type','line','SegmentType','trunk');
ph = get_param(mdlsignals,'SrcPortHandle');
changed=false;
for i=1: length(ph)
    try
        if strcmp(get_param(ph{i},'datalogging'),'on')
            if ~force
              answer=input(['MDL: ' modelname ' SIG: ' get_param(ph{i},'Name') ' log is ' get_param(ph{i},'datalogging') ', turn off? [Y/N]:'],'s');
            else
              fprintf(['MDL: ' modelname ' SIG: ' get_param(ph{i},'Name') ' disabling logging.\n']);
              answer='Y';
            end
            if strcmp(upper(answer),'Y')
                changed=true;
                set_param(ph{i},'datalogging','off');
            end
        end
    catch ME
        continue;
    end
end
if changed && ~dryrun
  save_system(modelname);
end
if ~dryrun
  close_system(modelname);
end
end