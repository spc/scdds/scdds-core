function config_export_to_base_and_scripts()
% exports all configurations of
% configurations_container to base
% workspace and to .m
% creators scripts so they can be
% easily
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

dict_name='configurations_container';
hDict = Simulink.data.dictionary.open([dict_name,'.sldd']);
hDesignData = hDict.getSection('Configurations');
childNamesList = hDesignData.evalin('who');
curpath=pwd;
[fcnpath,~,~]=fileparts(mfilename('fullpath'));
try
    system(['cd ' fcnpath '; mkdir exportedcfgs']);
    for n = 1:numel(childNamesList)
        hEntry = hDesignData.getEntry(childNamesList{n});
        fprintf('Exporting %s to base workspace and %s.m ...\n',hEntry.Name,hEntry.Name);
        assignin('base', hEntry.Name, hEntry.getValue);
        evalin('base', [hEntry.Name '.saveAs(''' fcnpath '/exportedcfgs/' hEntry.Name ''');']);
    end
catch ME
    report = getReport(ME)
    system(['cd ' curpath]);
end
system(['cd ' curpath]);