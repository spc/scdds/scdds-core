Simulink configurations are stored
in configurations_container.sldd 
There is one configuration for
simulation and a number of
configurations for code generation.
This is necessary as code generation
config may vary depending on
algorithm content (different tmf file,
additional libs, includes, 
custom code ecc.)

There are functions to manually
handle this container:

- config_set_diags.m: a function
for defining the diagnostic part
for configSettings
- config_update_diags.m: calls the
previous and updates diags in
the sldd 
- config_export_to_base_and_scripts.m:
exports the content of configurations
sldd to base workspace and files


