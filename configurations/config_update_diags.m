function config_update_diags(config_name)
% gets the configurationSettings config_name from
% dd configurations_container.sldd and calls config_set_diags on it
% the result is written back to the dd (without saving it)
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

dict_name='configurations_container';
hDict = Simulink.data.dictionary.open([dict_name,'.sldd']);
hDesignData = hDict.getSection('Configurations');
csentry = hDesignData.getEntry(config_name);
csvalue = csentry.getValue;
csvalue = config_set_diags(csvalue);
csentry.setValue(csvalue);


