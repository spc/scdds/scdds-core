classdef SCDDSclass_functions_test < SCDDStest
% Tests for SCDDSclass_parameter
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

  properties
    testbus
    shot
  end
  
  methods(TestClassSetup)
    function setup_paths(~)
    end
  
    function test_constructor(testCase)
      env=SCDDSclass_env;
      mdsconfig   = env.get_mdsconfig();
      testCase.shot = env.get_testshot();
      
      testCase.testbus=Simulink.Bus;
      selem=Simulink.BusElement;
      selem.Name='a';
      selem.DataType = 'single';
      elems(1)=selem;
      selem.Name='b';
      selem.DataType = 'logical';
      elems(2)=selem;
      selem.Name='c';
      selem.DataType = 'Enum: test_enum';
      elems(3)=selem;
      selem.Name='d';
      selem.DataType = 'boolean';
      elems(4)=selem;      
      testCase.testbus.Elements=elems;
    end
  end
  
  methods(Access=private)
    
  end
  
  methods(Test,TestTags={'unit'})
    
    function test_SCDconf_createstructfrombus(testCase)
      s = SCDconf_createstructfrombus(testCase.testbus);
      
      testCase.assertTrue(isa(s,'struct'));
      testCase.assertTrue(isa(s.a,'timeseries'));      
      testCase.assertTrue(isa(s.b,'timeseries'));      
      testCase.assertTrue(isa(s.c,'timeseries'));  
      testCase.assertTrue(isa(s.d,'timeseries'));        
      testCase.assertTrue(isa(s.a.Data,'single'));
      testCase.assertTrue(isa(s.b.Data,'logical'));
      testCase.assertTrue(isa(s.c.Data,'test_enum'));
      testCase.assertTrue(isa(s.d.Data,'logical'));      
    end
    
    function test_disablesignalslog(testCase)
      close_system('logremove_test');
      log1 = sim('logremove_test');
      disablesignalslog('logremove_test','force',1,'dryrun',1);
      log2 = sim('logremove_test');
      
      testCase.assertClass(log1.logsout,'Simulink.SimulationData.Dataset');
      testCase.assertEmpty(find(log2,'logsout'));
    end
    
  end
  
  
end
