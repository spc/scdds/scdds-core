classdef(Enumeration) test_enum < int8
  % Test enumeration class
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  enumeration
    OK(0)
    NOK(1)
  end
end

