classdef test_structbus < SCDDStest
  % Test for structbus functions
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  properties
    testBus = test_structbus.getTestBus();
  end
  
  methods(Test,TestTags={'unit'})
    function testDifferentStruct(testCase)
      mystruct = struct('a',1);
      testCase.assertFalse(SCDconf_structbuscmp(mystruct,testCase.testBus));
    end
    
    function testSameStruct(testCase)
      mystruct = SCDconf_createstructfrombus(testCase.testBus);  % generate to match bus
      areequal = SCDconf_structbuscmp(mystruct,testCase.testBus);
      testCase.assertTrue(areequal);
    end
  end
  
    methods(Static)
    function testBus = getTestBus()
      testBus = Simulink.Bus;
      testBus.Description = '';
      testBus.DataScope = 'Auto';
      testBus.HeaderFile = '';
      testBus.Alignment = -1;
      saveVarsTmp{1} = Simulink.BusElement;
      saveVarsTmp{1}.Name = 'a';
      saveVarsTmp{1}.Complexity = 'real';
      saveVarsTmp{1}.Dimensions = 24;
      saveVarsTmp{1}.DataType = 'single';
      saveVarsTmp{1}.Min = [];
      saveVarsTmp{1}.Max = [];
      saveVarsTmp{1}.DimensionsMode = 'Fixed';
      saveVarsTmp{1}.SamplingMode = 'Sample based';
      saveVarsTmp{1}.SampleTime = -1;
      saveVarsTmp{1}.DocUnits = 'varies';
      saveVarsTmp{1}.Description = ['References for observable errors coming ' ...
        'from the A matrix'];
      saveVarsTmp{1}(2, 1) = Simulink.BusElement;
      saveVarsTmp{1}(2, 1).Name = 'b';
      saveVarsTmp{1}(2, 1).Complexity = 'real';
      saveVarsTmp{1}(2, 1).Dimensions = 8;
      saveVarsTmp{1}(2, 1).DataType = 'single';
      saveVarsTmp{1}(2, 1).Min = [];
      saveVarsTmp{1}(2, 1).Max = [];
      saveVarsTmp{1}(2, 1).DimensionsMode = 'Fixed';
      saveVarsTmp{1}(2, 1).SamplingMode = 'Sample based';
      saveVarsTmp{1}(2, 1).SampleTime = -1;
      saveVarsTmp{1}(2, 1).DocUnits = 'V';
      saveVarsTmp{1}(2, 1).Description = ['Feed-forwards for SCR E converters' ...
        ' voltage commands'];
      saveVarsTmp{1}(3, 1) = Simulink.BusElement;
      saveVarsTmp{1}(3, 1).Name = 'hyb_FF_f_u';
      saveVarsTmp{1}(3, 1).Complexity = 'real';
      saveVarsTmp{1}(3, 1).Dimensions = 8;
      saveVarsTmp{1}(3, 1).DataType = 'single';
      saveVarsTmp{1}(3, 1).Min = [];
      saveVarsTmp{1}(3, 1).Max = [];
      saveVarsTmp{1}(3, 1).DimensionsMode = 'Fixed';
      saveVarsTmp{1}(3, 1).SamplingMode = 'Sample based';
      saveVarsTmp{1}(3, 1).SampleTime = -1;
      saveVarsTmp{1}(3, 1).DocUnits = 'V';
      saveVarsTmp{1}(3, 1).Description = ['Feed-forwards for SCR F converters' ...
        ' voltage commands'];
      
      testBus.Elements = saveVarsTmp{1};
    end
    end
end

