classdef SCDDSclass_parameter_test < SCDDStest
% Tests for SCDDSclass_parameter
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

  properties
    mypar
    myparfixdim
    myparidx
    mypartestdimandidx
    myparunlinked
    myparinvidx
    shot
  end
  
  methods(TestClassSetup)
    function setup_paths(~)
    end
  
    function test_constructor(testCase)
      env=SCDDSclass_env;
      mdsconfig   = env.get_mdsconfig();
      testCase.shot = env.get_testshot();
      
      testCase.mypar = SCDDSclass_mdsparnumeric(...
        'demo1.params.enable'        ,'enable'          ,...
        'mdshelp','a bool param',...
        'mdsvalid','1','srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree);
      testCase.mypar = testCase.mypar.setmdsinterface(mdsconfig.mdsinterface);

      testCase.myparfixdim = SCDDSclass_mdsparnumeric(...
        'demo1.params.fixdivector1'        ,'fixdimvect'          ,...
        'mdshelp','a fixdim param',...
        'mdsvalid','1','srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree, ...
        'dim',2);
      testCase.myparfixdim = testCase.myparfixdim.setmdsinterface(mdsconfig.mdsinterface);
      
      testCase.myparidx = SCDDSclass_mdsparnumeric(...
        'demo1.params.vectindex%02d'        ,'vectindex'          ,...
        'mdshelp','an indexed param',...
        'mdsvalid','1','srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree, ...
        'startidx',1,'stopidx',2);
      testCase.myparidx = testCase.myparidx.setmdsinterface(mdsconfig.mdsinterface);

      testCase.mypartestdimandidx = SCDDSclass_mdsparnumeric(...
        'demo1.params.vectindex%02d'        ,'vectindex'          ,...
        'mdshelp','an indexed param',...
        'mdsvalid','1','srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree, ...
        'dim',1,'startidx',1,'stopidx',2);
      testCase.mypartestdimandidx = testCase.mypartestdimandidx.setmdsinterface(mdsconfig.mdsinterface);
      
      testCase.myparunlinked = SCDDSclass_mdsparnumeric(...
        ''        ,'enable'          ,...
        'mdshelp','a bool param',...
        'mdsvalid','1','srcsrv',mdsconfig.mdsserver,'srctree',mdsconfig.mdstree);
      testCase.myparunlinked = testCase.myparunlinked.setmdsinterface(mdsconfig.mdsinterface);
      
      
      testCase.assertClass(testCase.mypar,'SCDDSclass_mdsparnumeric');
      testCase.assertClass(testCase.myparfixdim,'SCDDSclass_mdsparnumeric');
      testCase.assertClass(testCase.myparidx,'SCDDSclass_mdsparnumeric');
      testCase.assertClass(testCase.mypartestdimandidx,'SCDDSclass_mdsparnumeric');   
      testCase.assertClass(testCase.myparunlinked,'SCDDSclass_mdsparnumeric');
    end
  end
  
  methods(Access=private)
    
    function define_invertedstartidxstopidx(testCase)
         testCase.myparinvidx = SCDDSclass_mdsparnumeric(...
        'demo1.params.vectindex%02d'        ,'vectindex'          ,...
        'mdshelp','an indexed param',...
        'mdsvalid','1','srcsrv','','srctree','', ...
        'startidx',2,'stopidx',1);
    end
    
  end
  
  methods(Test,TestTags={'unit'})
    
    function test_printinfo(testCase)
       P1 = testCase.mypar;
       P2 = testCase.myparfixdim;
       P3 = testCase.myparidx;
       P4 = testCase.mypartestdimandidx;
       P5 = testCase.myparunlinked;
       
       P1.printinfo;
       P2.printinfo;
       P3.printinfo;       
       P4.printinfo;
       P5.printinfo;
    end
    
    function test_actualize(testCase)
       P1 = testCase.mypar;
       P2 = testCase.myparfixdim;
       P3 = testCase.myparidx;
       P4 = testCase.mypartestdimandidx;
       P5 = testCase.myparunlinked;

       % local tunable parameter with this structure
       TP = Simulink.Parameter;
       TPS = struct();
       TPS.enable = false;
       TPS.fixdimvect = single([0 0]);
       TPS.vectindex = int8([0 0]);
       TP.Value= TPS; % init
       assignin('base','TP',TP); % assign to base workspace
       P1 = P1.bind('base',[],'TP'); % bind parameter object to TP in base workspace
       P2 = P2.bind('base',[],'TP');
       P3 = P3.bind('base',[],'TP');
       P4 = P4.bind('base',[],'TP');
       P5 = P5.bind('base',[],'TP');
       
       testCase.assertFalse(TP.Value.enable);
       testCase.assertEqual(TP.Value.fixdimvect,single([0 0]));
       testCase.assertEqual(TP.Value.vectindex,int8([0 0]));
       
       % actualize from mds source
       P1.actualize(testCase.shot);
       P2.actualize(testCase.shot);
       P3.actualize(testCase.shot);
       P4.actualize(testCase.shot);
       P5.actualize(testCase.shot);
       
       % check that is is actualized
       testCase.assertTrue(TP.Value.enable);
       testCase.assertEqual(TP.Value.fixdimvect,single([1 2]));
       testCase.assertEqual(TP.Value.vectindex,int8([1 2]));  
       
       % exciting MARTe2 cfg entries
       fprintf('P1 MARTe2 is:\n%s\n',P1.genMARTe2entry(-1));
       fprintf('P2 MARTe2 is:\n%s\n',P2.genMARTe2entry(-1));
       fprintf('P3 MARTe2 is:\n%s\n',P3.genMARTe2entry(-1));     
       fprintf('P4 MARTe2 is:\n%s\n',P4.genMARTe2entry(-1));
       fprintf('P5 MARTe2 is:\n%s\n',P5.genMARTe2entry(-1));       
    end
    
    function test_mdsupload(testCase)
       P1 = testCase.mypar;
       P2 = testCase.myparfixdim;
       P3 = testCase.myparidx;
       P4 = testCase.mypartestdimandidx;
       P5 = testCase.myparunlinked;
       
       TP = Simulink.Parameter;
       TPS = struct();
       TPS.enable = false;
       TPS.fixdimvect = single([0 0]);
       TPS.vectindex = int8([0 0]);
       TP.Value= TPS; % init
       assignin('base','TP',TP); % assign to base workspace
       P1 = P1.bind('base',[],'TP'); % bind parameter object to TP in base workspace
       P2 = P2.bind('base',[],'TP');
       P3 = P3.bind('base',[],'TP');
       P4 = P4.bind('base',[],'TP');
       P5 = P5.bind('base',[],'TP');
       
       P1.autopopulatemds(1);
       P2.autopopulatemds(1);
       P3.autopopulatemds(1);
       P4.autopopulatemds(1);
       P5.autopopulatemds(1);
    end
   
    function test_invertedstartidxstopidx(testCase)
       testCase.verifyError(@() testCase.define_invertedstartidxstopidx(), 'SCDDSclass_MDSparameter:parseconstructor:startstopidxerror');
    end
    
  end
  
  
end
