classdef test_SCDDSclass < SCDDStest
  % test file for SCDDS interface class
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  properties
    expcode = 1;
    shot = 65668;
  end
  
  methods(TestClassSetup)
    function setup(testCase)
      testCase.assumeFail('skipping while expcode containers not defined in SCDDS-core')
    end
  end
  
  methods(Test,TestTags={'unit'})
    function test_load(testCase)
      SCDDS.load(testCase.expcode);
    end
    
    function test_init(testCase)
      SCDDS.init(testCase.expcode,testCase.shot);
    end
  end
end