## SCDDS-core

**Simulink Control Development and Deployment Suite**

This repository contains a set of tools (a library) to develop real-time control systems in Matlab/Simulink, specifically for pulsed devices like tokamaks.

This code was developed at the Swiss Plasma Center at EPFL Lausanne, originally for use on the TCV tokamak, over the period 2008-2022.

In 2022 the library was refactored to be independent of `TCV` and suitable for use for any tokamak control system. These tools are now distributed in open-source (see license below) in the hope of contributing to a standardization of the way we define, test and deploy control algorithms across fusion devices.

If this library finds its use outside tokamaks or fusion devices, that would also be welcome.

### Key features

The code consists of a set of functions and classes to define components of the control system. Usually, a matlab object (instantiation of a class) is associated with one (or more) Simulink models. The matlab object allows the user to perform common tasks for the Simulink model such as retrieving its parameters and signals (from a file or external parameter database), building it into an executable, or running (unit or integration) tests.

* Various control system components are defined: 
  * `algos` represenging functional blocks with algorithms. 
  * `wrappers` (running collections of algorithms), this is the unit that is normally compiled into an executable. 
  * `nodes` (collections of threads running on one computational node).
  * `expcode` a representation of the entire control system, being a collection of nodes.
It is not necessary to use all the components in this hierarcy, depending on the use-case, already using `algos` may be very useful.

* Classes for testing all these components.
* Interfaces to parameter and signal databases (like [`MDSplus`](www.mdsplus.org)) to load parameters and signals for a given experiment (shot).
* Methods to programmatically define and trigger the compilation of components to `C` code and deployment to a real-time framework. For now, [`MARTe2`](https://vcis-gitlab.f4e.europa.eu/aneto/MARTe2) is used as target framework.


### Use of the library
The normal use of this library consists of deriving classes for a specific instantiation of a control system.

For example the TCV control code is built using classes that derive from the superclasses from this library.

This library comes with a demo file (repository `SCDDS-demo` that contains a demo implementation of a simple control system, that is interfaced with an `MDSplus` database and deployed to the `MARTe2` real-time control system.

New users can first check out this demo.

### License
This code is distributed under the `LGPL v3.0-only` license.

This license allows you to develop derivatives works that use this library (by calling functions, derive classes, call binaries...) without needing to share it back. For example, you can develop (components of) a tokamak control system using these tools without the need to make it publicly available.
However any modifications and updates to the library _itself_ must be shared under the same LGPL license. Also if you copy components of the library into other codes, that code should be shared back under the same LGPL license.

The authors would greatly value if the modifications are shared back to the source repository at EPFL, see the `Contributing` section below.

The above is not legally binding text, the legally binding text is in the license file `LICENSE.md`.

Note that this software requires Matlab/Simulink, including toolboxes needed for C code generation (typically Embedded coder). Users must obtain their own licenses from Mathworks.

### Contributing
The code is hosted on [https://gitlab.epfl.ch/spc/scdds/scdds-core](https://gitlab.epfl.ch/spc/scdds/scdds-core) and can be freely cloned. 

You are very welcome to contribute to the code by requesting membership to the project, which allows you to read and post issues, or propose merge requests. Before proposing major changes or doing large developments, it is recommended to open an issue and discuss with other developers.

Issues suitable for beginning contributors are marked with the label `good_for_beginners`

### Main contributors:

* [Cristian Galperti (SPC-EPFL)](mailto:cristian.galperti@epfl.ch) (2014 - )
* [Federico Felici (SPC-EPFL)](mailto:federico.felici@epfl.ch) (2009 - )
* James Paley (SPC-EPFL) (2007-2011)
