classdef SCDDSclass_env
  %Sets up SCDDS enviroment
  %   Detailed explanation goes here
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
  methods(Static)
    function mdsconfig = get_mdsconfig()
      % return MDS config
      where = SCDDSclass_env.getsite();
      mdsconfig = SCDDSclass_mdsconfig(where);
    end
    
    function shot = get_testshot
      % return shot for testing
      where = SCDDSclass_env.getsite();
       switch where
        case {'PSFC','EPFL'}
          shot = -1;
        otherwise
          shot = -1;
      end
    end
    
    function where = getsite()
      [s,hostname] = system('hostname -A');
      if s, [~,hostname] = system('hostname'); end % alternative calls
      hostname = lower(deblank(hostname));
      if contains(hostname,'epfl.ch')
        where = 'EPFL';
      elseif contains(hostname,'mit.edu')
        where ='PSFC';
      else
        where = hostname;
        warning('don''t know how to treat hostname %s',hostname);
      end
    end
  end
end

