function [busObj,busName] = SCDconf_createRFMOUTextbus
% add special RFM out bus for MANTIS
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
busName = 'RFMOUTextbus';
busObj = Simulink.Bus;
busObj.Description = 'RFM bus from external nodes (MANTIS)';
Elems                = repmat(Simulink.BusElement,10,1);

Elems(1).Name        = 'MANTIS1DetachmentFront';
Elems(1).DataType    = 'single';
Elems(1).Description = '';

Elems(2).Name        = 'MANTIS1CIIIFront';
Elems(2).DataType    = 'single';
Elems(2).Description = '';

Elems(3).Name        = 'MANTIS1NIIFront';
Elems(3).DataType    = 'single';
Elems(3).Description = '';

Elems(4).Name        = 'MANTIS1HeIIFront';
Elems(4).DataType    = 'single';
Elems(4).Description = '';

Elems(5).Name        = 'MANTIS1IonisationFront';
Elems(5).DataType    = 'single';
Elems(5).Description = '';

Elems(6).Name        = 'MANTIS1IonisationRate';
Elems(6).DataType    = 'single';
Elems(6).Description = '';

Elems(7).Name        = 'MANTIS1MARFEFront';
Elems(7).DataType    = 'single';
Elems(7).Description = '';

Elems(8).Name        = 'MANTIS1ToBeDefined1';
Elems(8).DataType    = 'single';
Elems(8).Description = '';

Elems(9).Name        = 'MANTIS1ToBeDefined2';
Elems(9).DataType    = 'single';
Elems(9).Description = '';

Elems(10).Name        = 'MANTIS1FiducialFrameNr';
Elems(10).DataType    = 'single';
Elems(10).Description = '';


busObj.Elements    = Elems;
end