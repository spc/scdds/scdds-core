% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

function [out] = SCDconf_transferentry(entryname,srcddname,dstddname,dryrun)
% SCDtransferentry(entryname,srcddname,dstddname)
%   Transfers entryname entry from srcddname data dictionary
%   to destddname data dictionary, overwrites if
%   destination is already present
%   If dryrun is 1 (default) no operations are performed
%   The function is safe against multiple occurrences of the entry
%   in the sldd set, as it checks entry DataSource property
%   to match the proper data dictionary
%
%   Examples:
%   
%   dry run:
%   SCDconf_transferentry('WG0202bus','SCDwrap_tcv02standard.sldd','SCDwrap_tcv02detachment.sldd');
%
%   real:
%   SCDconf_transferentry('WG0202bus','SCDwrap_tcv02standard.sldd','SCDwrap_tcv02detachment.sldd',0);
%


if nargin==3
  dryrun=1;
end
dd_src=SCDconf_getdatadict(srcddname);
dd_dst=SCDconf_getdatadict(dstddname);
if ~dd_src.exist(entryname)
  error('entry not found in original dd');
else
  srcentry=dd_src.getEntry(entryname);
  found=false;
  for ii=1:numel(srcentry)
    if strcmp(srcentry(ii).DataSource,srcddname)
      srcentry=srcentry(ii);
      found=true;
      break;
    end
  end
  if ~found
    error('source entry %s not found in dd %s',entryname,srcddname);
  end
  disp('source entry is:');
  disp(srcentry);
  value=srcentry.getValue();
  replaceorcreateddentry(dd_dst,entryname,value,dryrun,dstddname);
end
out=[];
end

function replaceorcreateddentry(designDataObj,entryname,value,dryrun,dstddname)
if designDataObj.exist(entryname)
  dstentry = designDataObj.getEntry(entryname);
  
  found=false;
  for ii=1:numel(dstentry)
    if strcmp(dstentry(ii).DataSource,dstddname)
      dstentry=dstentry(ii);
      found=true;
      break;
    end
  end
  if ~found
    error('destination entry %s not found in dd %s',entryname,dstddname);
  end
  disp('destination entry is:');
  disp(dstentry);
  if isequal(dstentry.getValue,value)
    fprintf('keep old value of %s since not changed\n',entryname);
  else
    fprintf('replaced value of %s since it changed\n',entryname);
    if ~dryrun
      dstentry.setValue(value); % replace
    else
      disp('DRYRUN, operation skipped.');
    end
  end
else
  fprintf('   %s: added new %s\n',obj.getname, entryname);
  if ~dryrun
    designDataObj.addEntry(entryname,value);
  else
    disp('DRYRUN, operation skipped.');
  end
end
end