% This function helps creating template simulation input structures
% for From Workspaces blocks feeding bus signals
% Called with a Simulink.Bus object as an input it creates
% a template struct of timeseries filled with 2 timepoints [0,1]
% and with zero data of the correct kind
% This is enough for passing a ctrl-D with from Workspace bus kind blocks
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

function [out] = SCDconf_createstructfrombus(varargin)
% Create data structure matching a bus signature
% From either a datadictionary+busname or from a bus directly
% [out] = SCDconf_createstructfrombus(dd,busname)
% [out] = SCDconf_createstructfrombus(bus)

if nargin==1
  bus = varargin{1};
else
  dd = varargin{1};
  busName = varargin{2};
  assert(dd.exist(busName),'%s does not exist in dd',busName)
  busEntry=dd.getEntry(busName); % get from data dictionary
  if numel(busEntry)>1
    report = sprintf('multiple entries found with name %s\n Sources:\n',busName);
    for ii=1:numel(busEntry)
      report = [report,sprintf('%20s\n',busEntry(ii).DataSource)];
    end
    error(report);
  end
    
  bus = busEntry.getValue;
end
bussize=size(bus.Elements);
signalssize=bussize(1);

for ii=1:signalssize
  element=bus.Elements(ii);
  if contains(element.DataType,'Bus: ')
    busName = erase(element.DataType,'Bus: ');
    % recursive call
    out.(element.Name) = SCDconf_createstructfrombus(dd,busName);  
  else
    try 
        enumerated=false;
        if contains(element.DataType,'Enum: ')
            enumerated=true;
            elementtype=strrep(element.DataType,'Enum: ','');
        elseif ~strcmp(element.DataType,'boolean')
            elementtype=element.DataType;
            cast(1,elementtype);
        else
            elementtype='logical'; % it seems that boolean isn't supported by cast 
            cast(1,elementtype);
        end
    catch ME 
        error('non-numeric type'); 
    end
    elementdims=element.Dimensions;
    switch numel(elementdims)
      case 1
        data=zeros(elementdims(1),2)';
      case 2
        data=zeros(elementdims(1),elementdims(2),2);
      otherwise
        error('Not supported data structure');
    end
    if ~enumerated
      out.(element.Name) = ...
        timeseries(cast(data,elementtype),[0;1],'Name',element.Name);
    else
      elementtypefcn=str2func(elementtype); 
      out.(element.Name) = ...
        timeseries(elementtypefcn(data),[0;1],'Name',element.Name);      
    end
  end
end

%out=Simulink.SimulationData.createStructOfTimeseries(bus,tempstruct)