function SCDconf_setConf(conf,sourcedd)
% SCDconf_setConf(conf,sourcefile)
%   conf: 'SIM', 'CODE' or use custom conf name
%   sourcedd: source data dictionary containing configurations (Default: configurations_container.sldd)
%
% Sets SCD Simulink configuration settings.
% Takes desired configurationSettings object from sourcefile
% Stores it in configurations/configurations.sldd as configurationSettings
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

switch lower(conf)
  case 'sim'
    confName = 'configurationSettingsSIM';
  case 'code'
    confName = 'configurationSettingsCODEgcc';
  otherwise
    confName = conf; % custom input
end

if nargin==2
  fname_src = sourcedd;
else
  fname_src = 'configurations_container.sldd'; % dd file name
end
assert(~isempty(which(fname_src)),'%s not in path?',fname_src);
fprintf('Retrieving %s from %s, ',confName,fname_src)
fprintf('setting it as configurationSettings in base workspace\n');

dd_src      = Simulink.data.dictionary.open(fname_src);
confSection = getSection(dd_src, 'Configurations');
assert(confSection.exist(confName),'%s does not exist in %s',confName,fname_src);
conf  = confSection.getEntry(confName);
configurationSettings      =  conf.getValue;

%% Set it in target file
% Change name to universal name
configurationSettings.Name = 'configurationSettings';

% set custom src - later make this a class
thisdir = fileparts(mfilename('fullpath'));
includeDir = fullfile(thisdir,'..','..','codegen');
configurationSettings.set_param('CustomInclude',includeDir)

assignin('base','configurationSettings',configurationSettings)

%% Create empty configurations.sldd temporarily while algo dds need it
if ~exist('configurations.sldd','file')
    dir_target = fileparts(which(fname_src)); % target dir as source dir
    handle = Simulink.data.dictionary.create(fullfile(dir_target,'configurations.sldd'));
end

end

