% bus2martecfg tester
%
% Target cfg file snippet to be automatically obtained
%
% outputbus1 = {
%  sig1 = { Type = float32 NumberOfDimensions = 0 NumberOfElements = 1 DataSource = DDB1 Alias = outputbus1sig1 }
%  sig2 = { Type = float32 NumberOfDimensions = 0 NumberOfElements = 1 DataSource = DDB1 Alias = outputbus1sig2 }
%  sig3 = { Type = int32   NumberOfDimensions = 0 NumberOfElements = 1 DataSource = DDB1 Alias = outputbus1sig3 }
%  subbus1 = {
%    sig1 = { Type = float32 NumberOfDimensions = 0 NumberOfElements = 1 DataSource = DDB1 Alias = outputbus1sig4 }
%    sig2 = { Type = float32 NumberOfDimensions = 0 NumberOfElements = 1 DataSource = DDB1 Alias = outputbus1sig5 }                    
%  }
% }
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

% Test bus definition
genstruct=struct;
genstruct.sig1=single(zeros(2,2));
genstruct.sig2=single(0);
genstruct.sig3=int32(0);
gensubstruct=struct;
gensubstruct.sig1=single(0);
gensubstruct.sig2=single(0);
genstruct.subbus1=gensubstruct;
genstruct.sig4=double(zeros(2,1));

busInfo=Simulink.Bus.createObject(genstruct);
testbus=eval(busInfo.busName);

% Function call
bus2marte2cfg(testbus, 'rootbus','DDB1',1);
