function [] = genbusflatstub1(cfgfile,siglist,datasource,vectorizematrices)
% genbugflatstub1(cfgfile,siglist,datasource,vectorizematrices)
%
%  this function generates MARTe2 cfg files stubs useful to build MARTe2
%  cfg files automatically
%
%  inputs:
%   cfgfile           cfg stub will be written to this filename
%   siglist           cell matrix of signals information, each row is 
%                     {signalname, numberofdimensions, , numberofelements}
%   datasource        datasource name
%   vectorizematrices =1 will force numberofdimensions=1 for matrices,
%                     default=0
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
if nargin<4
  vectorizematrices=false;
end

fid=fopen(cfgfile,'w');
for i=1:numel(siglist)
 ndim = cell2mat(siglist{i}{3});
 if vectorizematrices && ndim==2
   ndim=1;
 end
 fprintf(fid, "%s = { DataSource = %s Type = %s NumberOfDimensions = %d NumberOfElements = %d }\n", ...
  char(siglist{i}{1}),datasource,char(siglist{i}{2}),ndim,cell2mat(siglist{i}{4}));   
end

end

