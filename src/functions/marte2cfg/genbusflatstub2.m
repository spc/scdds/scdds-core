function [] = genbusflatstub2(cfgfile,siglist,mdspreamble,mdsperiod,mdsseglength)
% genbusstub2
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
fid=fopen(cfgfile,'w');
for i=1:numel(siglist)
 fprintf(fid,"%s = { NodeName =""%s.%s"" Period = %s AutomaticSegmentation = 0 MakeSegmentAfterNWrites = %s SamplePhase = 0 } \n",char(siglist{i}{1}),mdspreamble,char(siglist{i}{5}),mdsperiod,mdsseglength);
end
end

