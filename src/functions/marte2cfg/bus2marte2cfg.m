function [signallist] = bus2marte2cfg(bus, name, datasourcename, fid, varargin)
%Translates a simulink bus object into MARTe2 cfg description
%   This function takes a Simuink.Bus object
%   as input and prints out its MARTe2 configuration
%   counterpart.
%   Inputs:
%     bus: the Simulink.Bus object to be analyzed
%     name: its root name
%     datasourcename: name of the datasource to insert
%     fid: target fid for fprintf (fopen result or 1 for screen output)
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
if nargin<=4
    assert(isa(bus,'Simulink.Bus'),'bus must be a Simulink.Bus object');
    depth=0;
    assignin('base','tempbus',bus);
    busstruct=evalin('base','Simulink.Bus.createMATLABStruct(''tempbus'')');
    evalin('base','clear tempbus');
    basealiasname=name;
    signallist={};
else
    depth=varargin{1};
    busstruct=bus;
    basealiasname=varargin{2};
    signallist=varargin{3};
end
spacer='';
for ii=1:depth, spacer=[spacer ' ']; end
    
if isa(busstruct,'struct')
    % called on a bus, scan into
    fprintf(fid,"%s%s={\n",spacer,name);
    f=fields(busstruct);
    for ii=1:numel(f)
       if isa(busstruct.(f{ii}),'struct')
         signallist=bus2marte2cfg(busstruct.(f{ii}),f{ii},datasourcename,fid,depth+1,[basealiasname '-' f{ii}],signallist);
       else
         martetype=type2marte2(busstruct.(f{ii}));
         martedims=dims2marte2(busstruct.(f{ii}));
         marteelems=elems2marte2(busstruct.(f{ii}));
         fprintf(fid,"%s %s = { Type = %s NumberOfDimensions = %d NumberOfElements = %d DataSource = %s Alias = %s }\n",...
             spacer,f{ii},martetype,martedims,marteelems,datasourcename,...
             [basealiasname '-' f{ii}]);
         signalinfo={{[basealiasname '-' f{ii}]} {martetype} {martedims} {marteelems} {f{ii}}};
         signallist{end+1}=signalinfo;
       end 
    end
    fprintf(fid,"%s}\n",spacer);
else
    error('SCDDScore:bus2marte2cfg', 'Wrong input datatype in bus parsing');
end

end

