function [marte2type] = type2marte2(input)
%Returns MARTe2 type of the input, if supported
%   Returnd MARTe2 type of the input, if supported
%   accordind to this map
%   | C type name     | Model    | MARTe     |
%   | :-------------- | :------- | :-------- |
%   | `unsigned char` | `uint8`  | `uint8`   |
%   | `signed char`   | `int8`   | `int8`    |
%   | `char`          | `int8`   | `int8`    |
%   | `unsigned short`| `uint16` | `uint16`  |
%   | `short`         | `int16`  | `int16`   |
%   | `unsigned int`  | `uint32` | `uint32`  |
%   | `int`           | `int32`  | `int32`   |
%   | `float`         | `single` | `float32` |
%   | `double`        | `double` | `float64` |
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

if      isa(input,'uint8')
        marte2type='uint8';
elseif  isa(input,'int8')
        marte2type='int8';
elseif  isa(input,'uint16')
        marte2type='uint16';
elseif  isa(input,'int16')
        marte2type='int16';
elseif  isa(input,'uint32')
        marte2type='uint32';
elseif  isa(input,'int32')
        marte2type='int32';
elseif  isa(input,'single')
        marte2type='float32';
elseif  isa(input,'double')
        marte2type='float64';
else
        warning('SCDDScore:type2marte2','not available type conversion');
        marte2type='n/a';

end

