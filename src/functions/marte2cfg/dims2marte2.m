function [numberofdimensions] = dims2marte2(input)
%Returns the MARTe2 NumberOfDimensions field of input
%   MARTe2 defines this:
%   scalars:  NumberOfDimensions = 0
%   vectors:  NumberOfDimensions = 1
%   matrices: NumberOfDimensions = 2
%
% https://vcis.f4e.europa.eu/marte2-docs/master/html/core/gams/datasource.html?highlight=numberofdimensions
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

if isscalar(input)
    numberofdimensions=0;
elseif isvector(input)
    numberofdimensions=1;
elseif ismatrix(input)
    numberofdimensions=2;
else
    warning('SCDDScore:dim2marte2','NumberOfDimensions not supported');
    numberofdimensions=-1;
end

end

