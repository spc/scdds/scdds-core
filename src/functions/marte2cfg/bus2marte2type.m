function bus2marte2type(bus, name, fid) 
%Translates a simulink bus object into MARTe2 IntrospectionStructure
%declarator
%   This function takes a Simuink.Bus object
%   as input and prints out its MARTe2 configuration
%   counterpart to be included into a type definition cfg block
%   Inputs:
%     bus: the Simulink.Bus object to be analyzed
%     name: its root name
%     fid: target fid for fprintf (fopen result or 1 for screen output)
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

    [typeslist,signalslist,signalidx] = bus2marte2typescan(bus, name, fid);

    for i=numel(typeslist):-1:1
        fprintf(fid," +%s={\n",typeslist{i}{1});
        fprintf(fid,"  Class = IntrospectionStructure\n");
        signallist=signalslist{typeslist{i}{2}};
        for j=1:numel(signallist)
            fprintf(fid,"  %s={ Type= %s ",signallist{j}{1},signallist{j}{2});
            if ~isempty(signallist{j}{3})
                fprintf(fid,"NumberOfElements = %s ", signallist{j}{3});
            end
            fprintf(fid,"}\n");
        end
        fprintf(fid," }\n");
    end
       
end

function [typeslist,signalslist,signalidx] = bus2marte2typescan(bus, name, fid, varargin)

    
if nargin<=3
    assert(isa(bus,'Simulink.Bus'),'bus must be a Simulink.Bus object');
    depth=0;
    assignin('base','tempbus',bus);
    busstruct=evalin('base','Simulink.Bus.createMATLABStruct(''tempbus'')');
    evalin('base','clear tempbus');
    typeslist={};
    signalslist={};
    signalidx=0;
else
    depth=varargin{1};
    busstruct=bus;
    typeslist=varargin{2};
    signalslist=varargin{3};
    signalidx=varargin{4};
end
spacer='';
for ii=1:depth, spacer=[spacer ' ']; end
    
if isa(busstruct,'struct')
    % called on a bus, scan into

    f=fields(busstruct);
    if depth==0
        typeslist{end+1}{1}=name;
    else
        typeslist{end+1}{1}=['type' name];
    end
    signalidx=signalidx+1;
    signalslist{end+1}={};
    typeslist{end}{2}=signalidx;
    levelsignalidx=signalidx;
    for ii=1:numel(f)
       if isa(busstruct.(f{ii}),'struct')
         signalinfo={f{ii}, ['type' f{ii}], ''};  
         signalslist{levelsignalidx}{end+1}=signalinfo;
         [typeslist,signalslist,signalidx]=bus2marte2typescan(busstruct.(f{ii}),f{ii},fid,depth+1,typeslist,signalslist,signalidx);
       else
         martetype=type2marte2(busstruct.(f{ii}));
         martedims=dims2marte2typestr(busstruct.(f{ii}));
         signalinfo={f{ii}, martetype, martedims};
         signalslist{levelsignalidx}{end+1}=signalinfo;
       end 
    end
else
    error('SCDDScore:bus2marte2cfg', 'Wrong input datatype in bus parsing');
end

end

