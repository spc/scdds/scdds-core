function [flatsignals] = genbusstructstub(cfgfile,bus,rootname,ddbname)
% genbusstructstub
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
fid=fopen(cfgfile,'w');
flatsignals=bus2marte2cfg(bus,rootname,ddbname,fid);
fclose(fid);
end

