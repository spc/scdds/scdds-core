function [numberofelements] = elems2marte2(input)
%Returns the MARTe2 NumberOfElements of input 
%   NumberOfElements is the total number
%   of elements of the data
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

nd=dims2marte2(input);

if nd==0
    % scalar
    numberofelements=1;
elseif nd==1
    numberofelements=numel(input);
elseif nd==2
    numberofelements=numel(input);
else
    warning('SCDDScore:elems2marte2','NumberOfElements not supported');
end    

end

