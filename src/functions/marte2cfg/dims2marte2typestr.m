function [dimensionstr] = dims2marte2typestr(input)
%Returns the MARTe2 NumberOfElements for IntrospectionStructure
%instantiations
%   MARTe2 defines this:
%   scalars:  dimensionstr = 1
%   vectors:  dimensionstr = numel(vector)
%   matrices: dimensionstr = {rows,cols}
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

if isscalar(input)
    dimensionstr='1';
elseif isvector(input)
    dimensionstr=num2str(numel(input));
elseif ismatrix(input)
    s=size(input);
    dimensionstr=['{' num2str(s(1)) ',' num2str(s(2)) '}'];
else
    warning('SCDDScore:dims2marte2type','Data dimension not supported');
    dimensionstr='';
end

end

