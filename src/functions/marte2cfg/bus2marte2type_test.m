% bus2martecfg tester
%
% Target cfg file snippet to be automatically obtained
%
%  * +Types = {
%  *     Class = ReferenceContainer
%  * 
%  *     +BusElementStruct = {                       // SubStruct
%  *         Class = IntrospectionStructure
%  *         SubElement1 = {                         // Scalar
%  *             Type               = uint16
%  *             NumberOfElements   = 1
%  *         }
%  *         SubElement2 = {                         // Matrix (2x2)
%  *             Type               = float64
%  *             NumberOfElements   = {2, 2}
%  *         }
%  *     }
%  * 
%  *     +BusSignalStruct = {
%  *         Class = IntrospectionStructure
%  *         BusElement1 = {                         // Vector (10 elements)
%  *             Type               = uint32
%  *             NumberOfElements   = 10
%  *             DataSource = DDB1
%  *         }
%  *         BusElement2 = {
%  *             Type = BusElementStruct
%  *         }
%  *     }
%  * }
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.


% Test bus definition
genstruct=struct;
genstruct.sig1=single(zeros(2,2));
genstruct.sig2=single(0);
genstruct.sig3=int32(0);
gensubstruct=struct;
gensubstruct.sig1=single(0);
gensubstruct.sig2=single(0);
genstruct.subbus1=gensubstruct;
genstruct.sig4=double(zeros(2,1));

busInfo=Simulink.Bus.createObject(genstruct);
testbus=eval(busInfo.busName);

% Function call
bus2marte2type(testbus, 'type1',1)
