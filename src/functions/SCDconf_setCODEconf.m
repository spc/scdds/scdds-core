% Sets the code generation configuration settings on all the models
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

function [] = SCDconf_setCODEconf()
    SCDconf_setConf('CODE')
end
