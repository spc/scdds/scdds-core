% Compare the structure of a bus to that of a structure, return 1 if they
% are the same.
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

function [areequal] = SCDconf_structbuscmp(mystruct,mybus)

nelem=numel(mybus.Elements);

for ielem = 1:nelem
  myelem = mybus.Elements(ielem);
  myelementname = myelem.Name;
  if ~isfield(mystruct,myelementname)
    areequal = false; return;
  end
  
  myfield = mystruct.(myelementname);
  if startsWith(myelem.DataType,'Bus')
    % recursive call
    areequal = SCDconf_structbuscmp(mystruct,mybus);
    if ~areequal; return; end
  elseif ~isa(myfield,'timeseries') % check timeseries
    areequal=false; return;
  elseif ~isa(myfield.Data,myelem.DataType) % check data type
    areequal=false; return;
  elseif ~all(size(myfield.Data,2:ndims(myfield.Data))==myelem.Dimensions) % check dimension
    areequal=false; return;    
  end
end

% if we made it this far..
areequal = true;
end
