% Gets tcv data dictionary design data as a object in base workspace
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
function [out] = SCDconf_getdatadict(varargin)
if nargin==0
    d=Simulink.data.dictionary.open('tcv.sldd');
    dd=getSection(d, 'Design Data');
    out=dd;
elseif nargin==1
    d=Simulink.data.dictionary.open(varargin{1});
    dd=getSection(d, 'Design Data');
    out=dd;
else
    error('Too many input params');
end