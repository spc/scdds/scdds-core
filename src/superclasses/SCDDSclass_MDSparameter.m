classdef (Abstract) SCDDSclass_MDSparameter < SCDDSclass_parameter
  % Superclass for MDS+ parameters
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  
  properties (SetAccess = protected)
    srcsrv            % The MDS+ server hosting the parameter
    srctree           % The MDS+ Tree hosting the parameter
    srctdimodel       % TDI expression to retrieve data when invoked with -1 shot
    srctdishot        % TDI expression to retrieve data when invoked with a give shotno, if empty the -1 is used
    cfgtdiexpr        % TDI expression before Target dimension or Start/Stop index mangling
    tdiexprused       % TDI expression actually used
    tdiexprlog        % TDI expression logged
    mdshelp           % MDS help string used in tree autopopulation
    mdsvalid          % MDS validation string used in autopopulation
    MDSinterface  MDSplus_interface = MDSplus_java;  % MDSplus inteface class
    ovrshotenable     % =1 if instantiated with overridden shot number, in this case this shot number will be always used during actualization
    overrideshot      % shot number in case of overridden shot number
    actualizeshot     % shot given via the actualize method
    usedshot          % shot used as source of the data
    targetdim         % Target dimension, if ~= 0, value is zero padded or trimmed to cope with this dimension
                      % Care must be taken here as dimension trim/pad is
                      % performed directly with TDI calls. This brings in
                      % these drawbacks:
                      % 1. It works only with the MDS Thin client and
                      %    remote TDI code must be executed
                      % 2. Data is trimmed by addit to the original path a
                      %    indexing postfix like [0:targetdim-1]
                      %    Care must be taken with signals node, always
                      %    embrace the path into a data() or dim_of()
                      %    TDI function
    startidx          % Start index for indexed fetching
    stopidx           % Stop index for indexed fetching
                      % It these 2 indexes are specified, C like token substitution is performed
                      % on the source path, which must contain a %d like token (%0<n>d being the
                      % most indicated). Also here, data retireval works by remote TDI expression call
                      % on the server, hence it works only with Thin client
                      
  end
  
  
  properties(Constant)
    marte2dataori     = 'ColumnMajor'; % data orientation for >1 D data
                                         % it can be either RowMajor or
                                         % ColumnMajor to be included in
                                         % MARTe2 cfg file  
                                         % Note that in a conventional
                                         % MATLAB -> MDSplus -> MARTE2
                                         % workflow, being MATLAB
                                         % ColumnMajor, this should never be changed
                                         % However it is given here for
                                         % completeness, since the MDSPlus ->
                                         % MARTe2 loader class interface
                                         % supports both encoding standards
  end
  
  properties
    verbose    uint8                    % Verbosity of the class
    clienttype string = 'Distributed';  % MDSplus client type (either 'Thin' or 'Distributed'
    skipnotok  logical = false;         % If true, not ok loading phase would not make MARTe2 fail but just invalidate the parameter
  end
  
  methods
    
    function obj=SCDDSclass_MDSparameter(mdssrcnode, destparam, varargin)
      cparser=inputParser;
      addRequired(cparser,'srctdimodel',@(x) ischar(x));
      addRequired(cparser,'destparam',@(x) ischar(x));
      addParameter(cparser,'srcsrv','',@(x) ischar(x));
      addParameter(cparser,'srctree','',@(x) ischar(x));
      addParameter(cparser,'srctdishot','',@(x) ischar(x));
      addParameter(cparser,'modelname','',@(x) ischar(x));
      addParameter(cparser,'datadictname','',@(x) ischar(x));
      addParameter(cparser,'modeltpstruct','',@(x) ischar(x)); % desttp ?
      addParameter(cparser,'skippable','false',@(x) ischar(x));
      addParameter(cparser,'shot',NaN,@(x) isnumeric(x) && isscalar(x));
      addParameter(cparser,'mdshelp','',@(x) ischar(x));
      addParameter(cparser,'mdsvalid','1',@(x) ischar(x));
      addParameter(cparser,'dim',0,@(x) isnumeric(x) && isscalar(x) && x>0);
      addParameter(cparser,'startidx',0,@(x) isnumeric(x) && isscalar(x) && x>0);
      addParameter(cparser,'stopidx',0,@(x) isnumeric(x) && isscalar(x) && x>0);
      obj.verbose=1;
      
      obj.sourcelogicaltype = 'uint8'; % generated C code uses uint8 to handle logicals
      
      % Constructor parser customization definitions here
      obj=obj.parseconstructor(cparser,mdssrcnode, destparam, varargin{:});
      % Constructor parser customization results here
      obj.classname=class(obj);
      % Default MDS interface to java
      obj.MDSinterface=MDSplus_java;
    end
    
    function actualize(obj, varargin)
      if nargin~=2
        error('SCDDScore:MDSparameter','actualize needs a shot number as first parameter');
      end
      if nargin && ~isnumeric(varargin{1})
        error('SCDDScore:MDSparameter','shot number is not numeric');
      end
      obj.actualizeshot=varargin{1};
      if obj.targetdim~=0 && obj.startidx~=0 && obj.stopidx~=0
        warning('SCDDSclass_MDSparameter:ParametersError','obj.targetdim~=0 && obj.startidx~=0 && obj.stopidx~=0 not supported, actuialization skipped'); 
      else
        actualize@SCDDSclass_parameter(obj, varargin);
      end
    end
    
    function [obj,value] = getdata(obj, varargin)
      obj=obj.actualizegetcmd(obj.usedshot);
      value=eval(obj.getcommand);
      if(~isnumeric(value))
        warning('SCDclass_mdspar:MDSerror','MDS+ error for parameter %s: %s. Actualization skipped.',obj.fullparam, value);
        obj.actualizable=false;
      else
        obj.actualizable=true;
      end
    end
    
    function obj = connect(obj, shot)
      ok=obj.MDSinterface.connect(obj.srcsrv);
      if ~ok, obj.actualizable=false; return; end
      % If the class has been declared with a 'shot' parameter, then
      % the shotnumber used for param loading is overridden by this
      % parameter, otherwise it is taken from the global shot number
      % given to the actualize command
      if nargin==1
        if obj.ovrshotenable == 1
          obj.usedshot = obj.overrideshot;
        else
          obj.usedshot = obj.actualizeshot;
        end
      else
        obj.usedshot = shot;
      end
      ok=obj.MDSinterface.open(obj.srctree, obj.usedshot);
      if ~ok, obj.actualizable=false; end
    end
    
    function printinfo(obj)
      fprintf('%s (class %s):\n', obj.fullparam, obj.classname);
      fprintf('  Simulink model: ''%s'', data dictionary: ''%s''\n', obj.modelname, obj.datadictname);
      if(~obj.unlinked)
        fprintf('  MDS+ source server: ''%s'', Tree: ''%s'', Interface: %s\n', ...
          obj.srcsrv, obj.srctree, class(obj.MDSinterface));
        fprintf('  MDS+ TDI expressions, model: ''%s''', obj.srctdimodel);
        if(strcmp(obj.srctdimodel, obj.srctdishot))
          fprintf(', shot: same');
        else
          fprintf(', shot: ''%s''', obj.srctdishot);
        end
        if obj.ovrshotenable == 1
          fprintf(', WARNING: shot number override to %d.\n', obj.overrideshot);
        else
          fprintf('.\n');
        end
        % specialized target dim and wildcard substituion print parts
        if obj.targetdim>0 && obj.startidx==0 && obj.stopidx==0
          fprintf('  Fixed target dimension loading. Dim = %d.\n',obj.targetdim);
        end
        if obj.targetdim==0 && obj.startidx>0 && obj.stopidx>0
          fprintf('  Wildcard subsitution loading. StartIdx = %d, StopIdx = %d.\n',obj.startidx,obj.stopidx);
        end
        if obj.targetdim>0 && obj.startidx>0 && obj.stopidx>0
          warning('SCDDSclass_MDSparameter:ParametersError','Invalid contextual target dimension and indexed wildcard parametrization');
        end
      else
        fprintf('  UNLINKED!\n');
      end
    end
    
    function [srcsrv] = getMDSserver(obj)
      srcsrv = obj.srcsrv;
    end
    
    function [srctree] = getMDStree(obj)
      srctree = obj.srctree;
    end
         
    function name=getmodelname(obj)
      name=obj.modelname;
    end
    
    function obj = setmodelname(obj, modelname)
      assert(nargout==1,'must set out argument for set method')
      obj.modelname = modelname;
    end
    
    function obj = setdatadictionary(obj, ddname)
      assert(nargout==1,'must set out argument for set method')
      obj.datadictname = ddname;
    end
    
    function obj = setloadpermissivity(obj, permissivity)
      assert(nargout==1,'must set out argument for set method')
      obj.loadpermissivity = permissivity;
    end
    
    function obj = setmdsinterface(obj,mdsinterface)
      obj.MDSinterface = mdsinterface;
    end
  end
  
  methods(Sealed) % works for heterogeneous class arrays
    function out = getparamtpstruct(obj)
      if numel(obj) == 1
        out = obj.paramtpstruct;
      else
        out = {obj.paramtpstruct}';
      end
    end
    
    function out = gettargetparammarte(obj)
      % convert model param list into version with marte separators
      out = strrep(obj.fullparam, obj.matlabseparator, obj.marteseparator);
    end
    
    function obj = bind(obj,modelname,datadictname,exportedtp,load_permissivity)
      for ii=1:numel(obj)
        obj(ii) = obj(ii).setmodelname(modelname);
        obj(ii) = obj(ii).setdatadictionary(datadictname);
        obj(ii) = obj(ii).setparamstructure(exportedtp);
        if nargin>4
          obj(ii) = obj(ii).setloadpermissivity(load_permissivity);
        end
      end
    end
  end
  
  methods
    function entrystring = genMARTe2entry(obj, shot)
      obj=obj.actualizetdiexpr(shot);
      entrystring=obj.genMARTe2entrypreamble(shot);
      if obj.targetdim==0 && obj.startidx==0 && obj.stopidx==0
        % standard plain case
      elseif obj.targetdim>0 && obj.startidx==0 && obj.stopidx==0
        % fixed target dimension case
        entrystring=[entrystring sprintf(' Dim=%d',obj.targetdim)]; 
        % distributed client warning
        if obj.clienttype=="Distributed"
          warning('SCDDSclass_MDSparameter:UnsupportedFeature','Parameter %s, MARTe2 will not support Dim~=0 in distributed client mode',obj.gettargetparammarte);
        end
      elseif obj.targetdim==0 && obj.startidx>0 && obj.stopidx>0
        % indexed fetching
        entrystring=[entrystring sprintf(' StartIdx=%d StopIdx=%d',obj.startidx,obj.stopidx)];
        if obj.clienttype=="Distributed"
          warning('SCDDSclass_MDSparameter:UnsupportedFeature','Parameter %s, MARTe2 will not support StartIdx~=0 StopIdx~=0 in distributed client mode',obj.gettargetparammarte);
        end
      else
        % not supported configuration
        warning('SCDDSclass_MDSparameter:ParametersError','Invalid contextual target dimension and indexed wildcard parametrization');
      end
      entrystring=[entrystring ' }'];
    end

    function entrystring = genMARTe2entrypreamble(obj, shot)
      obj=obj.actualizetdiexpr(shot);
      entrystring = sprintf('+%-50s = { Class=%-16s Path=%-40s DataOrientation=%s',obj.gettargetparammarte,obj.marteclassname,obj.genMARTe2MDStdiexpression,obj.marte2dataori);
    end
    
    function str = genMARTe2MDSsourcestr(obj)
      str = sprintf(' +Connection_%s_%s = {\n  Class=MDSObjConnection\n  ClientType="%s"\n  SkipActualizeErrors=%d\n  Server="%s"\n  Tree="%s"',...
        strrep(obj.srcsrv,'.','-'),strrep(obj.srctree,'.','-'),obj.clienttype,obj.skipnotok==true,obj.srcsrv,obj.srctree);
    end
    
    function str = genMARTe2MDStdiexpression(obj)
      % Duplicate first backslash
      %             if(obj.tdiexprused(1)=='\' && not(obj.tdiexprused(2)=='\'))
      %                 martetdi=['\' obj.tdiexprused];
      %             else
      %                 martetdi=obj.tdiexprused;
      %             end
      % Duplicate backslashes
      martetdi=strrep(obj.cfgtdiexpr, '\', '\\');
      %substitute every " with '
      martetdi=strrep(martetdi, '"', '''');
      %put double string quota
      martetdi=['"' martetdi '"'];
      str=martetdi;
    end
    
    function str = genTDIpopulatecmd(obj)
      obj=obj.actualizegetcmd('%s', 1);
      tdistr = 'rtcalgo_putparam(';
      tdistr = [tdistr '''' obj.tdiexprused ''','];
      str=tdistr;
    end
    
    function autopopulatemds(obj, shot)
      if obj.unlinked
        fprintf('%s UNLINKED, skipping.\n', obj.fullparam);
        return
      end
      if obj.targetdim==0 && obj.startidx==0 && obj.stopidx==0
        obj=obj.actualizegetcmd(shot);
        node = obj.tdiexprused;
        
        % workspace version of tunparams
        valsrc = [obj.paramtpstruct '.Value.' obj.destparam];
        val = evalin('base', valsrc);
        help = obj.mdshelp;
        
        % template version of tunparams
        %valsrc = [obj.paramtpstructt '_tmpl.Value.' obj.destparam]
        %val = Simulink.data.evalinGlobal(obj.modelname, valsrc);
  
        obj.populatemdsnode_param(shot, node, val, valsrc, help);
  
      elseif obj.targetdim>0 && obj.startidx==0 && obj.stopidx==0
        % dim upload, equal to the previus except that TDI is not expanded
        obj=actualizetdiexpr(obj, shot);
        node=obj.cfgtdiexpr;
        
        % workspace version of tunparams
        valsrc = [obj.paramtpstruct '.Value.' obj.destparam];
        val = evalin('base', valsrc);
        help = obj.mdshelp;
        
        obj.populatemdsnode_param(shot, node, val, valsrc, help);
        
      elseif obj.targetdim==0 && obj.startidx>0 && obj.stopidx>0
        % indexed upload
        obj=actualizetdiexpr(obj, shot);
        
        % workspace version of tunparams
        valsrc = [obj.paramtpstruct '.Value.' obj.destparam];
        val = evalin('base', valsrc);
        help = obj.mdshelp;
        
        tdiexpr=strrep(obj.cfgtdiexpr,'\','\\');
        for ii=obj.startidx:obj.stopidx
          exptdiexpr=sprintf(tdiexpr,ii);
          obj.populatemdsnode_param(shot, exptdiexpr, val(ii), [valsrc sprintf('(%d)',ii)], help);
        end        
      else
         warning('SCDDScore:MDSparameter','autopopulatemds not implemented for Dim, && StartIdx a&& StopIdx ~= 0 case');        
      end      
    end
    
    function populatemdsnode_param(obj, shot, node, val, valsrc, help)
      % original TDI:
      %   _nodeval = _node//'.value';
      %   _nodecom = _node//'.comment';
      %   _nodevalid = _validity;
      %   _expr = 'build_param('//_nodeval//','//_nodecom//','//_nodevalid//')';
      %   TreePut(_node,_expr);
      %   TreePut(_nodeval,'$', _value);
      %   TreePut(_nodecom,'$', _comment);
      
      nodeval = [node '.value'];
      nodecom = [node '.comment'];
      fprintf('Changing MDS node: ''%s'' <- %s\n',node, valsrc);
      obj.connect(shot);
      writeval = ['build_param(' nodeval ',' nodecom ',' obj.mdsvalid ')'];
      ok=obj.MDSinterface.put(node,writeval);
      if ~ok
        warning('SCDDSclass_MDSparameter:autopopulatemds:build_param_write_error','Error writing node build_param');
      end
      ok=obj.MDSinterface.put(nodecom,'$',help);
      if ~ok
        warning('SCDDSclass_MDSparameter:autopopulatemds:help_write_error','Error writing mds help');
      end
      writeval=obj.casttomds(val);
      % logical type conversion, TODO take logical type from config
      % somewhere ?
      if islogical(writeval), writeval=uint8(writeval); end
      ok=obj.MDSinterface.put(nodeval,'$',writeval);
      if ~ok
        warning('SCDDSclass_MDSparameter:autopopulatemds:value_write_error','Error writing mds value');
      end
    end
  end
  
  methods(Access = protected)   
    function obj=pregetdatacustom(obj)
        % Opening the tree
        obj=obj.connect;
    end
    
    function dispactualize(obj)      
      fprintf('Actualizing parameter: ''%s''', obj.fullparam)
      if ~obj.unlinked
        if obj.verbose==1
          if ~isempty(obj.usedshot)
            fprintf(' <- ''%s'' (%s, shot %d)\n', ...
              obj.tdiexprlog, obj.classname, obj.usedshot);
          else
            fprintf(' <- ''%s'' (%s, shot N/A)\n', ...
              obj.tdiexprlog, obj.classname);
          end
        end
      else
        fprintf(' parameter unlinked, SKIPPED!\n');
      end
    end
    
    function obj=parseconstructor(obj, cparser, varargin)
      parse(cparser,varargin{:});
      P = cparser.Results;
      
      obj.srcsrv = P.srcsrv;
      obj.srctree = P.srctree;
      obj.srctdimodel = P.srctdimodel;
      if isempty(P.srctdishot)
        obj.srctdishot = P.srctdimodel;
      else
        obj.srctdishot = P.srctdishot;
      end
      obj.destparam = P.destparam;
      obj.modelname = P.modelname;
      obj.datadictname = P.datadictname;
      obj.paramtpstruct = P.modeltpstruct;
      obj.skippable = P.skippable;
      if isempty(P.srctdimodel)
        obj.unlinked = 1;
      else
        obj.unlinked = 0;
      end
      if isnan(P.shot)
        obj.ovrshotenable=0;
      else
        obj.ovrshotenable=1;
        obj.overrideshot=P.shot;
      end
      obj.mdshelp=P.mdshelp;
      obj.mdsvalid=P.mdsvalid;
      obj.targetdim=P.dim;
      obj.startidx=P.startidx;
      obj.stopidx=P.stopidx;
      
      if obj.startidx>0 && obj.startidx>=obj.stopidx
        error('SCDDSclass_MDSparameter:parseconstructor:startstopidxerror','Error: startidx cannot be <= then stopidx, if defined' );
      end
    end

    function obj=actualizegetcmd(obj, shot)
      % actualize the getcommand string containing the expression for getting the data
      obj = actualizetdiexpr(obj, shot);
      obj.getcommand  = sprintf('%s(''%s'')',...
        obj.MDSinterface.getmdsvaluecmdstr, obj.tdiexprused);
      obj.fullparam=[obj.paramtpstruct '.' obj.destparam];
    end
    
    function obj=actualizetdiexpr(obj, shot)
          % choose tdi expression based on whether we take the model or
          % another shot
            if(shot==-1)
                obj.cfgtdiexpr=obj.srctdimodel;
            else
                obj.cfgtdiexpr=obj.srctdishot;
            end
            % Now build TDI expressions to handle the targetdim~=0 and
            % (startidx~=0 and stopidx~=0) cases
            % as done by MDSObjLoader MARTe2 component
            if obj.targetdim==0 && obj.startidx==0 && obj.stopidx==0
                % standard plain case
                obj.tdiexprused=obj.cfgtdiexpr;
                obj.tdiexprlog=obj.tdiexprused;
            elseif obj.targetdim>0 && obj.startidx==0 && obj.stopidx==0
                % fixed target dimension case
                exptdiexpr=sprintf( ...
                    ['_swgTargetDim = %u;' ...
                     '_swgVec       = %s[0:%u];' ...
                     '_swgVecSize   = shape(_swgVec, 0);' ...
                     'if(_swgTargetDim > _swgVecSize)' ...
                     ' for(_i = 0; _i < _swgTargetDim - _swgVecSize; _i++)' ...
                     '  _swgVec = [_swgVec, 0];' ...
                     '_swgVec'], ...
                     obj.targetdim, ...
                     obj.cfgtdiexpr, ...
                     obj.targetdim-1);                    
                obj.tdiexprused=exptdiexpr;
                obj.tdiexprlog=[obj.cfgtdiexpr sprintf('[0:%u]',obj.targetdim-1)];
            elseif obj.targetdim==0 && obj.startidx>0 && obj.stopidx>0
                % indexed fetching 
                backslashescapedtdi=strrep(obj.cfgtdiexpr,'\','\\');
                exptdiexpr='[';
                for ii=obj.startidx:obj.stopidx
                    exptdiexpr=[exptdiexpr sprintf(backslashescapedtdi,ii)];
                    if ii~=obj.stopidx
                        exptdiexpr=[exptdiexpr ','];
                    end
                end
                exptdiexpr=[exptdiexpr ']';];
                obj.tdiexprused=exptdiexpr;
                obj.tdiexprlog=['[' sprintf(backslashescapedtdi,obj.startidx) '..' sprintf(backslashescapedtdi,obj.stopidx) ']'];
            else
                % not supported configuration
                warning('SCDDSclass_MDSparameter:ParametersError','Invalid contextual target dimension and indexed wildcard parametrization');
                obj.tdiexprused='0';
                obj.tdiexprlog='';
                obj.actualizable=false;
            end
    end    
     
    function out = casttomds(~, in)
      out = in;
    end
    
  end
end

