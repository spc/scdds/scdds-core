classdef SCDclass_mdswg < matlab.mixin.Heterogeneous
    % Superclass for MDS+ wavegens
    % the superclass matlab.mixin.Heterogeneous allows
    % building of lists of mixed kind parameters in the
    % expcode configuration (and later in C++ code)
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    properties (Access = protected)
    %properties    
        mdsserver           % The MDS+ server hosting the parameter
        mdstree             % The MDS+ Tree hosting the parameter
        tdiexpr             % TDI expression to retrieve data
        wavegenbasestruct   % Wavegen base structure hosting the wavegen timeseries struct
        wavegentarget       % Wavegen target to be filled
        value               % commandile output of the wavegen
        datadictionary      % data dictionary hosting the parameter, if empty base workspace
        modelname           % name of the Simulink model using the parameter
        getcommand          % full command for getting the value (callablSCDalgo_f4ealgoe by matlab eval)
        classname           % class name for logging
        marteclassname      % class name for generating MARTe2 cfg file
        cparser             % constructor parameters parser
        
        timebasestart       % timebase start time variable
        timebasedt          % timebase dt variable
        timebasestop        % timebase stop time variable
        
        mdshelpstr          % MDS help string used in tree autopopulation
        mdsvalidationstr    % MDS validation string used in autopopulation
        
        overrideshot        % =1 if instantiated with overridden shot number, in this case this shot number will be always used during actualization
        overrideshotn       % shot number in case of overridden shot number
        
        bound               % true if wavegen has been bound to another SCDDS component 
    end
    
    properties 
        verbose             % Verbosity of the class
    end
    
    methods
        
        %function obj=SCDclass_mdswavegen(srcsrv, srctree, srctdi, modelname, destwavegen, timebasestart, timebasedt, timebasestop)
        function obj=SCDclass_mdswg()
            obj.cparser=inputParser;
            addRequired(obj.cparser,'srctdi',@(x) ischar(x));
            addRequired(obj.cparser,'destwavegen',@(x) ischar(x));
            addParameter(obj.cparser,'srcsrv','tcvdata',@(x) ischar(x));
            addParameter(obj.cparser,'srctree','tcv_shot',@(x) ischar(x));
            addParameter(obj.cparser,'modelname','',@(x) ischar(x));
            addParameter(obj.cparser,'mdshelp','',@(x) ischar(x));
            addParameter(obj.cparser,'mdsvalid','1',@(x) ischar(x));
            addParameter(obj.cparser,'shot',NaN,@(x) isnumeric(x) && isscalar(x));
            
            obj.verbose=1;
            obj.wavegenbasestruct='';
            obj.bound=false;
            
        end
        
        function obj=parseconstructor(obj, srctdi, destwavegen, varargin)
            parse(obj.cparser,srctdi,destwavegen,varargin{:}{:});
            P=obj.cparser.Results;
            
            obj.mdsserver=P.srcsrv;
            obj.mdstree=P.srctree;
            obj.tdiexpr=P.srctdi;
            obj.wavegentarget=P.destwavegen;
            obj.modelname=P.modelname;
            obj.mdshelpstr=P.mdshelp;
            obj.mdsvalidationstr=P.mdsvalid;
            
            if isnan(P.shot)
                obj.overrideshot=0;
            else
                obj.overrideshot=1;
                obj.overrideshotn=P.shot;
            end  
        end    
        
    end
    
    % Sealed methods for heterogeneous arrays
    methods(Sealed)
      function obj = bind(obj,modelname,datadictionary,timingsrc,basestruct)
        for ii=1:numel(obj)
          obj(ii) = obj(ii).setmodelname(modelname);
          obj(ii) = obj(ii).setdatadictionary(datadictionary);
          obj(ii) = obj(ii).settiminginfo(timingsrc);
          obj(ii) = obj(ii).setbasestruct(basestruct);
          obj(ii).bound = true;
        end
      end
    end
    
    % Not abstract methods common to all child classes
    methods
        function mdsconnect(obj, shot)
           assert(~~exist('mdsconnect','file'),...
           'SCD:NoMDS','mdsconnect not found, are the mds matlab tools installed?')
           % If the class has been declared with a 'shot' parameter, then
           % the shotnumber used for param loading is overridden by this
           % parameter, otherwise it is taken from the global shot number
           % given to the actualize command
           if obj.overrideshot == 1
             localshot = obj.overrideshotn;
           else
             localshot = shot;
           end 
           mdsconnect(obj.mdsserver);
           [~,s] = mdsopen(obj.mdstree, localshot);
           if ~rem(s,2)
             warning('SCDclass_mdspar:MDSerror','MDS+ tree open error for parameter %s. Actualization skipped.',obj.modelparam);
             obj.actualizable=false;
           end
        end
        
        function printinfo(obj)
            fprintf('%s (class %s):\n', obj.gettargetwavegen, obj.classname);
            fprintf('  Simulink model: ''%s''\n', obj.modelname);
            fprintf('  MDS+ source server: ''%s'', Tree: ''%s''\n', obj.mdsserver, obj.mdstree);
            fprintf('  MDS+ TDI expression: ''%s''\n',obj.tdiexpr);
        end
               
        function obj = settiminginfo(obj, timeinfo)
            obj.timebasestart=timeinfo.t_start;
            obj.timebasedt=timeinfo.dt;
            obj.timebasestop=timeinfo.t_stop;
        end
        
        function obj = setmodelname(obj, modelname)
            if(isempty(obj.modelname))
                obj.modelname = modelname;
            end
            
        end
        
        function obj = setdatadictionary(obj, ddname)
            if(isempty(obj.datadictionary))
                obj.datadictionary = ddname;
            end
        end

        function obj = setbasestruct(obj, basestruct)
            obj.wavegenbasestruct = basestruct;
        end

        function out = getbasestruct(obj)
            out = obj.wavegenbasestruct;
        end

        
        %function out = gettargetparam(obj)
        %    out = obj.modelparam;
        %end

        function entrystring = genMARTe2entry(obj, shot)
            %entrystring = ['+' obj.wavegentarget ' = { Class=' obj.classname ' Path=' obj.tdiexpr ];
            entrystring = sprintf('+%-50s = { Class=%-30s Path=%-40s',obj.wavegentarget,obj.marteclassname,obj.genMARTe2MDStdiexpression);
        end
        
        function name=getmodelname(obj)
            name=obj.modelname;
        end
        
        function [mdsserver] = getMDSserver(obj)
          mdsserver = obj.mdsserver;
        end
        
        function [mdstree] = getMDStree(obj)
          mdstree = obj.mdstree;
        end
        
        function str=gettarget(obj)
           str = obj.wavegentarget; 
        end
       
        function str = genMARTe2MDStdiexpression(obj)
            % Duplicate first backslash
%             if(obj.tdiexprused(1)=='\' && not(obj.tdiexprused(2)=='\'))
%                 martetdi=['\' obj.tdiexprused];
%             else
%                 martetdi=obj.tdiexprused;
%             end
            % Duplicate backslashes
            %martetdi=strrep(obj.tdiexpr, '\', '\\');
            martetdi=obj.tdiexpr;
            %substitute every " with '
            martetdi=strrep(martetdi, '"', '''');
            %put double string quota
            martetdi=['"' martetdi '"'];
            str=martetdi;
        end
        
        function obj = autopopulatemds(obj, shot, basenode, dim, data, units, srcval)
                        
            fprintf('Changing MDS node: ''%s'' <- %s\n',basenode,srcval);

            node = basenode;
            nodeval = [node '.data'];
            nodedim = [node '.dim'];
            nodecom = [node '.comment'];
            nodevalid = obj.mdsvalidationstr;
            signal = ['build_signal(build_with_units(' nodeval ',"' units '"),*,build_with_units(' nodedim ',"s"))'];
            expr = ['build_param(' signal ',' nodecom ',' nodevalid ')'];
              
            obj.mdsconnect(shot);
            retval=mdsput(node, expr, 'x');
            if isnumeric(retval)
                if ~rem(retval,2)
                    warning('SCDclass_mdswg:MDSerror','Error writing node build_param.');
                end
            else
                warning('SCDclass_mdswg:MDSerror','Error writing node build_param.');
            end
            
            if isnumeric(data)
              retval=mdsput(nodeval,data);    
            else
              retval=mdsput(nodeval,'$',data);
            end        
            if isnumeric(retval)
                if ~rem(retval,2)
                    warning('SCDclass_mdswg:MDSerror','Error writing node data.');
                end
            else
                warning('SCDclass_mdswg:MDSerror','Error writing node data.');
            end

            if isnumeric(dim)
              retval=mdsput(nodedim,dim);
            else
              retval=mdsput(nodedim,'$',dim);
            end
            if isnumeric(retval)
                if ~rem(retval,2)
                    warning('SCDclass_mdswg:MDSerror','Error writing node dim.');
                end
            else
                warning('SCDclass_mdswg:MDSerror','Error writing node dim.');
            end
                        
            retval=mdsput(nodecom, obj.mdshelpstr);
            if isnumeric(retval)
                if ~rem(retval,2)
                    warning('SCDclass_mdswg:MDSerror','Error writing node comment.');
                end
            else
                warning('SCDclass_mdswg:MDSerror','Error writing node comment.');
            end
        end
    end
    
    
    % Abstract method actually implemented by child classes
    methods (Abstract)
        
        % Gets data from MDS+ and actualizes it on the model,
        % MDS+ connection is reopened for every call ...
        actualizedata(obj, shot)
                     
        % Gets data from mds, mds connection is assumed already estabilished
        % and tree opened
        value = getdata(obj)
       
        % Gets a string containing the target wavegen of the model
        out = gettargetwavegen(obj)
        
        % Generate C++ code 
        %gencode(obj)
                
        % casts to proper type
        out = castdata(obj, in);

        % number of signals
        out = getnsigs(obj);
        
    end
end

