classdef (Abstract) SCDDSclass_expcode
    %Superclass for SCDDS expcode object
    %   
    % Main methods:
    %  .open: Open baseline .slx model
    %     open(N): Open node N
    %     open(N,M): Open thread M of node N
    %
    % .compile, compile(N), compile(N,M): Compile Simulink model for node N, thread M)
    % .close_all: Close all Simulink block diagrams 
    % .build, build(N), build(N,M): Generate C code for node N, thread M
    % .clean: Clean configured build folder
    % .deploy, deploy(N), deploy(N,M): deploy object code for node N, thread M
    % 
    % .printinfo: Print information about components of this expcode
    % .printtasks: Print configured tasks
    % .printwavegens: Print configured wavegen signals
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

    properties (Access = public)
        % Main properties
        status        (1,:)    char    % Expcode development status  
        loadverbose   (1,1)    int32   % Verbosity level of the loading (currently 0 or 1)  
        maincode      (1,1)    int32   % Expcode number set by expcode container
    end
    
    properties (SetAccess = protected, Abstract) % to be implemented by derived classes
        mainname      (1,:) char          % Main control system name
        definednodes  (1,:) int32 % List of defined nodes at top expcode level
    end
    
    properties (SetAccess = private) % can not be changed by derived classes
        name(1,:)      char % Expcode main name
        algoobjlist         % list of loaded algorithm objects with configured inits
        algonamelist        % List of loaded algorithm names 
        algoddlist          % list of data dictionaries at algorithm level
        mdscontainer        % container class for MDS+ interface objects
        taskcontainer       % container class for generic init and term task objects
        exportedtps         % list of tunable parameters variable to be exported
        fpinits             % list of standard inits scripts
        modeltoactualize    % name of the algorithm to actualize ('all' means all coonfigured) 
        algos               % list of algorithms objects
        nodes               % array of node properties structs
    end
    
    properties (SetAccess = protected) % can be changed by derived classes
        ddname              % main expcode data dictionary name
        mainslxname         % Top-level SLX name
        algonameprefix char % algorithms name prefix
        ddpath              % main expcode data dictionary save path
        SCDBusdd_path       % data dictionary path for common SCDBus objects
    end
%% STATIC METHODS    

methods(Static)
  function help
    help(mfilename)
  end
end

%%    
%% PUBLIC METHODS    
%%

    methods
        function obj=SCDDSclass_expcode(name,code)
            % Constructor
                

            if nargin==0; name = 'template'; end % default
            if nargin<2;  code = 1;          end % default
            
            obj.name          = name;
            obj.status        = 'debug';
            obj.maincode      = code;
            obj.mainslxname   = obj.mainname;
            obj.ddname        = [obj.mainslxname,'.sldd'];
            obj.algonamelist  = {};
            obj.algoddlist    = {};
            obj.exportedtps   = [];
            obj.nodes        = cell(1,max(obj.definednodes));
            obj.mdscontainer  = SCDclass_mdsobjcontainer;
            obj.taskcontainer = SCDclass_taskcontainer;

            mainslxpath = fileparts(which(obj.mainslxname));
            assert(~isempty(mainslxpath),'%s.slx not found?',obj.mainslxname)
            obj.ddpath   = mainslxpath;
            obj.SCDBusdd_path = mainslxpath;
            
            % Populate defined nodes with defaults, needed since top-level
            % main slx needs all nodes defined, including inactive ones, to pass.
            for inode = obj.definednodes
              obj.nodes{inode} = obj.getdefaultnode(inode);
            end
          end

%% Main expcode handling methods

        function open(obj,varargin)
          openslx = obj.getslxname(varargin{:});
          fprintf('Opening %s.slx\n',openslx)
          open(openslx);
        end

        function setup(obj)
           % This function sets up the SCD Simulink model
           % to simulate this experimental code.
           % NOTE THAT it doesn't actualize tunable parameters
           % and wavegens, call actualizedata for that
           fprintf('Setting up expcode %d, ''%s'', configuring data dictionaries ...\n',obj.maincode,obj.name);
           obj.createmaindd;
           
           obj.setupwrapperdd;
           obj.setupnodedd;
           obj.setupmaindd;
         
           fprintf('Setting up expcode %d, ''%s'', configuring default tunable parameters ...\n',obj.maincode,obj.name);           
           obj.updatedefaulttp;
           fprintf('Setting up expcode %d, ''%s'', configuring global data ...\n',obj.maincode,obj.name);
           obj.setupmain;
           fprintf('Setting up expcode %d, ''%s'', configuring main workspace variables ...\n',obj.maincode,obj.name);           
           obj.buildworkspacesimstruct;
           obj.buildworkspacetpstruct;
           
           % Set cache and codegen folders
           SCDDSclass.setcachefolder(obj.cachefolder)
           SCDDSclass.setcodegenfolder(obj.codegenfolder);
        end

        function createmaindd(obj)
          % create main data dictionary from scratch to ensure it exists
          if contains(Simulink.data.dictionary.getOpenDictionaryPaths,obj.ddname)
              Simulink.data.dictionary.closeAll(obj.ddname,'-discard');
              % close if it exists and discard changes 
          end
          delete(which(obj.ddname));
          Simulink.data.dictionary.create(fullfile(obj.ddpath,obj.ddname));
        end
          
        function setupmaindd(obj)
          % prepare main top-level SLDD
          dd = Simulink.data.dictionary.open(obj.ddname);

          % link data dictionaries for active nodes
          for ii=obj.definednodes
            mynode = obj.nodes{ii};
            if isempty(mynode), continue; end
            mydatasource = obj.nodes{ii}.ddname;
            fprintf('adding data source %s to %s\n',mydatasource,obj.ddname)
            dd.addDataSource(mydatasource);
          end
          % Set up RFM bus
          fprintf('Setting up RFM main bus\n')
          addRFMbus(obj,dd)

          fprintf('Setting up variant model configuration')
          obj.setupvaralgo;
          
          fprintf('Setting up node parameters configuration\n')
          obj.setupnodeparams;
          
          dd.saveChanges;

        end
      
        function actualize(obj, shot, varargin)
            % if a model name is given, operations will be performed
            % only on it, otherwise they will be performed on all
            % configured models
            p = inputParser;
            addParameter(p,'model','',@(x) ischar(x));
            parse(p,varargin{:});
            
            if(isempty(p.Results.model))
                obj.modeltoactualize='all';
            else
                obj.modeltoactualize=p.Results.model;
            end
            
            obj.actualizedata(shot);
            fprintf('Actualizing expcode %d, ''%s'', performing tasks ...\n',obj.maincode,obj.name);
            obj.taskcontainer.modeltoexecute=obj.modeltoactualize;
            obj.taskcontainer.exectasksoninit(shot);
        end

        function obj = init(obj)
          % close all data dictionaries
          Simulink.data.dictionary.closeAll('-discard')

          % Set configuration settings sldd
          SCDconf_setConf('SIM')
          
          % check if anything to be done
          if isempty(obj.algoobjlist)
            fprintf(' no SCD inits to run, done ***\n'); return; 
          end

          % sort initobj list to respect dependencies in refdd parents
          obj = obj.sortalgoobjlist;         
          
          % Carry out any init tasks of algorithms
          for ii=1:numel(obj.algoobjlist)
            obj.algoobjlist{ii}.init();
            
            opendds = Simulink.data.dictionary.getOpenDictionaryPaths;
            % check that .sldd is not opened by some algorithm init
            assert(~any(contains(opendds,obj.ddname)),...
              'init for %s leaves %s open - this should be avoided',...
              obj.algoobjlist{ii}.getname,obj.ddname)
          end
          fprintf('\n** DONE WITH ALL SCDINITS **\n');
        end
        
        function updatemdsmodel(obj, algo)
           obj.updatemds(algo, -1); 
        end
                
        function compile(obj,varargin)
          myslx = obj.getslxname(varargin{:});
          fprintf('Compiling %s.slx\n',myslx)
          try
            eval(sprintf('%s([],[],[],''compile'')',myslx));
            eval(sprintf('%s([],[],[],''term'')',myslx));
          catch ME
            rethrow(ME)
          end
        end
        
        function [results] = sim(obj,varargin)
          w=evalin('base','whos');
          if ~ismember('configurationSettings',[w(:).name])
            obj.setsimconf;
          end
          myslx = obj.getslxname(varargin{:});
          fprintf('Simulating %s.slx\n',myslx)
          results = sim(myslx,'SaveOutput','on','OutputSaveName','logsout');
        end  
        
        function [ok]=build(obj,varargin)  
          % this method builds expcode threads
          % invoking rtwbuild command
          % first optional parameter is an array
          % of nodes and second is an array of threads
          % if no additional parameters are given,
          % all expcode configured nodes and threads are built
          %
                    
            % make list of nodes to build
            compileslx_list = cell(0);
            compilebuildcfg_list = cell(0);
            compilebuildargs_list = cell(0);
            switch numel(varargin)
              case 0
                inodeset=1:numel(obj.nodes);
                ithreadset=[];
                fprintf('\n=== Building all threads for %s ===\n',obj.name);
              case 1
                inodeset=varargin{1};
                ithreadset=[];
              case 2
                inodeset=varargin{1}; 
                ithreadset=varargin{2}; 
              otherwise
                error('Wrong number of inputs');
            end
            
            for inode=inodeset
              nodeinfo=obj.nodes{inode};
                if ~isempty(obj.nodes{inode})
                  if isempty(ithreadset)
                    ithreadsetnode=1:nodeinfo.getncpu();
                  else
                    ithreadsetnode=ithreadset;
                  end
                  for icpu=ithreadsetnode
                    getcpuactive=nodeinfo.getcpuactive();
                    if getcpuactive(icpu)
                        compileslx_list = [compileslx_list,...
                        obj.getslxname(inode,icpu)];
                        compilebuildcfg_list = [compilebuildcfg_list,...
                        obj.getbuildcfgset(inode,icpu)];
                        compilebuildargs_list = [compilebuildargs_list,...
                        obj.getbuildargs(inode,icpu)];
                    end
                  end
                end
            end
            
            if isempty(compileslx_list)
              fprintf('No active nodes found, no .slx to compile. Done\n');
              return
            end              
     
          % check env variable
          %assert(~isempty(getenv('RTCCODE_LIBPATH')),'RTCCODE_LIBPATH environment variable needs to be defined to compile');
 
          % Build
          buildresults = false(numel(compileslx_list), 1);
          for ii=1:numel(compileslx_list)
             try
               fprintf('\n\n*** BUILDING ''%s'' CFG: ''%s'', ARGS: ''%s'' ***\n\n',...
                 compileslx_list{ii},compilebuildcfg_list{ii},compilebuildargs_list{ii});
               SCDconf_setConf(compilebuildcfg_list{ii});
               % adding additional build args
               cs = evalin('base','configurationSettings');
               makecmd = get_param(cs,'MakeCommand');
               makecmd = [makecmd compilebuildargs_list{ii}];
               cs.set_param('MakeCommand',makecmd);
               assignin('base','configurationSettings',cs)
               % codegen + build
               if ~exist('scddsalgoinfo.h','file')
                 write_SCDDS_header('n/a');
               end
               rtwbuild(compileslx_list{ii});
               buildresults(ii)=true;
             catch EX
               fprintf('**** BUILDING ''%s'' WITH ''%s''  FAILED **** \n',compileslx_list{ii},compilebuildcfg_list{ii})
               fprintf('Error message:\n %s\n',getReport(EX));
             end
          end
          
          % Print a final status report, comprising code description
          % script
          ok=true;
          fprintf('\n*** BUILD SUMMARY ***\n')
          for ii=1:numel(compileslx_list)
            fprintf('\n%s:\n',compileslx_list{ii})
            fprintf(' build outcome: ');
            if buildresults(ii)
              fprintf('SUCCESS\n');
              afolder=Simulink.fileGenControl('getConfig').CodeGenFolder;
              fulllibname=[afolder '/' compileslx_list{ii} '.so'];
              fprintf(' code library: %s\n',fulllibname);
              [r,cds]=system(['getalgoinfo ' fulllibname]);
              if r==0
                fprintf(' code description string (use gittag method to re-tag and change it):\n');
                fprintf(' %s\n',cds);
              else
                fprintf(' code description string not available.\n');
              end
            else
              fprintf('FAIL\n'); 
              ok=false;
            end
          end
        end
        
        function clean(obj)
          afolder=Simulink.fileGenControl('getConfig').CodeGenFolder;
          fprintf('cleaning %s\n', afolder);
          system(['rm -rf ' afolder '/*']);  
        end
                
        %% Property access methods
        function obj = setnode(obj,node,nodenr)
          assert(nargout==1,'must set output argument when calling setnode')
          node = node.setactive(true);
          assert(node.nodenr == nodenr,...
            'set node number and node property do not match while setting node %s',nodenr)
          obj.nodes{node.nodenr} = node;
          
          % Make lists of data dictionaries, mds objects, init functions etc
          % that come with this node.
          
          % wrapper algorithms
          for iwrap = 1:numel(node.wrappers)
            wrapperObj = node.wrappers{iwrap}.wrapperobj;
            cpunr        = node.wrappers{iwrap}.cpunr;
            
            for ialgo = 1:numel(wrapperObj.algos)
              algoObj = wrapperObj.algos(ialgo);
              obj = processalgorithm(obj,algoObj,node.nodenr,cpunr);
            end
          end
          
          % node algorithms
          for ialgo = 1:numel(node.algos)
            algoObj = node.algos(ialgo);
            obj = processalgorithm(obj,algoObj,node.nodenr,1);
          end
        end
        
        function myslx = getslxname(obj,inode,icpu)
            if nargin==1
                myslx = obj.mainslxname;
            elseif nargin==2
                assert(isnumeric(inode))
                myslx = sprintf('SCD_rtc_%02d',inode);
            elseif nargin==3
		myslx = obj.nodes{inode}.wrappers{icpu}.wrapperobj.name
            else
                error('invalid number of arguments');
            end
        end
        
        %% Printing methods
        
        function printinfo(obj,nodenr,cpunr)
          if nargin==3
            % thread printinfo
            obj.nodes{nodenr}.wrapper{cpunr}.wrapperObj.printinfo;
            return
          elseif nargin==2
            if isempty(obj.nodes{nodenr}); fprintf('no node %d defined\n',nodenr); end
            obj.nodes{nodenr}.printinfo;
            return
          end
          
          fprintf('*****************************************************\n');
          fprintf('* SCD expcode: ''%s'', main code: %d\n',obj.name,obj.maincode);
          fprintf('*****************************************************\n');
          fprintf('* Configured nodes:\n')
          for node = obj.nodes
            if ~isempty(node{:}), node{:}.printinfo; end
          end
                
          fprintf('* Configured algorithms:\n');
          if(numel(obj.algonamelist)>0)
            for ii=1:numel(obj.algonamelist), fprintf('  ''%s''\n', char(obj.algonamelist{ii})); end
          end
          fprintf('* Configured algorithm data dictionaries:\n')
          if(numel(obj.algoddlist)>0)
            for ii=1:numel(obj.algoddlist), fprintf('  ''%s''\n', char(obj.algoddlist{ii})); end
          end
          fprintf('* Configured exported tunparams structures:\n')
          if(numel(obj.exportedtps)>0)
            for ii=1:numel(obj.exportedtps), fprintf('  ''%s''\n', char(obj.exportedtps{ii})); end
          end
          fprintf('* Configured MDS tunparams objects: %d (use printparameters method for details)\n', obj.mdscontainer.getnumparams);
          fprintf('* Configured MDS wavegens objects: %d (use printwavegens method for details)\n', obj.mdscontainer.getnumwavegens);
          fprintf('* Configured general purpose tasks: %d (use printtasks method for details)\n', obj.taskcontainer.numtasks);
          fprintf('* Configured init scripts: %d (use printinits method for details)\n', numel(obj.algoobjlist));
        end
        
        function printtasks(obj)
            obj.taskcontainer.printtasks;
        end

        function printinits(obj)
           % TODO: uniform the approach between stdinits and fpinits
           % but only if fpinits will stay ...
            
           fprintf('* Configured std init scripts:\n');
           if(~isempty(obj.algoobjlist))
               for ii=1:numel(obj.algoobjlist)
                   [stdinitstmp,~]=obj.algoobjlist{ii}.getinits;
                   if ~isempty(stdinitstmp)
                    for jj=1:numel(stdinitstmp)
                     fprintf('  %s\n',char(stdinitstmp{jj}));
                    end
                   end
               end
           end      
           
           fprintf('* Configured fp init scripts:\n');
           if(~isempty(obj.fpinits))
               for ii=1:numel(obj.fpinits)
                   fprintf('  %s -> %s\n',char(obj.fpinits{ii}{1}),char(obj.fpinits{ii}{2}));
               end
           end
           
        end

        function printMARTe2taskconfig(obj, shot, varargin)   
            p=inputParser;
            % if a model name is given, operations will be performed
            % only on it, otherwise they will be performed on all
            % configured models
            addParameter(p,'model','',@(x) ischar(x));
            parse(p,varargin{:});
            
            if(isempty(p.Results.model))
                obj.taskcontainer.modeltogenerate='all';
            else
                obj.taskcontainer.modeltogenerate=p.Results.model;
            end
            obj.taskcontainer.printMARTe2taskconfig(shot);
        end
        
        function printMARTe2parconfig(obj, shot, varargin)  
            % function printMARTe2parconfig(shot, varargin)
            %
            % produces an automatically generated text output
            % instanitating a MARTe2 MDSObjLoader component
            % loading all configured tunable parameters.
            %
            % shot is used to distinguish the loader classes loading source
            % between model shot and standard shot, if configured
            %
            % varargin has two optional parameters:
            % if a 'model' name is given, operations will be performed
            % only on it, otherwise they will be performed on all
            % configured models
            % an optional 'fid' parameter can contain an open file
            % identifier, in this case the output will be redirected
            % to thsi file, otherwise it will be on the screen
            p=inputParser;
            addParameter(p,'model','',@(x) ischar(x));
            addParameter(p,'fid',1,@(x) isnumeric(x));
            parse(p,varargin{:});
            
            if(isempty(p.Results.model))
                obj.mdscontainer.modeltogenerate='all';
            else
                obj.mdscontainer.modeltogenerate=p.Results.model;
            end
           
            fid=p.Results.fid;
            obj.mdscontainer.printMARTe2parconfig(shot,fid);
        end

        function [signames, sigdims] = printMARTe2wgbusconfig(obj, shot, busname, frequency, fid, varargin)
            % printMARTe2wgbusconfig(obj, shot, busname, frequency, fid, varargin)
            %
            % prints cfg file for loading busname Simulink.Bus
            % as a wavegen (or a set of them) in MARTe2
            % shot can be -1 or a fixed shot (usually -1), but currently
            % the Shot= entry is populated by a fixed macro
            % frequency is the frequency of signal generation in MARTe2
            % the optional parameter 'ddname' can be given to specify
            % the data dictionary where the bus definition is, if omitted
            % the default expcode level data dicationary is used
            
            p=inputParser;
            addParameter(p,'ddname',obj.ddname,@(x) ischar(x));
            parse(p,varargin{:});
            myddname = p.Results.ddname;
                        
             [signames, sigdims] = obj.mdscontainer.printMARTe2wgbusconfig(shot, myddname, busname, frequency, fid);               
        end

        function printparameters(obj)
            obj.mdscontainer.printparameters;
        end

        function printwavegens(obj)
            obj.mdscontainer.printwavegens;
        end

        %% Configuration methods  
        function setsimconf(~)
          % set simulation configuration
          SCDconf_setConf('SIM');
        end
        
        function setbuildconf(~)
          % set build configuration
          SCDconf_setConf('CODE');
        end
        
        function obj = processalgorithm(obj,algoObj,node,cpu)
          % Checking and importing algorithm name
          if(~ismember(algoObj.getname,obj.algonamelist))
            obj.algonamelist{end+1} = algoObj.getname;
            obj.algos{end+1}        = algoObj;
            algoispresent = false;
          else
            fprintf('algorithm ''%s'' already present in the expcode, importing only wavegens\n',algoObj.getname);
            algoispresent = true;
          end
          
          % Set path of data dictionary containing SCDBus
          algoObj.SCDBusdd_path = obj.SCDBusdd_path;
          
          % Importing algorithm MDS objects into the main mdscontainer,
          % filling base structure name
          algomdscontainer = algoObj.getmdscontainer;
          
          % import wavegens
          basewgstruct     = sprintf('SCDsimdata.SCDnode%02d%02d_simdata.wavegen', node, cpu);
          algomdscontainer = algomdscontainer.setwavegenbasestruct(basewgstruct);
          obj.mdscontainer = obj.mdscontainer.importmdswavegens(algomdscontainer);
          
          if algoispresent, return; end % return if algo is already present
          
          % Importing exported tunable parameters
          obj.mdscontainer=obj.mdscontainer.importmdsparams  (algomdscontainer);
          algoexptps=algoObj.getexportedtps;
          if numel(algoexptps)>0
            for ii=1:numel(algoexptps)
              if ~ismember(algoexptps{ii},obj.exportedtps)
                obj.exportedtps{end+1}=algoexptps{ii};
              else
                fprintf('exported tunparams sctruct ''%s'' already present, ignoring',algoexptps{ii})
              end
            end
          end
          
          % Importing algorithms data dictionary, only those with proper name
          algodd=algoObj.getdatadictionary;
          if(strcmp(algodd(1:numel(obj.algonameprefix)),obj.algonameprefix))
            if(~ismember(algodd,obj.algoddlist))
              obj.algoddlist{end+1}=algodd;
            else
              warning('SCDclass_expcode:addalgorithm','algorithm data dictionary ''%s'' already present, ignoring', algodd);
            end
          else
            error('attempting to add algorithm data dictionary not starting with ''%s''', obj.algonameprefix)
          end
          
          % Importing tasks
          algotaskcontainer=algoObj.gettaskcontainer;
          obj.taskcontainer=obj.taskcontainer.importtaskobjects(algotaskcontainer);
          
          % Importing inits
          [stdinitstmp,fpinitstmp]=algoObj.getinits;
          if numel(fpinitstmp)>0
            toadd = ones(numel(fpinitstmp),1);
            for ii=1:numel(fpinitstmp)
              if ~isempty(obj.fpinits)
                for jj=1:numel(obj.fpinits)
                  for kk=1:numel(obj.fpinits{jj}{2})
                    if(strcmp(char(obj.fpinits{jj}{2}{kk}),fpinitstmp{ii}{2}))
                      warning('SCDclass_expcode:addalgorithm','An init driving the structure %s has already been added, ignoring this init',char(fpinitstmp{ii}{2}))
                      toadd(ii)=0;
                    end
                  end
                end
              end
              if toadd(ii)
                temp=cell(10,1);
                temp{1}=fpinitstmp{ii}{1};
                temp{2}=fpinitstmp{ii}{2};
                obj.fpinits{end+1}=temp;
              end
            end
            if any(toadd) % if any inits from this algoobj were taken
              obj.algoobjlist{end+1}=algoObj; %% Add the full algorithm object here, to see if it is fine
            end
          elseif(numel(stdinitstmp)>0)
            obj.algoobjlist{end+1}=algoObj;
          end
        end
        
        function obj = sortalgoobjlist(obj)
          % sort algoobj list such that no algoobj has dependency on other 
          % algoobj that are further down the list. So running algoobj inits 
          % in this order ensures that everything is initialized in the correct order.
          
          A = zeros(numel(obj.algoobjlist)); % init adiadency matrix
          % This matrix element (col,row)=(j,i) will contain 1 if the 
          % algorithm in algoobjlist(i) references a data dictionary which
          % depends on the algoritm in algoobjlist(j).
          
          for ii=1:numel(obj.algoobjlist)
            myalgoobj = obj.algoobjlist{ii};
            refddparentalgo = myalgoobj.getrefddparentalgo;
             if isempty(refddparentalgo) || all(cellfun(@isempty,refddparentalgo))
               A(:,ii) = 0; % no dependency
             else
               % find indices of dependent algos
               for iref = 1:numel(refddparentalgo) % loop on possibly many dd algos
                 % index of this referenced parent algo in algoobjlist
                 jj = contains(obj.algonamelist,refddparentalgo{iref}.getname);                
                 A(jj,ii) = 1;
               end
             end
          end
          % Use matlab tools to sort the graph. Could also write e.g. Kahn's
          % algorithm explicitly (see wikipedia/topological_sorting)
          D = digraph(A); % directed graph
          % topological sorting, maintaining index order where possible
          isort = toposort(D,'Order','stable'); 
          obj.algoobjlist = obj.algoobjlist(isort); % sort the init obj list
        end
            
        function out = getexportedtps(obj)
            out = obj.exportedtps;
        end

        function obj = setmainslxname(obj, name);
            obj.mainslxname = name;
        end

        %% Default values setup methods 
        function updatedefaulttp(obj)
          % updatedefaulttp()
          %
          % updates _tmpl tunparams structures
          % for every defined updating function
          % To define an update function see SCDclass_algo.addtunparamstruct
          
          for ii=1:numel(obj.nodes)
            mynode = obj.nodes{ii};
            if ~isempty(mynode)
              mynode.updatetemplatetp();
            end
          end
        end
    end

%% PRIVATE METHODS    
    
    methods (Access = private)

%% Matlab environmental setup methods       
        
        function Folder = setTmpFolder(obj,whichFolder)
          
          switch whichFolder
            case {'CacheFolder','CodeGenFolder'}
              % Set tmp Folder for experimental code (avoid conflicts with other tmp files)
              Folder = fullfile(fileparts(mfilename('fullpath')),'..','..',...
                'gencodes',sprintf('%s-%d',whichFolder,obj.maincode));
              fprintf('Setting Simulink %s folder to %s\n',whichFolder,Folder)
              Simulink.fileGenControl('set',whichFolder,Folder,'createdir',true);
            otherwise
              error('untreated case %s',whichFolder);
          end
          
          if ~exist(Folder,'dir')
            mkdir(Folder')
          end
        end
        
        function CacheFolder = setcachefolder(obj)
          CacheFolder = setTmpFolder(obj,'CacheFolder');
        end
        
        function CodeGenFolder = setcodegenfolder(obj)
          CodeGenFolder = setTmpFolder(obj,'CodeGenFolder');
        end
        
        function obj=printlog(obj,str,varargin)
          % printlog, allows sprintf()-like expressions
            if obj.loadverbose==1
               fprintf('Expcode %d, ', obj.maincode); 
               fprintf(str,varargin{:}); fprintf('\n');
            end
        end
        
        function add_remove_sources(obj,ddsources_to_add,ddname,prefix)
          % add and remove data sources from data dictionary ddname
          % ddsources_to_add is cell array with sources
          % prefix is an optional filter to only remove those with the correct
          % prefix.
          
          ddObj = Simulink.data.dictionary.open(ddname);
          prevsources = ddObj.DataSources;
          prevsources_prefix = prevsources(startsWith(prefix,prevsources));
          for ii=1:numel(prevsources_prefix)
            myddsource = prevsources_prefix{ii};
            if ~contains(ddsources_to_add,myddsource)
              obj.printlog('Removing wrapper data source %s from %s', myddsource, ddname);
              ddObj.removeDataSource(myddsource)
            end
          end
          
          for ii=1:numel(ddsources_to_add)
            myddsource = ddsources_to_add{ii};
            if contains(prevsources_prefix,myddsource)
              obj.printlog('Not Adding wrapper data source %s from %s - already there',...
                myddsource, ddname);
            else
              obj.printlog('Adding wrapper data source %s to %s',...
                myddsource, ddname);
              ddObj.addDataSource(myddsource);
            end
          end
        end
        
        function obj = buildworkspacetpstruct(obj)
            % this funtion builds a workspace structures containing
            % replicas of all tunable parameters structures in the data
            % dictionaries, this structure is the one actually used 
            % for loading simulation wavegen data
            % It is better not to use directly data dictionaries structures
            % to avoid flooding dds with big sim data sets (and
            % conseguently the rtccode GIT repo itself
           
            dict=Simulink.data.dictionary.open(obj.ddname);
            dd=getSection(dict, 'Design Data');
            
            for ii=1:numel(obj.exportedtps)
                simstructnamedd=sprintf('%s_tmpl',char(obj.exportedtps(ii)));
                simstructnamews=char(obj.exportedtps(ii));
                if ~dd.exist(simstructnamedd)
                  warning('tunable params structure %s not found', simstructnamedd);
                  continue
                end
                Entry = dd.getEntry(simstructnamedd);
                assert(numel(Entry)<2,'multiple entries found for %s',simstructnamedd);
                simstruct = dd.getEntry(simstructnamedd).getValue;
                assignstr = sprintf('%s=temp;',simstructnamews);
                assignin('base','temp',simstruct);
                evalin('base',assignstr);
            end
            evalin('base','clear temp;');            
        end
        
        function setupnodedd(obj)
            % For every node, add required wrapper datadicationaries as
            % sources for the main data dicationary

            % Getting main data dictionary and required data sources
            datadictname = obj.ddname;
            fprintf('opening %s\n',datadictname)
            
            % Looping through every active node
            for idx_nodedds=1:numel(obj.definednodes)
               inode = obj.definednodes(idx_nodedds);
               mynode = obj.nodes{inode};
               if isempty(mynode), continue; end

               % Add all wrapper dictionaries as datasources for main dd
               wrapperdd_to_add = cell(numel(mynode.wrappers),1); % init
               for iwrap=1:numel(mynode.wrappers)
                 wrapperdd_to_add{iwrap} = mynode.wrappers{iwrap}.wrapperobj.ddname;
               end
               ddsources_to_add = wrapperdd_to_add;
               
               % remove ones we don't need
               obj.add_remove_sources(ddsources_to_add,datadictname,'SCDwrap')
               
               %%               
               % Add also node algorithm dd to node dd directly
               if ~isempty(mynode.algos)
                 algodd_to_add = {mynode.algos.getdatadictionary};
               else
                 algodd_to_add = {};
               end
               obj.add_remove_sources(algodd_to_add,mynode.ddname,obj.algonameprefix)
            end  
        end
        
        function setupwrapperdd(obj)
          % Set up the wrapper data dictionary links
          
          for inode = 1:numel(obj.nodes)
            mynode = obj.nodes{inode};
            if isempty(mynode), continue; end
            for ii=1:numel(mynode.wrappers)
              wrapperObj = mynode.wrappers{ii}.wrapperobj;
              % link to algorithms contained in the wrappers
              wrapperObj.linkalgodd(obj.algonameprefix);
            end
          end
          
          % link to hosting node
        end
        
        function buildworkspacesimstruct(obj)
%           obj.mdscontainer.buildworkspacesimstructnode(obj,node)
%           obj.mdscontainer.buildworkspacesimstruct; % old
          
          dd=SCDconf_getdatadict(obj.ddname);
            for ii = 1:numel(obj.definednodes)
              inode = obj.definednodes(ii);
              buildworkspacesimstructnode(obj,inode,dd);
            end
            
          evalin('base', 'SCDrefdata = SCDsimdata;');  
            
        end    

        function obj = setupmain(obj)
            % sets up global configs for the expcode
            scd.expcode = obj.maincode;
            scd.timing.dt = 1e-4; % hard code for now, determine later from fastest thread
            Simulink.data.assigninGlobal(obj.getslxname,'scd',scd);
            
            % add configurations.sldd as reference, contains simulation parameters
            ddObj = Simulink.data.dictionary.open(obj.ddname);
            ddObj.addDataSource('configurations.sldd');
        end
        
        function addRFMbus(obj,dd)
          ddSection = dd.getSection('Design Data');
          zeroElem = Simulink.BusElement;
          zeroElem.Name = 'zero';
          
          Elems = repmat(Simulink.BusElement,numel(obj.definednodes),1);

          
          % Extra bus and leaf for Externals_RFM (non-node entities writing to RFM)
          [extBusObj,extBusName] = SCDconf_createRFMOUTextbus;
          assignin(ddSection,extBusName,extBusObj);
          
          Elems(1) = Simulink.BusElement;
          Elems(1).Name        = 'Externals_RFM';
          Elems(1).DataType    = ['Bus: ',extBusName];
          Elems(1).Description = 'Bus for data from external nodes writing to RFM';
     
          for ii = 1:numel(obj.definednodes)
            inode = obj.definednodes(ii);
            leafName    = sprintf('Node%02d_RFM' ,inode);
            subBusName  = sprintf('Bus: RFMOUT%02dbus',inode);
            
            % corresponding leaf element in main RFMIn bus
            Elems(ii+1) = Simulink.BusElement;
            Elems(ii+1).Name        = leafName;
            Elems(ii+1).Description = sprintf('Node %02d RFM output',inode);
            Elems(ii+1).DataType    = subBusName;
          end
        
          % Assemble RFM input bus
          RFMINbus = Simulink.Bus;
          RFMINbus.HeaderFile  = '';
          RFMINbus.Description = 'RFM input bus for all nodes';
          RFMINbus.DataScope   = 'Auto';
          RFMINbus.Alignment   = -1;
          RFMINbus.Elements    = Elems;
          
          assignin(ddSection,'RFMINbus',RFMINbus);

        end
        
        function setupvaralgo(obj)
            % Sets up varalgo structure in main data dictionary
            % according to varalgo info of the nodes

            % Getting data section of main data dictionary
            fprintf('opening %s to setup variant model configurations\n',obj.ddname);
            
            d=Simulink.data.dictionary.open(obj.ddname);
            dd=getSection(d, 'Design Data');
            
            s=struct();
            % Looping through every active node
            for ii=1:numel(obj.definednodes)
              inode = obj.definednodes(ii);
              mynode = obj.nodes{inode};
              if isempty(mynode), continue; end
              
              for jj=1:numel(mynode.wrappers)
                fieldname = sprintf('algo%02d%02d',inode,jj);
                s.(fieldname) = mynode.wrappers{jj}.varalgo;
              end
            end
            % save in data dictionary
            if dd.exist('SCDvaralgo')
              ee = dd.getEntry('SCDvaralgo'); ee.setValue(s);
            else
              dd.addEntry('SCDvaralgo',s);
            end
        end
        
        function setupnodeparams(obj)
          % set node parameter structure in main data dictionary
          for ii=obj.definednodes
            mynode = obj.nodes{ii};
            if ~isempty(mynode)
              for field = {'active','name','timing'}
                nodeparams.(field{:}) = mynode.(field{:});
              end
            end
            
            varname = sprintf('SCDnode%02dparams',mynode.nodenr);
            % setup node parameters
            d=Simulink.data.dictionary.open(obj.ddname);
            dd=getSection(d, 'Design Data');
            fprintf('setting Node parameters %s in %s\n',varname,obj.ddname);
            if dd.exist(varname)
              ee=dd.getEntry(varname);
              ee.setValue(nodeparams);
            else
              dd.addEntry(varname,nodeparams);
            end
          end
        end
        
        function obj = buildworkspacesimstructnode(obj,inode,dd)
          % build simulation data structure
          node = obj.nodes{inode};

          if evalin('base','exist(''SCDsimdata'',''var'')')
            SCDsimdata = evalin('base','SCDsimdata');
          else
            SCDsimdata = struct(); % assign here if empty
          end
          
          if node.haswavegen
            for ithread = 1:node.ncpu
              % get whatever is in data dictionary template in wrappers
              simstructname = sprintf('SCDnode%02d%02d_simdata',inode,ithread);
              wgbusname = sprintf('WG%02d%02dbus',inode,ithread);

              fprintf('   setting up %s',simstructname);
              if dd.exist(simstructname)
                ddentry = dd.getEntry(simstructname);
                assert(numel(ddentry)==1,'multiple entries found for %s',simstructname)

                simstruct = ddentry.getValue;
                mybus=dd.getEntry(wgbusname).getValue; % get from data dictionary
                if ~isstruct(simstruct) || ~isfield(simstruct,'wavegen')
                  fprintf('.. loaded simstruct wavegen is not compatible');
                  regenerate = true;
                elseif SCDconf_structbuscmp(simstruct.wavegen,mybus)
                  fprintf('... loaded WG from data dictionary\n');
                  regenerate=false;
                else
                  fprintf('... loaded WG structure from dd does not match bus definition...');
                  regenerate=true;
                end
              else
                regenerate = true;
                fprintf('... could not find %s in data dictionary',simstructname);
              end
              
              if regenerate
                simstruct.wavegen = SCDconf_createstructfrombus(dd,wgbusname); % structure to match
                %ddsource = dd.find('Name',wgbusname).DataSource; % dd containing bus
                %wrapdd=Simulink.data.dictionary.open(ddsource).getSection('Design Data');
                %wrapdd.addEntry(simstructname,simstruct)
                fprintf('... re-generated from bus %s\n',wgbusname')
              end
              SCDsimdata.(simstructname) = simstruct;
            end
          end
          
          % Node-specific
          simstructname = sprintf('SCDnode%02dsimdata',inode);
          if node.hasadc
            adcbusname = sprintf('ADC%02dbus',inode);
            fprintf('   setting up %s.adc\n',simstructname);
            SCDsimdata.(simstructname).adc = SCDconf_createstructfrombus(dd,adcbusname);
          end
          
          if node.hasextsource
            % to separate out into specific method
            if node.hasethercat
              ethercatbusname = 'ETHCAT1IN';
              fprintf('   setting up %s.ethercat\n',simstructname);
              SCDsimdata.(simstructname).ethercat = ...
                SCDconf_createstructfrombus(dd,ethercatbusname);
            end
            
            if node.hasrfm
              % add also RFM
              RFMbusname = 'RFMINbus';
              SCDsimdata.rfm = SCDconf_createstructfrombus(dd,RFMbusname);
            end
          end
          % assign result in base workspace
          assignin('base','SCDsimdata',SCDsimdata);
        end
        
%% Actualization methods        
        
        function actualizedata(obj, shot)
           % This function actualizes configured
           % tunable parameters and wavegens timeseries
           % according to the given shot
           
           currentexpcode=Simulink.data.evalinGlobal(obj.mainslxname,'scd.expcode');
           if(currentexpcode~=obj.maincode)
               error('SCDclass_expcode:wrongexpcodeloaded','Cannot perform data actualization, the loaded expcode is not matching, try to setup the expcode first');
           end
           fprintf('Actualizing expcode %d, ''%s'', configuring tunable parameters ...\n',obj.maincode,obj.name);
           obj.actualizeparameters(shot)
           fprintf('Actualizing expcode %d, ''%s'', configuring wavegens ...\n',obj.maincode,obj.name);
           obj.actualizewavegens(shot);
        end

        function actualizeparameters(obj,shot)
            obj.mdscontainer.modeltoactualize=obj.modeltoactualize;
            obj.mdscontainer.actualizeparameters(shot);
        end
        
        function actualizewavegens(obj,shot)
          obj.mdscontainer.modeltoactualize=obj.modeltoactualize;
          obj.mdscontainer.actualizewavegens(shot);
        end
        
        %% MDS update methods
        function updatemds(obj, algo, shot)
          if shot~=-1
            error('SCDclass_expcode:updatemds','update permitted only on the model shot');
          end
          
          %             % first update algorithm tunparams default locally
          %             for ii=1:numel(obj.algonamelist)
          %                if strcmp(algo, obj.algonamelist{ii})
          %                    obj.algos{ii}.updatetemplatetp();
          %                end
          %             end
          %             % then update mds
          obj.mdscontainer.modeltogenerate=algo;
          obj.mdscontainer.autopopulateMDSparams(shot);
          obj.mdscontainer.autopopulateMDSwavegens(shot);
        end
        
        %% Matlab environment methods
        function CacheFolder = cachefolder(obj)
          CacheFolder = fullfile(fileparts(mfilename('fullpath')),'..','..','..',...
            'gencodes',sprintf('CacheFolder-%d',obj.maincode));
        end
        
        function CodeGenFolder = codegenfolder(obj)
          CodeGenFolder = fullfile(fileparts(mfilename('fullpath')),'..','..','..',...
            'gencodes',sprintf('CodeGenFolder-%d',obj.maincode));
        end
        
        %% Build methods              
        function cs = getbuildcfgset(obj, inode, icpu)
            cs = obj.nodes{inode}.getbuildcfgset(icpu);
        end
        
        function out = getbuildargs(obj, inode, icpu)
            node=obj.nodes{inode};
            wrapper=node.getwrapper(icpu);
            if ~isempty(wrapper)
              out=wrapper.getbuildargs();
            end
            if isempty(out), out= ' '; end
        end
        
    end

    %% Abstract methods
    methods(Abstract,Static,Hidden)
      % method to return default node, used in constructor to fill
      % non-defined nodes
      node = getdefaultnode(nodenr); 
    end
end




