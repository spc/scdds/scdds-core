classdef (Abstract) SCDDSclass_parameter <  matlab.mixin.Heterogeneous
  % Generic superclass for all SCDDS parameters
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  properties (Constant,Hidden)
    matlabseparator = '.' % structure separator character used in MAtlab/Simulink
    marteseparator  = '-' % structure separator character used in MARTe2
  end
  
  properties(Abstract)
    marteclassname       % class name for generating MARTe2 cfg file
  end
  
  properties (SetAccess = protected)
    unlinked             % parameter without an external source, no actualization will be performed
    actualizable         % true if after MDS data retrieval phase the parameter is actualizable
    value                % value of the parameter
    fullparam            % Full expansion of the model target parameter (paramtpstruct+destparam)
    paramtpstruct        % target tunable parameter structure, if left empty it will be filled once binding to an algorithm
    destparam            % Model parameter w/o tunable parameters structure
    datadictname         % data dictionary hosting the parameter, if empty base workspace
    modelname            % name of the Simulink model using the parameter
    getcommand           % full command for getting the value (callable by matlab eval)
    classname            % class name for logging
  
    skippable           = false % true if the parameters loading can be skipped during the actualization process and default value used, unused for now
    loadpermissivity    = false % if true, load error will not cause an error (warning only)
    forcetype           = false  % if true, will force target data type to be same as source upon actualization
    
    sourcelogicaltype   % expected type for a logical parameter from source
  end
  
  properties (Hidden, Access = protected)
        assignvar           % actualizedata specific
        assignstring        % actualizedata specific   
        denanstring         % actualizedata specific
        caststring          % actualizedata specific
  end
  
  properties(Access = public)

  end
  
  methods(Abstract)
     obj = connect(obj,varargin)         % connect to source of data
     [obj,data] = getdata(obj,varargin); % get data from source
     printinfo(obj) % print object information
     entrystring = genMARTe2entry(obj,varargin); % Generate MARTe2 configuration entry

     % Generate C++ code 
     % gencode(obj)
     %
  end
  
  methods(Access = public)
    function actualize(obj, varargin)
      obj=obj.preactualize;
      if ~obj.actualizable, return; end
      [obj,actchk] = obj.actualizationchecks;
      if actchk
        obj.postactualize;
      end
    end
  end
    
  methods(Access = protected) 
    function obj=pregetdatacustom(obj)
      % custom code for pre-getdata, implement per derived class
    end
    
    function obj=preactualizecustom(obj)
      % custom code for pre-actualization, implement per derived class
    end
    
    function obj=postactualizecustom(obj)
     % custom code for post-actualization, implement per derived class
    end
    
    function obj=dispactualize(obj)
      % re-defined in derived classes
    end
    
    function [obj,actchk] = actualizationchecks(obj)
      % checks for actualization
      
      actchk = false; % default
      
      % error handler if not actualizable
      if ~obj.actualizable
        obj.loaderrorhandler(...
          'SCDDScore:parameter',...
          '%s: parameters not actualizable (preactualize fails), actualization skipped!',...
          obj.paramtpstruct);
        return;
      end
      
      assert(~isempty(obj.modelname),'modelname should not be empty')

      % Find target variable in base workspace or in model workspace
      try
        if isempty(obj.modelname) || strcmp(obj.modelname,'base')
          % TP structure in base workspace
          target = evalin('base',obj.assignvar);
        elseif exist(obj.modelname,'file')==4
          target = Simulink.data.evalinGlobal(obj.modelname,obj.assignvar);
        else
          error('obj.modelname %s invalid. Should refer to a simulink model or be set to ''base''');
        end
      catch ME
        % target not found, skip actualization;
        obj.loaderrorhandler('SCDDScore:parameter',...
          'Could not get target variable %s from workspace.\n Error was: %s',...
          obj.assignvar,getReport(ME));
        return
      end
      
      % check source value against target value
      % Enum special (to be cleaned up)
      if isenum(target)
        actchk=true;
        return
      end
      
      % check dimensions
      sourcedim = size(obj.value);
      targetdim = size(target);
      actchk=true;
      if numel(sourcedim)~=numel(targetdim)
        obj.loaderrorhandler('SCDDScore:parameter','%s: number of dimensions not matching, actualization skipped!',obj.fullparam);
        actchk=false;
      end
      
      %%% handle vector transposition
      if numel(sourcedim)==2 && ~isscalar(obj.value)
        if (sourcedim(1)==1 && targetdim(2)==1) || (sourcedim(2)==1 && targetdim(1)==1)
          % we have a vector that needs to be transposed
          obj.value=obj.value';
          sourcedim=size(obj.value);
          % TODO: in the new implementation, use obj.value for
          % actualization and not an evaluated command ?
          obj.assignstring=[obj.assignstring(1:numel(obj.assignstring)-1) ''';']; % add a transpose before last ;
          %obj.assignstring=sprintf('%s=%s'';',obj.assignvar,obj.getcommand);
        end
      end
      if actchk && ~all(sourcedim == targetdim)
        % build dims string for log message 
        srcdimlog='[';
        for ii=1:numel(sourcedim)-1, srcdimlog=[srcdimlog num2str(sourcedim(ii)), ',']; end
        srcdimlog=[srcdimlog num2str(sourcedim(ii+1)) ']'];
        trgdimlog='[';
        for ii=1:numel(targetdim)-1, trgdimlog=[trgdimlog num2str(targetdim(ii)), ',']; end
        trgdimlog=[trgdimlog num2str(targetdim(ii+1)) ']'];        
        obj.loaderrorhandler('SCDDScore:parameter','%s: dimensions not matching (src: %s, trg: %s), actualization skipped!',obj.fullparam,srcdimlog,trgdimlog);
        actchk=false;
      end
      
      % Check type
      sourceclass=class(obj.value);
      targetclass=class(target);
      %%% handle casting to logical
      boolcast = false;
      if isa(target,'logical')
        if ~isa(obj.value,'uint8')
          obj.loaderrorhandler('SCDDScore:parameter',...
            '%s: source type must be %s for logical parameters. Found: %s',...
            obj.fullparam,obj.sourcelogicaltype,sourceclass)
          actchk=false;
        end
        if actchk
          sourceclass='logical';
          boolcast=true;
        end
      end
      
      % Handle class mismatch - either force or abort
      if actchk && ~strcmp(sourceclass,targetclass)
        if obj.forcetype, msgaction = sprintf('forcing Simulink target type to %s\n',sourceclass);
        else,             msgaction = 'actualization skipped';
        end
        
        obj.loaderrorhandler('SCDDScore:parameter',...
          '%s: Data types not matching. Source: %s, Target: %s\n %10s',...
          obj.paramtpstruct,sourceclass,targetclass,msgaction);
        if ~obj.forcetype, actchk=false; end % if forcetype, allow overwriting anyway
      end
      
      % MARTe2 checks matrix orientation, but MDSplus - MATLAB
      % interface do not provide in information (matrix
      % orientation) to do this. Or, probably, I misunderstood and
      % matrix orientation is always preserved
      
      if isempty(obj.caststring)
        ME = MException('SCDDScore:caststringundefined', ...
             'SCDDS core internal error, caststring undefined');
        throw(ME);
      end
  
      if boolcast
        obj.caststring=sprintf('%s=logical(%s);',obj.assignvar,obj.assignvar);
      end
    end
  end

  
  methods(Sealed, Hidden=true)
    
    function obj=preactualize(obj)
      % connect to data source and get object value
      if ~obj.unlinked
        if ~obj.actualizable, return; end
        % Checking if data can be retrieved, updating get command
        obj=obj.pregetdatacustom;
        [obj, obj.value]=obj.getdata;
        if ~obj.actualizable, return; end
        obj.dispactualize;
        obj=obj.setactualizecmds;
        obj=obj.preactualizecustom; % optional subclass-specific actions
      else
        fprintf('Parameter: %s is unlinked, skipping\n',obj.fullparam);
        obj.actualizable = false;
      end
    end
    
    function obj = setactualizecmds(obj)
      % defines some commands to be performed after actualization
      pointspos        = strfind(obj.fullparam,'.');
      baseparam        = obj.fullparam(1:pointspos(1)-1);
      structparam      = obj.fullparam(pointspos(1):end);
      obj.assignvar    = sprintf('%s.Value%s',baseparam,structparam);
      obj.assignstring = sprintf('%s=%s;',obj.assignvar,obj.getcommand);
      obj.denanstring  = sprintf('%s(isnan(%s))=0;',obj.assignvar,obj.assignvar);
      obj.caststring   = sprintf('%s=%s;',obj.assignvar,obj.assignvar);
    end

    function obj=postactualize(obj)
      % take the stored value and assign it to the tunable parameter
      if ~obj.unlinked && obj.actualizable
        evalin('base', obj.assignstring);
        evalin('base', obj.denanstring);
        evalin('base', obj.caststring);
      end
      obj = obj.postactualizecustom; % optional subclass-specific actions
    end
    
    function loaderrorhandler(obj,msgid,varargin)
      % handler for loading errors, thrown as warnings or errors
      if obj.loadpermissivity
        warning(msgid,varargin{:}); % throw warning
      else
        ME=MException(msgid,varargin{:}); % throw error message
        ME.throwAsCaller;
      end
    end
    
    function obj = setparamstructure(obj, structname)
      assert(nargout==1,'must set out argument for set method')
      if isempty(obj.paramtpstruct)
        obj.paramtpstruct = structname;
      end
      obj.fullparam=[obj.paramtpstruct '.' obj.destparam];
    end
  end
end
