function isvalid = SCDsignal_isvalid(SCDsignals)

% true if GOOD and RUNNING
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
isvalid = false(size(SCDsignals));

for ii=1:numel(SCDsignals)
  isRunning = int8(SCDsignals(ii).ProductionState) == int8(ProductionState.RUNNING);
  isGood    = int8(SCDsignals(ii).QualityTag)      == int8(QualityTag.GOOD);
  isvalid(ii) = isRunning & isGood;
end
end