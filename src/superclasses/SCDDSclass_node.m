classdef (Abstract) SCDDSclass_node < SCDDSclass_component
  % node class containing wrappers or algos
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

  properties
    buildable = false;
  end
  
  properties (SetAccess = private)
    wrappers % info about thread wrappers contained in this node
  end

  properties (SetAccess = protected)
    active = false; % not active by default
    nodenr
    ncpu           % number of CPUs
    cpuactive
    cputype
    buildcfg
    haswavegen   = false % has wavegen source
    hasadc       = false % has adc source
    hasextsource = false % has external source (from other nodes)
    type = 'node';
  end
  
  properties (Access = private)
  end
  
  methods(Abstract)
    defaultnodeconfig(obj,nodenr)
  end
  
  methods
    function obj = SCDDSclass_node(nodenr)
      % constructor
      name = sprintf('SCD_rtc_%02d',nodenr);
      obj.nodenr = nodenr;
      obj.name    = name;
      obj.algos   = [];
      obj.ddname  = [name,'.sldd'];
      obj.mdlname = [name,'.slx'];
      obj.wrappers = [];
      
      % get node-specific configurations
      obj = defaultnodeconfig(obj,nodenr);
    end
    
    function obj = addwrapper(obj,wrapperObj,cpunr,varalgo,isactive)
      % add wrapper to a node at a given cpunumber
      % varalgo is the variant subsystem number used to select this wrapper
      % isactive (true by default) sets whether the addition of the wrapper
      % also sets the cpu to be active;
      assert(nargout==1,'must assign output for addwrapper method')
      
      % by default, activate CPU when adding a wrapper
      if nargin==4, isactive=true; end
      
      assert(isa(wrapperObj,'SCDDSclass_wrapper'));
      
      for ii=1:numel(obj.wrappers)
        assert(~isequal(wrapperObj,obj.wrappers{ii}.wrapperobj),...
          'wrapper %s is already present in node %d thread %d',...
          obj.wrappers{ii}.wrapperobj.name,obj.nodenr,cpunr)
      end
      
      mywrapper.wrapperobj = wrapperObj;
      mywrapper.cpunr      = cpunr;
      mywrapper.varalgo    = varalgo;
      
      % add wrapper to obj
      obj.wrappers{cpunr}  = mywrapper;
      % set cpu of this wrapper to active
      obj.cpuactive(cpunr) = isactive;
      % set receiving node cpu sample time to wrapper sample time
      obj.timing.thperiod(cpunr) = wrapperObj.timing.dt;
    end
    
    function obj = setactive(obj,value)
      % set the node to be active
      assert(islogical(value),'value must be boolean')
      obj.active = value;
    end
    
    function updatetemplatetp(obj)
      % update wrapper algos
      for ii=1:numel(obj.wrappers)
        obj.wrappers{ii}.wrapperobj.updatetemplatetp;
      end
      % update node algos
      for ii=1:numel(obj.algos)
        if ~isempty(obj.algos(ii))
          obj.algos(ii).updatetemplatetp;
        end
      end
    end
    
    function printinfo(obj)
      % print wrapper and algo info
      fprintf('\nNode%02d, CPUs:%02d, name:%s, dt=%5.3fms\n',...
              obj.nodenr,obj.ncpu,obj.name,obj.timing.dt*1e3)
      for wrapper = obj.wrappers
        fprintf(' CPU %d wrapper: ',wrapper{:}.cpunr);
        wrapper{:}.wrapperobj.printinfo;
      end
      if ~isempty(obj.algos)
        fprintf(' Node Algos:\n')
        for ii=1:numel(obj.algos)
          algo = obj.algos(ii);
          fprintf('%s',algo.getname);
        end
      end
      fprintf('\n');
    end
    
    function build(obj,cpunr)
      if nargin==1
        for ii=1:numel(obj.wrappers)
          mywrapper = obj.wrappers{ii}.wrapperobj;
          if ~isempty(mywrapper)
            mywrapper.build;
          end
        end
      else % build a specific wrapper
        mywrapper = obj.wrappers{cpunr};
        mywrapper.build;
      end
    end
   
    function ncpu = getncpu(obj)
        ncpu=obj.ncpu;
    end

    function cpuactive = getcpuactive(obj)
        cpuactive=obj.cpuactive;
    end
    
    function csname = getbuildcfgset(obj, cpunr)
        csname=obj.buildcfg.confset{cpunr};
    end
    
    function out = getwrapper(obj, icpu)
      out=[];
      for wrapper = obj.wrappers
        if wrapper{:}.cpunr==icpu
          out=wrapper{:}.wrapperobj;
        end
      end
    end
    
  end
  
  methods(Access = protected)

  end
end
