classdef SCDclass_task < matlab.mixin.Heterogeneous
    % Superclass for general purposes init and term tasks
    % the superclass matlab.mixin.Heterogeneous allows
    % bilding of lists of mixed kind parameters in the
    % expcode configuration (and later in C++ code)
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    properties (Access = protected)
        id                  % Task identification
        datadictionary      % data dictionary hosting the parameter, if empty base workspace
        modelname           % name of the Simulink model using the parameter
        classname           % class name for logging
        cparser             % constructor parameters parser
        
        % Properties for a MDS+ related task (not necessarly used)
        mdsserver           % MDS+ server for a MDS+ related task
        mdstree             % MDS+ tree for a MDS+ related task
        tdiexprmodel        % TDI expression to retrieve data when invoked with -1 shot
        tdiexprshot         % TDI expression to retrieve data when invoked with a give shotno, if empty the -1 is used
        tdiexprused         % TDI expression actually used
        value               % value from MDS+
        getcommand          % full command for getting the value (callable by matlab eval)
        
        marteclassname      % marte class name
    end
    
    properties 
        verbose             % Verbosity of the class
    end
    
    methods
        
        %function obj=SCDclass_mdsparam(srcsrv, srctree, srctdimodel, srctdishot, modelname, destparam)
        function obj=SCDclass_task(id,varargin)
            % MDS source and model destination constructor
            
            obj.cparser=inputParser;
            addRequired(obj.cparser,'id',@(x) ischar(x));

            addParameter(obj.cparser,'srcsrv','tcvdata',@(x) ischar(x));
            addParameter(obj.cparser,'srctree','tcv_shot',@(x) ischar(x));
            addParameter(obj.cparser,'srctdimodel','',@(x) ischar(x));
            addParameter(obj.cparser,'srctdishot','',@(x) ischar(x));
            addParameter(obj.cparser,'modelname','',@(x) ischar(x));
            addParameter(obj.cparser,'datadictname','',@(x) ischar(x));  
         
            obj.verbose=1;
        end
        
        function obj=parseconstructorcommon(obj, id, varargin)    
            parse(obj.cparser,id,varargin{:}{:});
            
            obj.id=obj.cparser.Results.id;
            obj.mdsserver=obj.cparser.Results.srcsrv;
            obj.mdstree=obj.cparser.Results.srctree;
            obj.tdiexprmodel=obj.cparser.Results.srctdimodel;
            if isempty(obj.cparser.Results.srctdishot)
                obj.tdiexprshot=obj.cparser.Results.srctdimodel;
            else
                obj.tdiexprshot=obj.cparser.Results.srctdishot;
            end
            obj.modelname=obj.cparser.Results.modelname;
            obj.datadictionary=obj.cparser.Results.datadictname;
        end    
        
        function name=getmodelname(obj)
            name=obj.modelname;
        end
        
        function [mdsserver] = getMDSserver(obj)
          mdsserver = obj.mdsserver;
        end
        
        function [mdstree] = getMDStree(obj)
          mdstree = obj.mdstree;
        end
        
        function str = genMARTe2MDSsourcestr(obj)
          str = sprintf(' +Connection_%s_%s = {\n  Class=MDSObjConnection\n  Server=%s\n  Tree=%s',obj.mdsserver,obj.mdstree,obj.mdsserver,obj.mdstree);        
        end
        
    end
    
    % Not abstract methods common to all child classes
    methods
        function mdsconnect(obj, shot)
           mdsconnect(obj.mdsserver);
           s=mdsopen(obj.mdstree, shot);
           str=sprintf('SCDclass_task (%s), failed opening MDS+ tree', obj.id);
           assert(s==shot, str);
        end
        
        function obj=actualizegetcmd(obj, cmdstring, shot)
            if(shot==-1)
                obj.getcommand=sprintf(cmdstring, obj.tdiexprmodel);
                obj.tdiexprused=obj.tdiexprmodel;
            else
                obj.getcommand=sprintf(cmdstring, obj.tdiexprshot);
                obj.tdiexprused=obj.tdiexprshot;
            end
        end
        
        function printinfocommon(obj)
            fprintf('%s (class %s):\n', obj.id, obj.classname);
            fprintf('  Simulink model: ''%s'', data dictionary: ''%s''\n', obj.modelname, obj.datadictionary);
            fprintf('  MDS+ source server: ''%s'', Tree: ''%s''\n', obj.mdsserver, obj.mdstree);
            fprintf('  MDS+ TDI expressions, model: ''%s''', obj.tdiexprmodel);
            if(strcmp(obj.tdiexprmodel, obj.tdiexprshot))
                fprintf(', shot: same\n');
            else
                fprintf(', shot: ''%s''\n', obj.tdiexprshot);
            end
        end
        
        function out = getid(obj)
            out = obj.id;
        end
        
      
        function obj = setmodelname(obj, modelname)
            if(isempty(obj.modelname))
                obj.modelname = modelname;
            end
            
        end
        
        function obj = setdatadictionary(obj, ddname)
            if(isempty(obj.datadictionary))
                obj.datadictionary = ddname;
            end
        end
        
    end
    
    methods(Access=protected)
    
        function entrystring = genMARTe2entrycommon(obj, shot)
            obj=obj.actualizetdiexpr(shot);
            %entrystring = sprintf('+%-50s = { Class=%-30s Path=%-40s',obj.gettargetparammarte,obj.marteclassname,obj.tdiexprused);
            entrystring = sprintf('+%-50s = { Class=%-30s Path=%-40s',obj.id,obj.marteclassname,obj.genMARTe2MDStdiexpression); 
        end
        
        function str = genMARTe2MDStdiexpression(obj)
            % Duplicate first backslash
%             if(obj.tdiexprused(1)=='\' && not(obj.tdiexprused(2)=='\'))
%                 martetdi=['\' obj.tdiexprused];
%             else
%                 martetdi=obj.tdiexprused;
%             end
            % Duplicate backslashes
            martetdi=strrep(obj.tdiexprused, '\', '\\');
            %substitute every " with '
            martetdi=strrep(martetdi, '"', '''');
            %put double string quota
            martetdi=['"' martetdi '"'];
            str=martetdi;
        end
        
        function obj=actualizetdiexpr(obj, shot)
            if(shot==-1)
                obj.tdiexprused=obj.tdiexprmodel;
            else
                obj.tdiexprused=obj.tdiexprshot;
            end            
        end
        
    end    
    
    % Abstract method actually implemented by child classes
    methods (Abstract)
        
       
        % Polymorphic inits
        init(obj, shot)
        
        % Polymorphic terms
        %term(obj, shot)
        
        % Gets data from mds, mds connection is assumed already estabilished
        % and tree opened
        [obj, value] = getdata(obj, shot)
        
        % Generate MARTe2 configuration entry
        entrystring = genMARTe2entry(obj, shot)
        
        % Prints the parameter info summary
        printinfo(obj)
        
    end
end

