classdef (Abstract) SCDDSclass
 % class with utilities for interacting with SCDDS expcodes
 %
 % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
 % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

 properties (Constant,Abstract)
   codegenfolder;
   projectfolder;
 end
 
 methods (Static)
  function help()
   help(mfilename);
  end
  
  function list()
   SCDDS.help;
  end
  
  function close_slx(saveflag)
    % close all open simulink models, save if saveflag=1;
    if nargin==0, saveflag = 1; end
    assert(saveflag == 1 || saveflag == 0,'saveflag must be 1 or 0')
    if saveflag==1
      closemsg = 'Saving and Closing %s\n';
    else
      closemsg = 'Discarding changes and closing %s\n';
    end
    
    while ~isempty(bdroot)
      fprintf(closemsg,bdroot);
      close_system(bdroot,saveflag)
    end
  end
  
  function clean_dd()
    debug=0;
    % Close all data open dictionaries
    fprintf('Closing all data dictionaries and discarding changes\n')
    Simulink.data.dictionary.closeAll('-discard');
    if debug
      disp('Opened sldds after cleanup:');
      openDDs = Simulink.data.dictionary.getOpenDictionaryPaths;
      disp(openDDs);
    end
    % Cleans unversioned data dictionaries
    fprintf('Cleaning unversioned data dictionaries for clean test\n')
    [s,w] = system('git rev-parse --show-toplevel');  % get top-level dir
    if s~=0, warning('clean_dd failed, message:\n%s\n',w); return; end
    oldpwd = pwd; cd(deblank(w));
    system('git clean -xf *.sldd');  % clean local sldd
    system('git submodule foreach --quiet ''git clean -fx *.sldd'''); % clean submodules too
    cd(oldpwd)
  end
  
  function setcachefolder(folder)
    % Set Cache Folder for experimental code (avoid conflicts with other tmp files)
    fprintf('Setting Simulink Cache folder to %s\n',folder)
    Simulink.fileGenControl('set',...
      'CacheFolder',folder,...
      'createdir',true);
  end
  
  function setcodegenfolder(folder)
    fprintf('Setting code generation folder to %s\n',folder)
    Simulink.fileGenControl('set',...
      'CodeGenFolder',folder,...
      'createdir',true);
  end
 end
end
