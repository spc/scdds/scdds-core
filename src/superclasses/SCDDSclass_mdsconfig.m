classdef SCDDSclass_mdsconfig
  % Class for site-dependent MDSplus configuration
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  properties
    mdsserver char = ''; % server name
    mdstree   char = 'scdds';  % tree used
    mdsinterface MDSplus_interface = MDSplus_mdsipmex; % MDSplus interface class with default
  end
  
  methods
    function obj = SCDDSclass_mdsconfig(whereami)
      % Get site-dependent MDSplus configuration
            % Set defaults for our case

      if contains(whereami,'EPFL')
        obj.mdsserver = 'spcscddev';
        obj.mdsinterface = MDSplus_mdsipmex;
      elseif contains(whereami,'PSFC')
        obj.mdsserver='alcdaq6.psfc.mit.edu';
        obj.mdsinterface = MDSplus_java;
      else
        warning('don''t know how to treat location %s',whereami);
        obj.mdsserver='';
        obj.mdsinterface = MDSplus_java; % dummy
      end
    end
  end
end
