classdef SCDclass_mdswgsigsingle < SCDclass_mdswg
    % This class loads a signal from MDS+ and puts it into
    % the target wavegen timeseries, the signal postition
    % in the target timeseries structure is remapped according to
    % the optional destindex constructor parameter. If it is not
    % specified, no remapping takes places (1-1 load)
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

    properties 
        destidx
    end
    
    methods
    
        function obj=SCDclass_mdswgsigsingle(srctdi, destwavegen, varargin)
            %obj@SCDclass_mdswg(srctdi, destwavegen, varargin);
            obj@SCDclass_mdswg();
            
            destidxvalchecker=@(x) isscalar(x);
            %addParameter(obj.cparser,'destindex',1,destidxvalchecker);
            obj.cparser.addParameter('destindex',1,destidxvalchecker)

            obj=obj.parseconstructor(srctdi, destwavegen, varargin);
                        
            %parse(p,varargin{:});
            obj.destidx=obj.cparser.Results.destindex;
            
            obj.classname=mfilename;
            obj.marteclassname='MDSWgSigSingle';
        end
       
    end
        
    methods 
        
         function actualizedata(obj, shot)
            try
                mdsconnect(obj, shot);
                
                targetfullexpansion=[obj.wavegenbasestruct,'.',obj.wavegentarget];
                
                if obj.verbose==1
                    if obj.overrideshot == 1
                        localshot = obj.overrideshotn;
                    else
                        localshot = shot;
                    end
                    fprintf('Actualizing wavegen: ''%s'' [%d] <- ''%s'' (%s, shot %d)\n', ...
                        targetfullexpansion, obj.destidx,  ...
                        obj.tdiexpr, ...
                        obj.classname, localshot);
                end
                
                value=obj.getdata();       
                
                baseparam=obj.wavegenbasestruct; % name of base workspace structure
                structparam=obj.wavegentarget; % name of target field
                wgentryval = evalin('base',baseparam); % structure of wavegen in base workspace
                
                assert(isfield(wgentryval,structparam),...
                    'Wavegen field %s does not exist in target structure %s. ',structparam,baseparam)
                
                % assign field from wavegen entry value
                ddtssamples=numel(wgentryval.(structparam).Time);
                if ddtssamples~=numel(value.Time)
                    % the dd timeseries has different dims w.r.t. the signals
                    % to be loaded, we have to load the timeseries as it is
                    wgentryval.(structparam)=value;
                else
                    % the dd timeseries has the same size than the signals
                    % to be loaded, we can do a partial loading
                    wgentryval.(structparam).Time=value.Time;
                    wgentryval.(structparam).Data(:,obj.destidx)=value.Data;
                end 
                
                assignin('base','temp',wgentryval);
                assigncmd=sprintf('%s=temp;',baseparam);
                evalin('base',assigncmd);
                evalin('base','clear temp');
            catch ME
                warning('SCDclass_mdswgsigsingle:actualizationerror','Actualization error for wavegen %s. Actualization skipped. Error is:\n%s',obj.wavegentarget,getReport(ME));                
                
            end
        end
        
        function value=getdata(obj)
            value=timeseries;
            
            % Getting the timebase of the model
            tstart=obj.timebasestart;
            dt=obj.timebasedt;
            tstop=obj.timebasestop;
            
            timebase=tstart:dt:tstop;
          
            timebaseoffset=-dt/100; 
            %timebaseoffset=0; 
            
            value.Time = timebase+timebaseoffset;
            
            value.Data = single(zeros(numel(timebase),1));
 
            %data=tdi(obj.tdiexpr);   
            %value.Data = single(interp1(data.dim{1},data.data,timebase,'linear',0))';
             
            dbdata=mdsvalue(obj.tdiexpr);
            dbtime=mdsvalue(['dim_of(' obj.tdiexpr ')']);
            value.Data = single(interp1(dbtime,dbdata,timebase,'linear',0))';
            
            firstindexes=find(timebase<dbtime(1));
            lastindexes=find(timebase>dbtime(end));
            value.Data(firstindexes)=single(dbdata(1));
            value.Data(lastindexes)=single(dbdata(end));

        end
        
        
        function out=gettargetwavegen(obj)
            if(isempty(obj.wavegenbasestruct))
                out=sprintf('%s[%d]',obj.wavegentarget, obj.destidx);
            else
                out=sprintf('%s.%s[%d]',obj.wavegenbasestruct,obj.wavegentarget, obj.destidx);
            end
        end
        
        function printinfo(obj)
            printinfo@SCDclass_mdswg(obj);
            if(obj.destidx~=1)
                fprintf('  Remapping: destination signal index is %d\n', obj.destidx);
            end
        end
        
        function entrystring = genMARTe2entry(obj, shot)
            if obj.destidx~=1
                error('SCDclass_mdswgsigsingle:marteerror', 'MARTe2 remapping not yet implemented');
            end
            entrystring=[genMARTe2entry@SCDclass_mdswg(obj,shot) ' }'];
        end
       
        function autopopulatemds(obj, shot)
            baseparam=strrep(obj.wavegenbasestruct,'SCDsimdata','SCDrefdata'); % name of base workspace structure
            
            structparam=obj.wavegentarget; % name of target field
            wgentryval = evalin('base',baseparam); % structure of wavegen in base workspace
            
            data = wgentryval.(structparam).Data(:,obj.destidx);
            dim = wgentryval.(structparam).Time;
            units = wgentryval.(structparam).DataInfo.Units;
            
            autopopulatemds@SCDclass_mdswg(obj, shot, obj.tdiexpr, dim, data, units, [baseparam '.' structparam '[' num2str(obj.destidx) ']']);             
        end     
        
        function out = castdata(obj, in)
            out = single(in);
        end
        
        function out = getnsigs(obj)
          out = 1;
        end
        
    end
end

