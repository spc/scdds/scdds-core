classdef SCDDSclass_mdsparnumeric < SCDDSclass_MDSparameter
    % General purpose numeric loader MDS+ parameter class
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
    
    properties
      marteclassname = 'MDSParameter';
    end

end

