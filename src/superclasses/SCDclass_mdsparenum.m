classdef SCDclass_mdsparenum < SCDDSclass_MDSparameter
    % A string enumerated MDS parameter, for uint16 enum codes
    % 
    % behavior in MATLAB:
    %  if a string match between MDS string and matlab num string is 
    %  asserted, the enum choice is loaded into the tun parameter
    %  if not a warning is issued and the param is left untouched
    % behavior in MARTe2:
    %  the same than before but the enum map is used to load the
    %  equivalent uint16 code on the MARTe2 side tunable parameter
    %  if a string match is not detected, the parameter is invalidated
    %
    % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
    % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

    
    properties
      marteclassname = 'MDSParEnum';
    end
    
    properties(Access=private)
        enumclass
        valuemap
        codemap
    end
    
    methods(Access = protected)
        function obj = preactualizecustom(obj)      
          if ~obj.actualizable, return; end
          if ~obj.unlinked
            % Enumeration map fill
            obj=obj.initenummap();
            
            % value search
            found=false;
            for ii=1:numel(obj.valuemap)
              if strcmp(obj.value,char(obj.valuemap(ii)))
                found = true;
                break;
              end
            end
            
            if ~found
              warning('SCDclass_mdsparenum:actualize','No match between MDS and enum values, parameter %s not actualized', obj.assignvar);
              obj.actualizable=false;
              return
            end
            obj.assignstring=sprintf('%s=%s.%s;',obj.assignvar,obj.enumclass,obj.value);
            obj.denanstring='';
            obj.caststring='';
          end
        end    
        
        function out = casttomds(~, ~)
            warning('SCDclass_mdsparenum:casttomdserror','Cast to mds not available for this class.');
            out = [];
        end
    end
    
    methods(Access = private)
       
        function obj = initenummap(obj)
            % TODO: probably Simulink.data.evalinGlobal better than
            % evalin('base'
            
            basecmd = ['class(' obj.assignvar ');'];
            obj.enumclass  = evalin('base',basecmd);
            basecmd = ['enumeration(' obj.assignvar ');'];
            obj.valuemap  = evalin('base',basecmd);
            basecmd = ['uint16(enumeration(' obj.assignvar '));'];
            obj.codemap     = evalin('base',basecmd);
        end
        
    end
    
    methods
 
        function entrystring = genMARTe2entry(obj, shot)
            entrystring=obj.genMARTe2entrypreamble(shot);
            
            % to populate obj.assignvar
            %obj=obj.setactualizecmds(shot);
            obj=obj.setactualizecmds();
              
            % Enumeration map fill
            obj=obj.initenummap();
            
            enumval  ='EnumVal = {';
            enumcode ='EnumCode = {';
            for ii=1:numel(obj.valuemap)
                enumval=[enumval '"' char(obj.valuemap(ii)) '"'];
                if ii~=numel(obj.valuemap)
                    enumval=[enumval ','];
                end
            end
            enumval=[enumval, '}'];
            for ii=1:numel(obj.codemap)
                enumcode=[enumcode num2str(obj.codemap(ii))];
                if ii~=numel(obj.codemap)
                    enumcode=[enumcode ','];
                end
            end
            enumcode=[enumcode '}']; 

            entrystring=[entrystring ' ' enumcode ' ' enumval '}'];
        end  
        
        function [obj,value] = getdata(obj, varargin)
          obj=obj.actualizegetcmd(obj.usedshot);
          value=eval(obj.getcommand);
          obj.actualizable=true;
        end
    end
end

