classdef (Abstract) SCDDSclass_component
  % Superclass for SCDDS component
  % Can be a wrapper or a node
  % Includes functions to link associated data dictionaries
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  properties(Abstract)
    buildable logical
  end
  
  properties
    loadverbose = 1;
  end
  
  properties
    name    % name
    mdlname % simulink model name
    ddname  % data dictionary name
    algos   % algorithms contained in this wrapper
    timing  % sample time
  end

  methods
   
    function obj = addalgo(obj,algo)
      % add an algorithm object
      
      assert(nargout==1,'must assign output for addalgo method')
      assert(isa(algo,'SCDDSclass_algo'),'algo is a %s and not an SCDDSclass_algo',class(algo));
      assert(algo.gettiming.dt == obj.timing.dt,...
        'Algorithm %s sample time: %f does not match %s sample time: %f',...
        algo.getname,algo.gettiming.dt,obj.name,obj.timing.dt)
      obj.algos = [obj.algos;algo];
    end
    
    function obj = linkalgodd(obj, nameprefix)
      % Set up data dictionary to link to all algo data
      % dictionaries as data sources
      
      % wrapper data dictionary
      obj.printlog('linking algo data dictionaries to %s\n',obj.ddname);
      ddObj = Simulink.data.dictionary.open(obj.ddname);
      
      % already linked sources
      prevsources = ddObj.DataSources;
      prevsources_algo = prevsources(startsWith(prevsources,nameprefix));
      
      % make algo data dictionary list 
      reqsources = cell(numel(obj.algos),1);
      for ii=1:numel(obj.algos)
        myalgo = obj.algos(ii);
        reqsources{ii} = myalgo.getdatadictionary;
      end
      
      % removing unnecessary ones
      for ii=1:numel(prevsources_algo)
        mysource = prevsources_algo{ii};
        if ~contains(reqsources,mysource)
          obj.printlog('Removing algorithm data source %s from %s',mysource,obj.ddname);
          ddObj.removeDataSource(prevsources_algo{ii})
        end
      end
      
      % add new ones not yet present
      for ii=1:numel(reqsources)
        mysource = reqsources{ii};
        assert(startsWith(mysource,nameprefix),...
          'attempting to add algo dd: %s that does not start with ''%s''-aborting',mysource,nameprefix);
        if contains(prevsources,mysource)
          obj.printlog('Not adding algorithm data source %s - already exists',mysource);
        else
          obj.printlog('Adding algorithm data source %s to %s',mysource,obj.ddname);
          ddObj.addDataSource(mysource);
        end
      end
    end
       
    function printlog(obj,varargin)
      % printlog, allows fprintf()-like expressions
      if obj.loadverbose==1
        fprintf('%s, ',obj.name);
        fprintf(varargin{:}); fprintf('\n');
      end
    end
    
    function build(obj)
      if ~exist('scddsalgoinfo.h','file')
        write_SCDDS_header('n/a');
      end
      % perform build
      rtwbuild(obj.name);
    end
    
    function deploy(obj)
      % prepare git tag
      dogittag=true;
      writegitinfoheader(dogittag)
      
      % build
      obj.build;
    end
  end
end
