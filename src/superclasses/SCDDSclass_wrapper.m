classdef (Abstract) SCDDSclass_wrapper < SCDDSclass_component
  % Wrapper class containing algos
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  properties
    buildable   = true
    createdd    = true % automatically create fresh dd during wrapper init
  end
  
  methods
    function obj = SCDDSclass_wrapper(name,dt)
      if nargin<2, dt=1e-3; end % default wrapper period
      
      obj.name    = name;
      obj.algos   = [];
      obj.ddname  = [name,'.sldd'];
      obj.mdlname = [name,'.slx'];
      obj.timing  = struct('dt',dt);
      assert(~isempty(which(obj.mdlname)),...
        'could not find %s for SCD component %s',obj.mdlname,obj.name);
    end
    
    function updatetemplatetp(obj)
      % update node algos tunable parameters
      for ii=1:numel(obj.algos)
        if isempty(obj.algos(ii)), continue; end
        obj.algos(ii).updatetemplatetp;
      end
    end
    
    function printinfo(obj)
      fprintf('%s, dt=%5.3f[ms]\n',obj.name,obj.timing.dt*1e3);
      fprintf('   Algos:');
      if isempty(obj.algos), fprintf('  none'); end
      fprintf('\n');
      for ii=1:numel(obj.algos)
        myalgo = obj.algos(ii);
        fprintf('  %9s%s\n','',myalgo.getname);
      end
    end
    
    function init(obj)
        
        % if property holds true, automatically create fresh dd for wrapper
        % when not yet there
        if obj.createdd
            % force close and delete linked sldd and create fresh
            if any(contains(Simulink.data.dictionary.getOpenDictionaryPaths,obj.ddname))
                Simulink.data.dictionary.closeAll(obj.ddname,'-discard');
                % close if it exists and discard changes
            end
            delete(which(obj.ddname));

            folder = fileparts(which(obj.mdlname));
            SCDDSclass_algo.createdatadictionary(obj.ddname, folder);

            load_system(obj.name);
            % link wrapper slx model to new sldd
            set_param(obj.name,'DataDictionary',obj.ddname);
        end

        
        for ii=1:numel(obj.algos)
            obj.algos(ii).init;
        end
    end
    
    function setup(obj)
        % only open dd if created during initialization
        if obj.createdd
            dd = Simulink.data.dictionary.open(obj.ddname);     
        end
        
        for ii=1:numel(obj.algos)
            obj.algos(ii).setup;
            
            if obj.createdd % only add algo dd params here if dd was freshly created
                mydatasource = obj.algos(ii).datadictionary;
                fprintf('adding data source %s to %s\n',mydatasource,obj.ddname)
                dd.addDataSource(mydatasource); 
            end
        end
        
        
    end
    
    function build(obj)
      assert(obj.buildable,'objects has property buildable=false');
      build@SCDDSclass_component(obj); % call superclass build method      
    end
    
    function out = getbuildargs(obj)
      out='';
      for ii=1:numel(obj.algos)
        algo = obj.algos(ii);
        if ~isempty(algo.getbuildargs())
          out=[out ' ' algo.getbuildargs()];
        end
      end
    end
  end
  
end
