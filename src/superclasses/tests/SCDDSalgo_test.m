classdef (Abstract) SCDDSalgo_test < SCDDStest
  % Abstract test superclass for SCDDSalgo tests
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  properties (Abstract=true)
    algoobj function_handle % SCD algo obj name - abstract expects implementation by sub-class
  end
  
  properties (Hidden)
    algo
  end
  
  methods (TestClassSetup)
    function clear_base(testCase)
      % clear base workspace
      %
      testCase.assertWarningFree(@() evalin('base','clear'));
    end
    
    function clear_sldd(testCase)
      SCDDSclass.clean_dd;
      testCase.assertEmpty(Simulink.data.dictionary.getOpenDictionaryPaths);
    end
    
    function setup_conf(testCase)
      testCase.assertWarningFree(@() SCDconf_setConf('SIM'));
    end
    
    function setup_algo(testCase)
      % instantiate object
      testCase.algo = testCase.algoobj(); % call function handle to instantiate
    end 
  end
  
  methods (TestClassTeardown)
    function close_dd(testCase) %#ok<MANU>
      Simulink.data.dictionary.closeAll('-discard');
    end
    
    function close_bd(testCase) %#ok<MANU>
      bdclose all;
    end
  end
  
  methods (Test,TestTags={'algos','unit'})

    function test_algo(testCase)
      disp('### TEST ALGO PRINTINFO ###');
      testCase.algo.printinfo;
      disp('### TEST ALGO INIT ###');
      testCase.algo.init; % init data dictionary & fixed params
      disp('### TEST ALGO SETUP ###');      
      testCase.algo.setup; % setup tunable parameters 
      disp('### TEST ALGO COMPILE ###');            
      testCase.algo.compile; % update model (Ctrl-D) Simulink    
      disp('### TEST ALGO TEST HARNESS ###');      
      testCase.algo.test_harness;
    end
    
  end
  
end
