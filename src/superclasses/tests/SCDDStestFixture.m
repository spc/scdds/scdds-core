classdef SCDDStestFixture < matlab.unittest.fixtures.Fixture
  % Fixture for SCDDS tests
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
       methods
        function setup(fixture)
            fixture.assertWarningFree(@() SCDDSclass.clean_dd); 
            % clean unversioned data dictionaries
            %
        end
    end
end
