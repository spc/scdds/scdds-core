classdef (Abstract) SCDDSexpcodes_test < SCDDStest
  % Abstract test class for all SCDDS expcode tests
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

  properties(SetAccess = protected,Hidden)
    expcode_obj
    shot
  end
  
  methods(TestClassSetup,ParameterCombination='sequential')
    function setup_environment(testCase)
      testCase.addTeardown(@cd,pwd);
      testCase.addTeardown(@path,path);
      testCase.addTeardown(@() SCDDSclass.close_slx(0))
      
      % Clean data dictionaries
      %
      SCDDSclass.clean_dd;

      % clear base workspace
      evalin('base','clear variables');
      
      SCDconf_setConf('SIM'); % set ConfigurationSettings for Simulation
    end 
  end
  
  methods(Test,TestTags={'expcodes'})
    
    function test_expcode_printinfo(testCase)
      testCase.expcode_obj.printinfo
    end
    
    function test_expcode_init(testCase)
        fprintf('\n=== Testing init for expcode %d: %s === \n',...
        testCase.expcode_obj.maincode,...
        testCase.expcode_obj.name);
      
        testCase.expcode_obj.init; % initialize
    end
    
    function test_expcode_setup(testCase)    
      fprintf('\n=== Testing setup for expcode %d: %s === \n',...
      testCase.expcode_obj.maincode,...
      testCase.expcode_obj.name);
    
      testCase.expcode_obj.setup; % run setup this exp code
    end
   
    function test_expcode_compile_nodes(testCase)
      % compile each node separately first
      nodes = testCase.expcode_obj.nodes;
      for inode = 1:numel(nodes)
        node = nodes{inode};
        if isempty(node)
          fprintf('skipping compilation for node %d since not configured\n',inode)
        elseif ~node.active
          fprintf('skipping compilation for node %d since not active\n',inode)
        else
          fprintf('\n === Testing Simulink compilation for node %02d of expcode %d: %s === \n',...
            inode,testCase.expcode_obj.maincode,testCase.expcode_obj.name);
          testCase.expcode_obj.compile(inode); % compile single node
        end
      end
    end
    
    function test_actualize(testCase)
      testCase.expcode_obj.actualize(testCase.shot);
    end
    
    function test_expcode_sim(testCase)
      fprintf('\n === Testing Simulink simulation of tcv.slx for expcode %d: === \n',testCase.expcode_obj.maincode)
      testCase.expcode_obj.sim; % simulate whole tcv.slx with this expcode
    end
    
    function [ok]=test_build(testCase)
      fprintf('\n === Testing build of %s for expcode %d: === \n',...
        testCase.expcode_obj.name,testCase.expcode_obj.maincode)
      ok = testCase.expcode_obj.build;
      testCase.assertTrue(ok);
    end
  end
end
