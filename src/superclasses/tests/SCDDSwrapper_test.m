classdef (Abstract) SCDDSwrapper_test < matlab.unittest.TestCase
  % Abstract test superclass for SCDDSwrapper tests
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  properties (Abstract=true)
    wrapper % wrapper class obj - abstract expects implementation by sub-class
  end
  
  
  methods (TestClassSetup)
    function clear_baseWS_close_sldd(testCase)
      % clear base workspace
      %
      testCase.assertWarningFree(@() evalin('base','clear'));
      disp('force closing models and sldds');
            
      bdclose all;
      Simulink.data.dictionary.closeAll('-discard')
      testCase.assertEmpty(Simulink.data.dictionary.getOpenDictionaryPaths);
    end
    
    
    function setup_conf(testCase)
      testCase.assertWarningFree(@() SCDconf_setConf('SIM'));
    end
    
    function setup_wrapper(testCase)
      % instantiate wrapper object
      % empty for now, to be done in derived class
    end 
  end
  
  methods (TestClassTeardown)
    function close_dd(testCase) %#ok<MANU>
      Simulink.data.dictionary.closeAll('-discard');
    end
    
    function close_bd(testCase) %#ok<MANU>
      bdclose all;
    end
  end
  
  methods (Test,TestTags={'wrapper','unit'})

    function test_wrapper(testCase)
      disp('### TEST WRAPPER INIT ###');
      testCase.wrapper.init; % init data dictionary & fixed params
      disp('### TEST WRAPPER SETUP ###');      
      testCase.wrapper.setup; % setup tunable parameters  
    end
    
  end
  
end
