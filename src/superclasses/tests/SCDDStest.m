classdef (Abstract,SharedTestFixtures={SCDDStestFixture}) ...
    SCDDStest < matlab.unittest.TestCase
  % Superclass for all SCDDDS tests. Adds common paths
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
 
  methods(TestClassSetup,Abstract)
    setup_paths(testCase); % function to setup desired paths
  end
  
end
