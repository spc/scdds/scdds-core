classdef SCDsignal
  
  properties
    Value                  {mustBeNumericOrLogical,mustBeNonempty} = 0;
    QualityTag             QualityTag                              = QualityTag.INVALID;
    ProductionState  (1,1) ProductionState                         = ProductionState.STOPPED;
  end
  
  methods
    function S = SCDsignal(varargin)      
      % constructor
      %
      % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
      % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
      if numel(varargin)>=1 && ~isempty(varargin{1}), S.Value           = varargin{1}; end
      if numel(varargin)>=2 && ~isempty(varargin{2}), S.QualityTag      = varargin{2}; end
      if numel(varargin)>=3 && ~isempty(varargin{3}), S.ProductionState = varargin{3}; end
      
      siz = S.datasize;
      assert(~(siz(1)==1 && siz(2)>1),'SCDsignal:RowVector',...
        'Row vectors are not supported due to matlab bug. Size=[%d %d]',siz(1),siz(2));
      % to reproduce the bug:
      % a=timeseries([1 2],1); Simulink.SimulationData.TimeseriesUtil.getSampleDimensions(a), a
      
      %Check that QualityTag is a scalar or has the same sizes as Value
      siz_Q = size(S.QualityTag);
      assert(isscalar(S.QualityTag)||all(siz==siz_Q),'SCDsignal:SizeMismatch',...
        sprintf(['QualityTag should be either a scalar or an array of the same size as Value.',...
        '\nSize_QualityTag=[',repmat(' %1.0f ',1,numel(siz_Q)),...
        ']\nSize_Value=['    ,repmat(' %1.0f ',1,numel(siz)),']'],siz_Q,siz));
      
    end
    
    function type = datatype(obj)
      type = class(obj.Value);
      assert(~ismember(type,{'double'}),...
        sprintf('DataType %s is not supported in SCDalgo', type))
    end
    
    function str = struct(obj)
      % method to convert to structure
      for ii=fieldnames(obj)'
        myf = ii{:};
        str.(myf) = obj.(myf);
      end
    end
    
    function dimension = dimension(obj)
      % dimension as required by buses etc.
      siz = obj.datasize;
      if siz(2) == 1, dimension = siz(1);
      else,          dimension = siz;
      end
    end
      
    function siz = datasize(obj,dim)
      % size of data
      if nargin==1, siz = size(obj.Value);
      else, siz = size(obj.Value,dim);  
      end
    end
    
    function valid = isvalid(obj)
      % true if GOOD and RUNNING
      isRunning = ([obj.ProductionState] == 'RUNNING');
      isGood    = ([obj.QualityTag]     == 'GOOD');
      valid = isRunning & isGood;
    end
    
    function [BusesObj] = createBus(obj)
      BusesObj = repmat(Simulink.Bus,size(obj));
      if numel(obj)>1 % for arrays of SCDsignal objects
        for ii = 1:numel(obj)
          BusesObj(ii) = createBus(obj(ii)); % recursive call
        end
      else
        % creates a bus object corresponding to this signal class
        tmp = Simulink.Bus.createObject(struct(obj));
        myBusObj = evalin('base',tmp.busName);
        evalin('base',sprintf('clear %s',tmp.busName));
        myBusObj.Description = obj.BusName;
        % 2D to 1D where possible
        for iEl = 1:numel(myBusObj.Elements)
          dims =  myBusObj.Elements(iEl).Dimensions;
          if dims(2) == 1
            myBusObj.Elements(iEl).Dimensions = dims(1);
          end
        end
        BusesObj = myBusObj;
      end
    end
    
    function Name = BusName(obj)
      % Returns the bus name for this signal
      type = obj.datatype;
      siz  = obj.datasize;
      assert(numel(siz)==2,'3 or larger dimensional data not supported')
      if all(siz == 1)
        sizestr = '_Scalar';
      elseif siz(2) == 1
        sizestr = sprintf('_%d_Vector',siz(1));
      else % matrix
        sizestr = sprintf('_%d_%d_Matrix',siz(1),siz(2));
      end
      Name = sprintf('SCDBus_%s%s',type,sizestr);
    end
    
    %Check if the size of Value is kept constant if there is an update and assigns 
    %the new value contained in var to obj.Value
    function obj = set.Value(obj,var)
      %perform this check only after the first assigment in the constructor,
      %default value of Value = 0
      if ~(isscalar(obj.Value) && obj.Value ==0)    
        siz     = size(obj.Value);
        siz_var = size(var);
        assert(all(siz==siz_var),'SCDsignal:SizeMismatch',...
          sprintf(['Assignment attempted to change the size of Value.',...
          '\nSize_Value=[',repmat(' %1.0f ',1,numel(siz)),...
          ']\nSize_Value_attempted=[',repmat(' %1.0f ',1,numel(siz_var)),']'],siz,siz_var));
      end
      obj.Value = var;
    end
    
    %Check if the QualityTag array is a scalar or has
    %the same number of elements as Value and assigns 
    %the new value contained in var to obj.QualityTag
    function obj = set.QualityTag(obj,var)
      %perform this check only after the first assigment in the
      %constructor, default value of QualityTag = INVALID
      if ~(isscalar(obj.QualityTag) && obj.QualityTag == QualityTag.INVALID)  
        siz     = size(obj.QualityTag);
        siz_var = size(var);
        assert(all(siz==siz_var),'SCDsignal:SizeMismatch',...
          sprintf(['Assignment attempted to change the size of QualityTag.',...
          '\nSize_QualityTag=[',repmat(' %1.0f ',1,numel(siz)),...
          ']\nSize_QualityTag_attempted=[',repmat(' %1.0f ',1,numel(siz_var)),']'],siz,siz_var));
      end
      obj.QualityTag = var;
    end
    
  end
  
end