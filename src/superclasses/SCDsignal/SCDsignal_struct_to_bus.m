function [BusObjList,BusNameList] = SCDsignal_struct_to_bus(mystruct,BusName,busDescription)
% convert struct to bus objects and names names
% Generalises Simulink.Bus.CreateObject but allows SCDsignals to be in
% some of the leaves.
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
if nargin==2, busDescription = ''; end

BusObjList = {}; BusNameList = {}; % init

fields = fieldnames(mystruct)';
nFields = numel(fields);
Elems  = repmat(Simulink.BusElement,nFields,1);

for ii=1:nFields
  myfield = fields{ii};
  Name = myfield;
  Value = mystruct.(myfield);
  
  % fill element datatype and size depending on example value object
  if isnumeric(Value) || islogical(Value) % accepted regular types
    DataType = class(Value);
    if isvector(Value), Dimensions = numel(Value);
    else,               Dimensions = size(Value) ;
    end
  else
    % this node is a bus in itself
    if isa(Value,'SCDsignal') % Special SCDsignal class
      newBusName = Value.BusName; % use SCDsignal method to get bus name
      newBusObj  = Value.createBus; % use SCD signal method to create bus;
    elseif isstruct(Value)
      % recurse
      [newBusObj,newBusNameList]  = SCDsignal_struct_to_bus(Value,[Name,'Bus']);
      BusNameList = [newBusNameList(2:end),BusNameList];  %#ok<AGROW>  % append these
      newBusName = [Name,'Bus'];
    else
      error('Unsupported datatype %s',class(Value));
    end
    Dimensions = 1;
    DataType   = newBusName;
    if ~any(strcmp(BusNameList,newBusName)) % Bus not yet defined
      % add Buses generated here to list
      BusObjList  = [{newBusObj},    BusObjList]; %#ok<AGROW> % add to list of buses
      BusNameList = [newBusName,  BusNameList]; %#ok<AGROW> 
    end
  end
  
  %% Complete bus
  % Elements
  myElem = Elems(ii); % init;
  myElem.Name        = Name;
  myElem.DataType    = DataType;
  myElem.Dimensions  = Dimensions;
  Elems(ii) = myElem;
end

myBus = Simulink.Bus;
myBus.HeaderFile  = '';
myBus.Description = busDescription;
myBus.DataScope   = 'Auto';
myBus.Alignment   = -1;
myBus.Elements    = Elems;

BusObjList  = [{myBus},   BusObjList];
BusNameList = [BusName,BusNameList];
end