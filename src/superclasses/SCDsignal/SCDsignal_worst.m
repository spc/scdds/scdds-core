function Qout = SCDsignal_worst(Q1,Q2)
% Worst between various quality tags
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
Qout = QualityTag(max(int8(Q1),int8(Q2)));
end