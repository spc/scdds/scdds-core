% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

% SCDsignal with symmetric error bar

classdef SCDsignalerror < SCDsignal
  
  properties
    Error                  {mustBeNumericOrLogical,mustBeNonempty} = 0;
  end
  
  methods
    function S = SCDsignalerror(varargin)      
      S@SCDsignal(varargin{:});    
      if numel(varargin)>=1 && ~isempty(varargin{1}), S.Error           = varargin{1}; end
    end
    
    function Name = BusName(obj)
      Name = BusName@SCDsignal(obj);
      Name = [Name '_error'];
    end
    
  end
end

