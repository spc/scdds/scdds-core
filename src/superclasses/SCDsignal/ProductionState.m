classdef(Enumeration) ProductionState < int8
  % Production state enumeration class
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  
  enumeration
    RUNNING(0)
    STOPPED(1)
    OUTDATED(2)
  end
  methods (Static = true)
    % add methods, e.g.  (See documentation)
    % getDefaultValue :
    % getDescription
    % getHeaderFile % define RT code C header
    %
        function retVal = getDefaultValue()
        % GETDEFAULTVALUE  Returns the default enumerated value.
        %   This value must be an instance of the enumerated class.
        %   If this method is not defined, the first enumeration is used.
        retVal = ProductionState.STOPPED;
    end
  end
end