function scdds_core_paths()
% function for adding scdds_core_paths
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

fprintf('Default temp dir is %s\n', tempdir);
[res,privatetmpdir]=system('mktemp -d -p /tmp'); % TODO: put into a subfolder of tmp ?
if res==0
  privatetmpdir=privatetmpdir(1:end-1);
  setenv('TMPDIR',privatetmpdir);
  clear tempdir;
  fprintf('Default temp dir changed to %s\n', tempdir);
  privatetmpdirok=true;
else
  warning('could not change the default temp dir (mktemp returned %d), concurrent sessions will likely fail randomly.',res);
  privatetmpdirok=false;
end

if privatetmpdirok
cachePath = Simulink.dd.defaultCachePath;
[filepath,name,ext] = fileparts(cachePath); 
Simulink.dd.setCachePath(fullfile(privatetmpdir,[name ext])); % setting it to a new different location
cachePath = Simulink.dd.defaultCachePath;
fprintf('Default Simulink cache path is: %s\n',cachePath);
end

fprintf('setting SCDDS-core paths...\n')
vv=ver('matlab');
if ~((strcmp(vv.Version,'9.6') || strcmp(vv.Version,'9.11') || strcmp(vv.Version,'9.12') || strcmp(vv.Version,'9.14')))
    warning('this matlab version is not officially supported for SCDDS')
end


%assert(strcmp(vv.Version,'9.6') || strcmp(vv.Version,'9.11') || strcmp(vv.Version,'9.12') || strcmp(vv.Version,'9.14'),'this matlab %version is not supported for SCDDS')
clear vv

scdds_core_folder = fileparts(mfilename('fullpath')); % folder containing this script

corepath = getenv('SCDDS_COREPATH');
if isempty(corepath)
  warning('SCDDS_COREPATH is empty, code generation may not work');
else
  assert(isequal(scdds_core_folder,corepath),...
    'SCDDS_COREPATH set to wrong folder, is %s but should be %s',corepath,scdds_core_folder);
end

% Core paths
addpath(scdds_core_folder);
addpath(genpath(fullfile(scdds_core_folder,'src')));
addpath(genpath(fullfile(scdds_core_folder,'configurations')));
addpath(genpath(fullfile(scdds_core_folder,'codegen')));
addpath(genpath(fullfile(scdds_core_folder,'tests')));
addpath(genpath(fullfile(scdds_core_folder,'algos')));
addpath(genpath(fullfile(scdds_core_folder,'mds'  )));
addpath(fullfile(scdds_core_folder,'tools'));
end
