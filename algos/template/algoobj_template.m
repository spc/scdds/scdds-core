function obj = algoobj_template()

%% Doublets SPC controller algorithm
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
obj = algo_template_class('algo_template');

%% Timing of the algorithm
obj=obj.settiming(-1,1e-3,1.0);

%% Fixed parameters init functions 
obj=obj.addfpinitfcn('algo_template_loadfp','algo_template_fp');

%% Tunable parameters structure name
obj=obj.addtunparamstruct('algo_template_tp', @()algo_template_loadtp(), false);

%% Tunable parameters
parshot=2;
mdsserver='spcscddev';
mdstree  ='scdds';
%obj=obj.addparameter(SCDDSclass_mdsparnumeric('template.params.enable'        ,'enable'          ,'mdshelp','a bool param',...
 %   'mdsvalid','1','srcsrv',mdsserver,'srctree',mdstree)); 
obj=obj.addparameter(SCDDSclass_mdsparnumeric('template.params.gain'          ,'gain'            ,'mdshelp','a tunable gain',...
    'mdsvalid','1','srcsrv',mdsserver,'srctree',mdstree));
obj=obj.addparameter(SCDDSclass_mdsparnumeric('template.params.refmodel.gain' ,'refmodel.gain'   ,'mdshelp','a tunable gain',...
    'mdsvalid','1','srcsrv',mdsserver,'srctree',mdstree));
obj=obj.addparameter(SCDDSclass_mdsparnumeric('template.params.rowvect'       ,'rowvect'         ,'mdshelp','a row vector',...
    'mdsvalid','1','srcsrv',mdsserver,'srctree',mdstree));
obj=obj.addparameter(SCDDSclass_mdsparnumeric('template.params.colvect'       ,'colvect'         ,'mdshelp','a column vector',...
    'mdsvalid','1','srcsrv',mdsserver,'srctree',mdstree));
obj=obj.addparameter(SCDDSclass_mdsparnumeric('template.params.matrix'        ,'matrix'          ,'mdshelp','a matrix',...
    'mdsvalid','1','srcsrv',mdsserver,'srctree',mdstree));

%% Wavegens
obj=obj.addwavegenbasetruct('algo_template_inbus1');
obj=obj.addwavegen(SCDclass_mdswgsigsingle( 'template.inputs.signal1','signal1'        ,'srcsrv',mdsserver,'srctree',mdstree));
obj=obj.addwavegen(SCDclass_mdswgsigsingle( 'template.inputs.signal2','signal2'        ,'srcsrv',mdsserver,'srctree',mdstree));


%% Buses
obj = obj.addbus('algo_template_inBus', 'algo_template_inBus_def' );
obj = obj.addbus('algo_template_outBus', 'algo_template_outBus_def' );

 % function handle that returns cell arays of buses and busnames to be registered
obj = obj.addbus('',@() algo_template_signal_buses());

%% Tasks

%% Print (optional)
obj.printinfo;

end

