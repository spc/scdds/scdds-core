classdef algo_template_test < SCDDSalgo_test
  % Class for template algorithm tests
  % Copy this class for your algorithm and change the 
  % algoobj property to be a function handle to 
  % the algoobj definition file
  %
  % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
  % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
  properties
    algoobj = @algoobj_template;
  end

  methods(TestClassSetup)
    % common methods for all tests
    function setup_paths(~) % function to setup desired paths
      run('scdds_core_paths.m');
    end
  end
  
end