function TP = algo_template_loadtp()
% Setup tunable control params default values
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

TP.enable           = true;
TP.gain             = double(2);
TP.refmodel.gain    = double(4); % another gain used in referenced model
TP.rowvect          = double([1 2 3]);
TP.colvect          = double([1 2 3]');
TP.matrix           = double([1 2; 3 4]);

TP = Simulink.Parameter(TP);

end
