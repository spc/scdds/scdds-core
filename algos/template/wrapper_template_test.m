classdef wrapper_template_test < SCDDSwrapper_test
% inherited wrapper test class

 % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
 % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.


    properties
        wrapper
    end

    methods(TestClassSetup)
        function obj = wrapper_template_test(TestCase)
            % constructor
            obj_algo = algoobj_template();

            obj.wrapper = wrapper_template_class('wrapper_template');

            obj.wrapper = obj.wrapper.addalgo(obj_algo);
        end
    end
end
