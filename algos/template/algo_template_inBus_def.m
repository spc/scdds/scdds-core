% Bus object: SCDalgo_template_inBus 
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'signal1';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'single';
elems(1).SampleTime = -1;
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'signal2';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).SampleTime = -1;
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

algo_template_inBus = Simulink.Bus;
algo_template_inBus.HeaderFile = '';
algo_template_inBus.Description = '';
algo_template_inBus.DataScope = 'Auto';
algo_template_inBus.Alignment = -1;
algo_template_inBus.Elements = elems;
clear elems;
