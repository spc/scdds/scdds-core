classdef wrapper_template_class < SCDDSclass_wrapper
    %WRAPPER_TEMPLATE_CLASS Empty inherited class for illustrative purpose

     % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
     % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.


end