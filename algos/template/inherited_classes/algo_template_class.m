classdef algo_template_class < SCDDSclass_algo
    %ALGO_TEMPLATE_CLASS Empty inherited class for illustrative purpose

     % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
     % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
     
     methods
         
         function obj = algo_template_class(name)
             % class constructor
             obj@SCDDSclass_algo(name);
             obj.exporttptobase = 1; % overwrite default to make actualize method work
                        
         end
     end


end

