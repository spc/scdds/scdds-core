% This script will crate automatically
% the TCL files for building 
% parameters, inputs and outputs trees, using SPC mds-matlab-structs
% functions (liked in a git submodule)
%
% it is assumed that the main algo_template_tp
% structure is already present in the base workspace
%
% if successfully executed the three generated TCL scripts:
% /tmp/<username>/template_tp.tcl
% /tmp/<username>/template_inbus.tcl
% /tmp/<username>/template_outbus.tcl
% can be executed by a MDSplus tree edit enabled user via:
% cat /tmp/<username>/template_tp.tcl | mdstcl
% and so on
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

addpath ../../mds/mds-matlab-structs

%% tunable parameters
tpsrc=Simulink.data.evalinGlobal('algo_template','algo_template_tp_tmpl').Value;
tpsrcpad=struct();
tpsrcpad.template.params=tpsrc;
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(tpsrcpad,'template_tp.tcl','scdds','value')
clear tpsrc tpsrcpad

%% input bus
inbus=Simulink.data.evalinGlobal('algo_template','algo_template_inBus');
inbussrc=Simulink.Bus.createMATLABStruct('inbus');
inbussrcpad.template.inputs=inbussrc;
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(inbussrcpad,'template_inbus.tcl','scdds','data','full_name','dim','signal')
clear inbus inbussrc inbussrcpad

%% output bus
outbus=Simulink.data.evalinGlobal('algo_template','algo_template_outBus');
outbussrc=Simulink.Bus.createMATLABStruct('outbus');
outbussrcpad.template.outputs=outbussrc;
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(outbussrcpad,'template_outbus.tcl','scdds','data','full_name','dim','signal')
clear outbus outbussrc outbussrcpad

%% metadata channels
metach.template.system.ch01=single(0);
metach.template.system.ch02=single(0);
metach.template.system.ch03=single(0);
metach.template.system.ch04=single(0);
[status_out,struct_out_ala_mds,fname_out]=...
    mds_create_nodes(metach,'template_system.tcl','scdds','data','full_name','dim','signal')
clear metach

