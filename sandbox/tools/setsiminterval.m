function setsiminterval(tstart,tstop)
%setsiminterval(tstart,tstop) sets the simulation time interval
%   It changes the currently defined configurationSettings
%   in the base workspace
%
% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

evalin('base',sprintf('configurationSettings.set_param(''StartTime'',''%f'')',tstart));
evalin('base',sprintf('configurationSettings.set_param(''StopTime'',''%f'')',tstop));
end

