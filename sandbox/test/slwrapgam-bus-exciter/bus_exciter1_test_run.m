% [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
% Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.

% simple test runner on SCD node06
% this is not included into scdds-core or scdds-demo mainstream dev
% since we have to decide whether to support this way
% of signal mapping on  scdds and MARTe2, currently they're only
% TCV-centric
% see: https://gitlab.epfl.ch/spc/SPCIT/marte2-components/-/issues/18

%% Declare configuration settings
SCDconf_setCODEconf;
write_SCDDS_header('N/A'); 

%% Build bus_exciter1
Simulink.fileGenControl('set','CacheFolder',[pwd '/cache'],'createdir',true);
Simulink.fileGenControl('set','CodeGenFolder',[pwd '/codegen'],'createdir',true);
rtwbuild('bus_exciter1');

%% scp cfg file and so to node06
system('scp bus_exciter1.cfg         tcv_oper@spcscddev:~/martecfgs/tests');
system('scp codegen/bus_exciter1.so  tcv_oper@spcscddev:~/martecfgs/tests');

%% run MARTe2 via the starter service
% note: this assumes that the MARTe2 run is self-advancing and self-terminating
system('ssh root@node06 ''timeout 10 /root/MARTe2-examples/MARTeApp.sh -f /home/tcv_oper/martecfgs/tests/bus_exciter1.cfg  -l RealTimeLoader -s State1''');
