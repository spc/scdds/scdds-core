#!/bin/bash
ADDINIT=$@
echo "ADDINIT=$ADDINIT"

# setup SCDDS core environment
SCDDS_COREPATH="$( dirname -- "$( readlink -f -- "$0"; )"; )" # this directory
echo SCDDS_COREPATH=$SCDDS_COREPATH
source $SCDDS_COREPATH/scdds_env

# command to execute inside matlab
#MATEXEC="-batch "
MATEXEC="-r "

if [[ -z "$ADDINIT" ]] 
then 
   # If no user-defined override
   ADDINIT="run(fullfile('$SCDDS_COREPATH','scdds_core_paths'));"; 
fi
MATEXEC+=$ADDINIT

# Start matlab
if [[ $(hostname -f) =~ ^(lac[0-9]*|spc-ci|spcpc478|spcscddev)\.epfl\.ch ]] # if on LAC clusters or spc-ci or scd.epfl.ch or spcscddev.epfl.ch
then
	MATCMD="/usr/local/MATLAB/R2023a/bin/matlab -c /etc/network.lic"
else
	MATCMD="/usr/local/bin/matlab"
fi

#echo "LIBGL_ALWAYS_SOFTWARE=1 $MATCMD -nodesktop -softwareopengl -nosplash $MATEXEC"
#LIBGL_ALWAYS_SOFTWARE=1 $MATCMD  -nodesktop -softwareopengl -nosplash $MATEXEC
echo $MATCMD -nodesktop -nosplash $MATEXEC
$MATCMD -nodesktop -nosplash $MATEXEC
