  function write_SCDDS_header(descstr,max_string_length)
   % Write header file for SCDDS generated code
   %
   % [ SCDDS - Simulink Control Development & Deployment Suite ] Copyright SPC-EPFL Lausanne 2022.
   % Distributed under the terms of the GNU Lesser General Public License, LGPL-3.0-only.
   
   fprintf('Code decription string will be set to:\n%s\n', descstr);
   
   if nargin==1
     max_string_length = 1024;
   end
   assert(numel(descstr)<max_string_length,'description string length (%d) exceeds maximum (%d)',...
     numel(descstr),max_string_length);
   
   [path,~,~]=fileparts(mfilename('fullpath'));
   
   fname = 'scddsalgoinfo.h';
   fpath = fullfile(path,fname);
   fd=fopen(fpath,'w');
   fprintf(fd, '/* %s */\n',fname);
   fprintf(fd, '/* generated on: %s */\n',char(datetime));
   fprintf(fd, '\n');
   fprintf(fd, '#ifndef SCDALGOINFO\n');
   fprintf(fd, '#define SCDALGOINFO\n');
   fprintf(fd, ['#define ALGOINFOSTR "' descstr '"\n']);
   fprintf(fd, ['#define ALGOINFOLEN ' num2str(numel(descstr)) '\n']);
   fprintf(fd, ['#define ALGOINFOMAXLEN ' num2str(max_string_length) '\n']);
   fprintf(fd, '\n');
   fprintf(fd, 'struct algoinfo {\n');
   fprintf(fd, ' char text[ALGOINFOMAXLEN];\n');
   fprintf(fd, ' unsigned int len;\n');
   fprintf(fd, '} __attribute__((packed));\n');
   fprintf(fd, '\n');
   fprintf(fd, '#endif\n');
   fclose(fd);

   fprintf('wrote %s\n',fpath);
  end