This folder contains tmf and tlc
files to generate Linux .so 
libraries via simulink codegen.

It is mandatory to insert this code
in Simulink configuration:

In header file:

#ifndef SCDMACROS
#define SCDMACROS
#define UTSTRUCTNAME(NAME) RT_MODEL_##NAME##_T
#define TSTRUCTNAME(NAME) UTSTRUCTNAME(NAME)
#define UGETMMIFCN(NAME) NAME##_GetCAPImmi
#define GETMMIFCN(NAME) UGETMMIFCN(NAME)
#endif 

In source file:

rtwCAPI_ModelMappingInfo* GETMMIFCN(MODEL) ( TSTRUCTNAME(MODEL) *rtm )
{
  return &(rtmGetDataMapInfo(rtm).mmi);
}
